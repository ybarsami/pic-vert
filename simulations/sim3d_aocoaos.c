/**
 * PIC simulations in 3d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                              // functions cos, log, fmin
#include <mpi.h>                               // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                               // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                               // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                             // functions printf, fprintf (output strings on a stream), sprintf
                                               // constant  stderr (standard error output stream)
#include <stdlib.h>                            // functions malloc, free ((de)allocate memory)
                                               //           exit (error handling)
                                               // constant  EXIT_FAILURE (error handling)
                                               // type      size_t
#include <string.h>                            // function  strcmp
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>                          // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                               // functions PAPI_read_counters
                                               // type      long_long
#    include "papi_handlers.h"                 // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"                     // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"                       // function  normL2_field_3d, get_damping_values
#include "fields.h"                            // type      field_3d
                                               // functions create_field_3d, free_field_3d, accumulate_field_3d
#include "initial_distributions.h"             // constants LANDAU_1D_PROJ3D, LANDAU_2D_PROJ3D, LANDAU_3D_SUM_OF_COS,
                                               //           LANDAU_3D_PROD_OF_COS, LANDAU_3D_PROD_OF_ONE_PLUS_COS
#include "math_functions.h"                    // function  sqr
#include "matrix_functions.h"                  // functions allocate_matrix, deallocate_matrix, allocate_3d_array, deallocate_3d_array
#include "meshes.h"                            // type      cartesian_mesh_3d
                                               // function  create_mesh_3d
#include "output.h"                            // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"                        // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG, NB_PARTICLE
#include "parameter_reader.h"                  // type      simulation_parameters
                                               // constants STRING_NOT_SET, INT_NOT_SET, DOUBLE_NOT_SET
                                               // function  read_parameters_from_file
#include "particle_type_chunkbags_of_aos_3d.h" // types     particle, chunk, bag
                                               // functions create_particle_array_3d, bag_init, bag_push, bag_append, init_freelists
#include "poisson_solvers.h"                   // type      poisson_3d_solver
                                               // function  new_poisson_3d_fft_solver, compute_E_from_rho_3d_fft, free_poisson_3d
#include "random.h"                            // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG
#include "rho.h"                               // constant  NB_CORNERS_3D
                                               // functions mpi_reduce_rho_3d, reset_charge_3d_accumulator, convert_charge_to_rho_3d_per_per
#include "space_filling_curves.h"              // macro     COMPUTE_I_CELL_3D

/*****************************************************************************
 *                             Simulation 3d                                 *
 *****************************************************************************/

// All the simulations in this file follow the 'array of structures'
// data layout. The structure doesn't contains the weight, because it is known
// to be a constant.

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

#ifndef THERMAL_SPEED
#    define THERMAL_SPEED 1.
#endif

#ifndef DRIFT_VELOCITY
#    define DRIFT_VELOCITY 4.
#endif

#ifndef PROPORTION_FAST_PARTICLES
#    define PROPORTION_FAST_PARTICLES 0.01
#endif

#ifndef INITIAL_DISTRIBUTION
#    define INITIAL_DISTRIBUTION LANDAU_3D_PROD_OF_ONE_PLUS_COS
#endif

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5;
    double time_particle_loop, time_append, time_mpi_allreduce, time_poisson;
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Automatic values for the parameters.
    unsigned char sim_distrib = INITIAL_DISTRIBUTION; // Physical test case (LANDAU_1D_PROJ3D, LANDAU_2D_PROJ3D, LANDAU_3D_SUM_OF_COS,
                                                      // LANDAU_3D_PROD_OF_COS, LANDAU_3D_PROD_OF_ONE_PLUS_COS or DRIFT_VELOCITIES_3D).
    int ncx               = NCX;                      // Number of grid points, x-axis
    int ncy               = NCY;                      // Number of grid points, y-axis
    int ncz               = NCZ;                      // Number of grid points, z-axis
    long int nb_particles = NB_PARTICLE;              // Number of particles
    int num_iteration     = NB_ITER;                  // Number of time steps
    double delta_t        = DELTA_T;                  // Time step
    double thermal_speed  = THERMAL_SPEED;            // Thermal speed
    // DRIFT_VELOCITIES_3D only
    double drift_velocity            = DRIFT_VELOCITY;            // Center of the second maxwellian
    double proportion_fast_particles = PROPORTION_FAST_PARTICLES; // Proportions of the particles in the second maxwellian
    // LANDAU_3D_PROD_OF_ONE_PLUS_COS only
    double L = 22.;        // Length of the physical space
    // LANDAU_XXX only
    double alpha   = 0.05; // Landau perturbation amplitude
    double kmode_x = 0.5;  // Landau perturbation mode, x-axis
    double kmode_y = 0.5;  // Landau perturbation mode, y-axis
    double kmode_z = 0.5;  // Landau perturbation mode, z-axis
    
    // Read parameters from file.
    if (argc >= 2) {
        simulation_parameters parameters = read_parameters_from_file(argv[1], "3D");
        if (strcmp(parameters.sim_distrib_name, STRING_NOT_SET) != 0)
            sim_distrib = parameters.sim_distrib;
        if (parameters.ncx != INT_NOT_SET)
            ncx             = parameters.ncx;
        if (parameters.ncy != INT_NOT_SET)
            ncy             = parameters.ncy;
        if (parameters.ncz != INT_NOT_SET)
            ncz             = parameters.ncz;
        if (parameters.nb_particles != INT_NOT_SET)
            nb_particles    = parameters.nb_particles;
        if (parameters.num_iteration != INT_NOT_SET)
            num_iteration   = parameters.num_iteration;
        if (parameters.delta_t != DOUBLE_NOT_SET)
            delta_t       = parameters.delta_t;
        if (parameters.thermal_speed != DOUBLE_NOT_SET)
            thermal_speed = parameters.thermal_speed;
        // DRIFT_VELOCITIES_3D only
        if (parameters.drift_velocity != DOUBLE_NOT_SET)
            drift_velocity = parameters.drift_velocity;
        if (parameters.proportion_fast_particles != DOUBLE_NOT_SET)
            proportion_fast_particles = parameters.proportion_fast_particles;
        // LANDAU_3D_PROD_OF_ONE_PLUS_COS only
        if (parameters.L != DOUBLE_NOT_SET)
            L = parameters.L;
        // LANDAU_XXX only
        if (parameters.alpha != DOUBLE_NOT_SET)
            alpha   = parameters.alpha;
        if (parameters.kmode_x != DOUBLE_NOT_SET)
            kmode_x = parameters.kmode_x;
        if (parameters.kmode_y != DOUBLE_NOT_SET)
            kmode_y = parameters.kmode_y;
        if (parameters.kmode_z != DOUBLE_NOT_SET)
            kmode_z = parameters.kmode_z;
    } else
        printf("No parameter file was passed through the command line. I will use the default parameters.\n");
    
    // Random initialization or read from file.
    const char sim_initial = INIT_NOFILE;
    
    // Spatial parameters for initial density function.
    double *params;
    if (sim_distrib == LANDAU_1D_PROJ3D) {
        params = malloc(2 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
    } else if (sim_distrib == LANDAU_2D_PROJ3D) {
        params = malloc(3 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
    } else if (sim_distrib == LANDAU_3D_PROD_OF_ONE_PLUS_COS) {
        params = malloc(4 * sizeof(double));
        params[0] = alpha;
        params[1] = 2 * PI / L;
        params[2] = 2 * PI / L;
        params[3] = 2 * PI / L;
    } else {
        params = malloc(4 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = kmode_z;
    }
    
    // Velocity parameters for initial density function.
    double *speed_params;
    if (sim_distrib == DRIFT_VELOCITIES_3D) {
        params = malloc(3 * sizeof(double));
        speed_params = malloc(3 * sizeof(double));
        speed_params[0] = thermal_speed;
        speed_params[1] = drift_velocity;
        speed_params[2] = proportion_fast_particles;
    } else if (sim_distrib == UNIFORM_3D) {
        speed_params = malloc(3 * sizeof(double));
        speed_params[0] = 1.;
        speed_params[1] = 1.;
        speed_params[2] = 1.;
    } else {
        speed_params = malloc(1 * sizeof(double));
        speed_params[0] = thermal_speed;
    }
    
    // Mesh
    double x_min, y_min, z_min, x_max, y_max, z_max;
    const int num_cells_3d = ncx * ncy * ncz;
    x_min = 0.;
    y_min = 0.;
    z_min = 0.;
    if (sim_distrib == LANDAU_1D_PROJ3D) {
        x_max = 2 * PI / kmode_x;
        y_max = 1.;
        z_max = 1.;
    } else if (sim_distrib == LANDAU_2D_PROJ3D) {
        x_max = 2 * PI / kmode_x;
        y_max = 2 * PI / kmode_y;
        z_max = 1.;
    } else if (sim_distrib == LANDAU_3D_PROD_OF_ONE_PLUS_COS) {
        x_max = L;
        y_max = L;
        z_max = L;
    } else if (sim_distrib == UNIFORM_3D) {
        x_max = ncx;
        y_max = ncy;
        z_max = ncz;
    } else {
        x_max = 2 * PI / kmode_x;
        y_max = 2 * PI / kmode_y;
        z_max = 2 * PI / kmode_z;
    }
    cartesian_mesh_3d mesh = create_mesh_3d(ncx, ncy, ncz, x_min, x_max, y_min, y_max, z_min, z_max);
    const int icell_param1 = I_CELL_PARAM1_3D(ncx, ncy, ncz);
    const int icell_param2 = I_CELL_PARAM2_3D(ncx, ncy, ncz);
    const int ncxminusone = ncx - 1;
    const int ncyminusone = ncy - 1;
    const int nczminusone = ncz - 1;

    // Vectorization of the deposit
    int corner;
/*
 * x-axis : left  -> right
 * y-axis : front -> back
 * z-axis : down  -> top
 *
 * (0, 0, 0) : Left,  Front, Down : LFD
 * (0, 0, 1) : Left,  Front, Top  : LFT
 * (0, 1, 0) : Left,  Back,  Down : LBD
 * (0, 1, 1) : Left,  Back,  Top  : LBT
 * (1, 0, 0) : Right, Front, Down : RFD
 * (1, 0, 1) : Right, Front, Top  : RFT
 * (1, 1, 0) : Right, Back,  Down : RBD
 * (1, 1, 1) : Right, Back,  Top  : RBT
 *
 *
 *    LBT +————————+ RBT
 *       /'       /|
 *      / '   RFT/ |
 * LFT +————————+  |
 *     |  '     |  |
 *     |  +-----|--+ RBD
 *     | /LBD   | /
 *     |/       |/
 * LFD +————————+ RFD
 *
 */
    // Space coeffs                                                             LFD  LFT  LBD  LBT  RFD  RFT  RBD  RBT
    const float coeffs_x[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  1.,  1.,  1.,  0.,  0.,  0.,  0.};
    const float  signs_x[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = { -1., -1., -1., -1.,  1.,  1.,  1.,  1.};
    const float coeffs_y[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  1.,  0.,  0.,  1.,  1.,  0.,  0.};
    const float  signs_y[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = { -1., -1.,  1.,  1., -1., -1.,  1.,  1.};
    const float coeffs_z[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  0.,  1.,  0.,  1.,  0.,  1.,  0.};
    const float  signs_z[NB_CORNERS_3D] __attribute__((aligned(VEC_ALIGN))) = { -1.,  1., -1.,  1., -1.,  1., -1.,  1.};
    
    // Simulation parameters
    const double q           = -1.; // particle charge
    const double m           =  1.; // particle mass
    const double dt_q_over_m = delta_t * q / m;
    const double dt_over_dx  = delta_t / mesh.delta_x;
    const double dt_over_dy  = delta_t / mesh.delta_y;
    const double dt_over_dz  = delta_t / mesh.delta_z;
    char simulation_name[42]     = "Vlasov-Poisson 3d";
    char data_structure_name[99] = "Array of Chunkbags of Array of Structures (num_threads private / cell)";
    char sort_name[42]           = "always sort";
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = malloc(num_cells_3d * sizeof(double));
    double* recv_buf = malloc(num_cells_3d * sizeof(double));
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    int thread_id;
    int offset;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    const int NB_BAGS_PER_CELL = num_threads; // Total number of chunkbags.
    
    // Temporary variables.
    double x, y, z;                                                 // store the new position values
    int ic_x, ic_y, ic_z, i_cell;                                   // store the new position index values
    size_t i, j, k, i_time;                                         // loop indices
    double*** q_times_rho = allocate_3d_array(ncx+1, ncy+1, ncz+1); // to store q * rho = -rho
    
    // The following matrices are (ncx+1) * (ncy+1) * (ncz+1) arrays, with the periodicity :
    //     M[ncx][ . ][ . ] = M[0][.][.]
    //     M[ . ][ncy][ . ] = M[.][0][.]
    //     M[ . ][ . ][ncz] = M[.][.][0]
    // rho the charge density
    // Ex the electric field on the x-axis
    // Ey the electric field on the y-axis
    // Ez the electric field on the z-axis
    double*** rho_3d = allocate_3d_array(ncx+1, ncy+1, ncz+1);
    double*** Ex = allocate_3d_array(ncx+1, ncy+1, ncz+1);
    double*** Ey = allocate_3d_array(ncx+1, ncy+1, ncz+1);
    double*** Ez = allocate_3d_array(ncx+1, ncy+1, ncz+1);
    
    // accumulators are num_cells_3d arrays : for each cell, the 8 corners values
    field_3d E_field = create_field_3d(ncx, ncy, ncz);
    // For each cell, the 8 corners values for vectorization ; each thread has its own copy
    double* charge_accu;
    if (posix_memalign((void**)&charge_accu, VEC_ALIGN, num_cells_3d * NB_CORNERS_3D * num_threads * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize charge_accu.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(charge_accu, VEC_ALIGN);
#else
    charge_accu = __builtin_assume_aligned(charge_accu, VEC_ALIGN);
#endif
    // For reduction of charge_accu over the threads.
    double* reduced_charge_accu = allocate_array(num_cells_3d * NB_CORNERS_3D);
    
    // Diagnostic energy.
    double kmode = 0.;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ3D:
            kmode = kmode_x;
            break;
        case LANDAU_3D_SUM_OF_COS:
        case LANDAU_3D_PROD_OF_ONE_PLUS_COS:
            // The smaller kmode is, the more it's dominating (the decay is smaller).
            kmode = fmin(kmode_x, fmin(kmode_y, kmode_z));
            break;
        case LANDAU_2D_PROJ3D:
            kmode = sqrt(sqr(kmode_x) + sqr(kmode_y));
            break;
        case LANDAU_3D_PROD_OF_COS:
            kmode = sqrt(sqr(kmode_x) + sqr(kmode_y) + sqr(kmode_z));
            break;
    }
    damping_values* landau_values = get_damping_values(kmode);
    const double er         = landau_values->er;
    const double psi        = landau_values->psi;
    const double omega_real = landau_values->omega_real;
    const double omega_imag = landau_values->omega_imag;
    double exval_ee, val_ee, t;
    const double landau_mult_cstt = sqr(4. * alpha * er) * PI / kmode; // Landau
    const int diag_energy_size = 5;
    const int diag_speed_size  = 5;
    double** diag_energy = allocate_matrix(num_iteration, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration, diag_speed_size);
    
    // Poisson solver.
    poisson_3d_solver solver = new_poisson_3d_fft_solver(mesh);
    
    // Particle data structure.
    init_freelists(nb_particles);
    // Bags in cell i are taken from free list i % num_threads to
    // equally distribute the pressure on the different free lists.
    bag* particles = malloc(num_cells_3d * sizeof(bag));
    for (i = 0; i < num_cells_3d; i++)
        bag_init(&(particles[i]), i % num_threads);
    bag** particlesNext = malloc(NB_BAGS_PER_CELL * sizeof(bag*));
    for (j = 0; j < NB_BAGS_PER_CELL; j++) {
        particlesNext[j] = malloc(num_cells_3d * sizeof(bag));
        for (i = 0; i < num_cells_3d; i++)
            bag_init(&(particlesNext[j][i]), i % num_threads);
    }
    bag* chunkbag;
    chunk* next_chunk;
    chunk* my_chunk;
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    if (sim_initial == INIT_READ) {
        time_start = omp_get_wtime();
        read_particle_array_3d(mpi_world_size, nb_particles, mesh, &weight, &particles);
        if (mpi_rank == 0)
            printf("Read time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
        create_particle_array_3d(mpi_world_size, nb_particles, mesh, sim_distrib,
            params, speed_params, &weight, &particles);
        if (mpi_rank == 0)
            printf("Creation time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%ldkk.dat", nb_particles / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d %d %d\n", ncx, ncy, ncz);
            fprintf(file_write_particles, "%ld\n", nb_particles);
            for (j = 0; j < num_cells_3d; j++) {
                chunkbag = &(particles[j]);
                bag_store_size_of_back(chunkbag);
                for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                    for (i = 0; i < my_chunk->size; i++) {
                        fprintf(file_write_particles, "%ld %.*g %.*g %.*g %.*g %.*g %.*g\n", j,
                          FLT_DECIMAL_DIG, my_chunk->array[i].dx, FLT_DECIMAL_DIG, my_chunk->array[i].dy,
                          FLT_DECIMAL_DIG, my_chunk->array[i].dz,
                          DBL_DECIMAL_DIG, my_chunk->array[i].vx, DBL_DECIMAL_DIG, my_chunk->array[i].vy,
                          DBL_DECIMAL_DIG, my_chunk->array[i].vz);
                    }
                }
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x * mesh.delta_y * mesh.delta_z);
    // We just use the electric fields to update the speed, with always the same multiply.
    const double x_field_factor = dt_q_over_m * dt_over_dx;
    const double y_field_factor = dt_q_over_m * dt_over_dy;
    const double z_field_factor = dt_q_over_m * dt_over_dz;
    
  if (mpi_rank == 0) {
    printf("#CHUNK_SIZE = %d\n", CHUNK_SIZE);
    printf("#VEC_ALIGN = %d\n", VEC_ALIGN);
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#alpha = %.*g\n", DBL_DECIMAL_DIG, alpha);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#y_min = %.*g\n", DBL_DECIMAL_DIG, y_min);
    printf("#y_max = %.*g\n", DBL_DECIMAL_DIG, y_max);
    printf("#z_min = %.*g\n", DBL_DECIMAL_DIG, z_min);
    printf("#z_max = %.*g\n", DBL_DECIMAL_DIG, z_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_3d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
//    printf("#y_field_factor = %.*g\n", DBL_DECIMAL_DIG, y_field_factor);
//    printf("#z_field_factor = %.*g\n", DBL_DECIMAL_DIG, z_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#ncy = %d\n", ncy);
    printf("#ncz = %d\n", ncz);
    printf("#nb_particles = %ld\n", nb_particles);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    reset_charge_3d_accumulator(ncx, ncy, ncz, num_threads, charge_accu);
    // Computes rho at initial time.
    #pragma omp parallel private(thread_id, offset)
    {
        thread_id = omp_get_thread_num();
        offset = thread_id * NB_CORNERS_3D * num_cells_3d;
        #pragma omp for private(i, j, chunkbag, my_chunk, corner)
        for (j = 0; j < num_cells_3d; j++) {
            chunkbag = &(particles[j]);
            bag_store_size_of_back(chunkbag);
            for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                for (i = 0; i < my_chunk->size; i++) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd aligned(coeffs_x, coeffs_y, coeffs_z, signs_x, signs_y, signs_z:VEC_ALIGN)
#endif
                    for (corner = 0; corner < NB_CORNERS_3D; corner++) {
                        charge_accu[offset + NB_CORNERS_3D * j + corner] +=
                            (coeffs_x[corner] + signs_x[corner] * my_chunk->array[i].dx) *
                            (coeffs_y[corner] + signs_y[corner] * my_chunk->array[i].dy) *
                            (coeffs_z[corner] + signs_z[corner] * my_chunk->array[i].dz);
                    }
                }
            }
        }
        #pragma omp for private(i, j, corner)
        for (j = 0; j < num_cells_3d; j++) {
            for (corner = 0; corner < NB_CORNERS_3D; corner++)
                reduced_charge_accu[NB_CORNERS_3D * j + corner] = charge_accu[NB_CORNERS_3D * j + corner];
            for (i = 1; i < num_threads; i++) {
                offset = i * NB_CORNERS_3D * num_cells_3d;
                for (corner = 0; corner < NB_CORNERS_3D; corner++)
                    reduced_charge_accu[NB_CORNERS_3D * j + corner] += charge_accu[offset + NB_CORNERS_3D * j + corner];
            }
        }
    } // End parallel region
    convert_charge_to_rho_3d_per_per(reduced_charge_accu, ncx, ncy, ncz, charge_factor, rho_3d);
    mpi_reduce_rho_3d(mpi_world_size, send_buf, recv_buf, ncx, ncy, ncz, rho_3d);
    
    // Computes E at initial time.
    for (i = 0; i < ncx + 1; i++)
        for (j = 0; j < ncy + 1; j++)
            for (k = 0; k < ncz + 1; k++)
                q_times_rho[i][j][k] = q * rho_3d[i][j][k];
    compute_E_from_rho_3d_fft(solver, q_times_rho, Ex, Ey, Ez);
    accumulate_field_3d(Ex, Ey, Ez, ncx, ncy, ncz, x_field_factor, y_field_factor, z_field_factor, E_field);
    
    // Computes speeds half time-step backward (leap-frog method).
    // WARNING : starting from here, v doesn't represent the speed, but speed * dt / dx.
    #pragma omp parallel for private(i, j, chunkbag, my_chunk)
    for (j = 0; j < num_cells_3d; j++) {
        chunkbag = &(particles[j]);
        bag_store_size_of_back(chunkbag);
        for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp simd
#endif
            for (i = 0; i < my_chunk->size; i++) {
                my_chunk->array[i].vx = my_chunk->array[i].vx * dt_over_dx - 0.5 * (
                      (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.right_back_top
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.left_back_top
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.right_front_top
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.left_front_top
                    + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.right_back_down
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.left_back_down
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.right_front_down
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.left_front_down);
                my_chunk->array[i].vy = my_chunk->array[i].vy * dt_over_dy - 0.5 * (
                      (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.right_back_top
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.left_back_top
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.right_front_top
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.left_front_top
                    + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.right_back_down
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.left_back_down
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.right_front_down
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.left_front_down);
                my_chunk->array[i].vz = my_chunk->array[i].vz * dt_over_dz - 0.5 * (
                      (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.right_back_top
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.left_back_top
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.right_front_top
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.left_front_top
                    + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.right_back_down
                    + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.left_back_down
                    + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.right_front_down
                    + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.left_front_down);
            }
        }
    }
    
    /********************************************************************************************
     *                         Simulation with 4 corners data structure                         *
     *                                  (no if, fast modulo)                                    *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics energy
        t = i_time * delta_t;
        exval_ee = landau_mult_cstt * exp(2. * omega_imag * t) *
               (0.5 + 0.5 * cos(2. * (omega_real * t - psi)));
        switch(sim_distrib) {
            case LANDAU_1D_PROJ3D:
                val_ee = normL2_field_3d(mesh, Ex);
                break;
            case LANDAU_2D_PROJ3D:
                val_ee = normL2_field_3d(mesh, Ex) + normL2_field_3d(mesh, Ey);
                break;
            default:
                val_ee = normL2_field_3d(mesh, Ex) + normL2_field_3d(mesh, Ey) + normL2_field_3d(mesh, Ez);
        }
        diag_energy[i_time][0] = t;                   // time
        diag_energy[i_time][1] = 0.5 * log(val_ee);   // E_field's log(L2-norm) (simulated)
        diag_energy[i_time][2] = 0.5 * log(exval_ee); // E_field's log(L2-norm) (expected)
        diag_energy[i_time][3] = val_ee;              // E_field's L2-norm (simulated)
        diag_energy[i_time][4] = exval_ee;            // E_field's L2-norm (expected)
        
        time_mark1 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif

        reset_charge_3d_accumulator(ncx, ncy, ncz, num_threads, charge_accu);
        #pragma omp parallel private(thread_id, offset, my_chunk, chunkbag, next_chunk)
        {
            thread_id = omp_get_thread_num();
            offset = thread_id * NB_CORNERS_3D * num_cells_3d;
            // Particle loop
            #pragma omp for private(i, j, corner, x, y, z, ic_x, ic_y, ic_z, i_cell) firstprivate(ncxminusone, ncyminusone, nczminusone, icell_param1, icell_param2)
            for (j = 0; j < num_cells_3d; j++) {
                chunkbag = &(particles[j]);
                bag_store_size_of_back(chunkbag);
                for (my_chunk = chunkbag->front; my_chunk; ) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd
#endif
                    for (i = 0; i < my_chunk->size; i++) {
                        my_chunk->array[i].vx +=
                                 (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.right_back_top
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.left_back_top
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.right_front_top
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_x.left_front_top
                               + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.right_back_down
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.left_back_down
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.right_front_down
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_x.left_front_down;
                        my_chunk->array[i].vy +=
                                 (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.right_back_top
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.left_back_top
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.right_front_top
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_y.left_front_top
                               + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.right_back_down
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.left_back_down
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.right_front_down
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_y.left_front_down;
                        my_chunk->array[i].vz +=
                                 (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.right_back_top
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.left_back_top
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.right_front_top
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (     my_chunk->array[i].dz) * E_field[j].field_z.left_front_top
                               + (     my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.right_back_down
                               + (1. - my_chunk->array[i].dx) * (     my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.left_back_down
                               + (     my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.right_front_down
                               + (1. - my_chunk->array[i].dx) * (1. - my_chunk->array[i].dy) * (1. - my_chunk->array[i].dz) * E_field[j].field_z.left_front_down;
                    }
                    for (i = 0; i < my_chunk->size; i++) {
                        x = ((j / ncz) / ncy        ) + my_chunk->array[i].dx + my_chunk->array[i].vx;
                        y = ((j / ncz) & ncyminusone) + my_chunk->array[i].dy + my_chunk->array[i].vy;
                        z = (j & nczminusone        ) + my_chunk->array[i].dz + my_chunk->array[i].vz;
                        ic_x = (int)x - (x < 0.);
                        ic_y = (int)y - (y < 0.);
                        ic_z = (int)z - (z < 0.);
                        i_cell = COMPUTE_I_CELL_3D(icell_param1, icell_param2, ic_x & ncxminusone, ic_y & ncyminusone, ic_z & nczminusone);
                        bag_push(&(particlesNext[thread_id][i_cell]), (particle){ .dx = (float)(x - ic_x), .dy = (float)(y - ic_y), .dz = (float)(z - ic_z),
                            .vx = my_chunk->array[i].vx, .vy = my_chunk->array[i].vy, .vz = my_chunk->array[i].vz }, thread_id);
#ifdef PIC_VERT_OPENMP_4_0
                        #pragma omp simd aligned(coeffs_x, coeffs_y, coeffs_z, signs_x, signs_y, signs_z:VEC_ALIGN)
#endif
                        for (corner = 0; corner < NB_CORNERS_3D; corner++) {
                            charge_accu[offset + NB_CORNERS_3D * i_cell + corner] +=
                                (coeffs_x[corner] + signs_x[corner] * (x - ic_x)) *
                                (coeffs_y[corner] + signs_y[corner] * (y - ic_y)) *
                                (coeffs_z[corner] + signs_z[corner] * (z - ic_z));
                        }
                    }
                    next_chunk = my_chunk->next;
                    chunk_free(my_chunk, thread_id);
                    my_chunk = next_chunk;
                }
            }
            time_mark2 = omp_get_wtime();
            #pragma omp for private(i, j, corner, offset)
            for (j = 0; j < num_cells_3d; j++) {
                particles[j] = particlesNext[0][j];
                bag_init(&(particlesNext[0][j]), thread_id);
                for (corner = 0; corner < NB_CORNERS_3D; corner++)
                    reduced_charge_accu[NB_CORNERS_3D * j + corner] = charge_accu[NB_CORNERS_3D * j + corner];
                for (i = 1; i < num_threads; i++) {
                    bag_append(&(particles[j]), &(particlesNext[i][j]), thread_id);
                    offset = i * NB_CORNERS_3D * num_cells_3d;
                    for (corner = 0; corner < NB_CORNERS_3D; corner++)
                        reduced_charge_accu[NB_CORNERS_3D * j + corner] += charge_accu[offset + NB_CORNERS_3D * j + corner];
                }
            }
        } // End parallel region
        time_mark3 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif
        
        // Converts accumulator to rho
        convert_charge_to_rho_3d_per_per(reduced_charge_accu, ncx, ncy, ncz, charge_factor, rho_3d);
        mpi_reduce_rho_3d(mpi_world_size, send_buf, recv_buf, ncx, ncy, ncz, rho_3d);
        time_mark4 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        for (i = 0; i < ncx + 1; i++)
            for (j = 0; j < ncy + 1; j++)
                for (k = 0; k < ncz + 1; k++)
                    q_times_rho[i][j][k] = q * rho_3d[i][j][k];
        compute_E_from_rho_3d_fft(solver, q_times_rho, Ex, Ey, Ez);
        accumulate_field_3d(Ex, Ey, Ez, ncx, ncy, ncz, x_field_factor, y_field_factor, z_field_factor, E_field);
        time_mark5 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after update v / x / deposit
        diag_speed[i_time][2] = time_mark3; // after append
        diag_speed[i_time][3] = time_mark4; // after all_reduce
        diag_speed[i_time][4] = time_mark5; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_particle_loop = 0.;
    time_append        = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_particle_loop += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_append        += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_mpi_allreduce += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_poisson       += diag_speed[i_time][4] - diag_speed[i_time][3];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    diag_energy_and_speed_chunkbags(mpi_rank,
        "diag_lee_8corners.txt",   num_iteration, diag_energy_size, diag_energy,
        "diag_speed_8corners.txt", num_iteration, diag_speed_size,  diag_speed);
    print_time_chunkbags(mpi_rank, mpi_world_size, nb_particles, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_particle_loop, time_append, time_mpi_allreduce, time_poisson);
    
    free(params);
    free(speed_params);
    deallocate_3d_array(q_times_rho, ncx+1, ncy+1, ncz+1);
    deallocate_3d_array(rho_3d, ncx+1, ncy+1, ncz+1);
    deallocate_3d_array(Ex, ncx+1, ncy+1, ncz+1);
    deallocate_3d_array(Ey, ncx+1, ncy+1, ncz+1);
    deallocate_3d_array(Ez, ncx+1, ncy+1, ncz+1);
    deallocate_matrix(diag_energy, num_iteration, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration, diag_speed_size);
    free(charge_accu);
    free(reduced_charge_accu);

    free(send_buf);
    free(recv_buf);
    free_poisson_3d(&solver);
    free_field_3d(E_field);
    pic_vert_free_RNG();
    MPI_Finalize();
    
    return 0;
}

