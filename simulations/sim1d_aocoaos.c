/**
 * PIC simulations in 1d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                              // functions cos, log
#include <mpi.h>                               // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                               // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                               // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                             // functions printf, fprintf (output strings on a stream), sprintf
                                               // constant  stderr (standard error output stream)
#include <stdlib.h>                            // functions malloc, free ((de)allocate memory)
                                               //           exit (error handling)
                                               // constant  EXIT_FAILURE (error handling)
                                               // type      size_t
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>                          // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                               // functions PAPI_read_counters
                                               // type      long_long
#    include "papi_handlers.h"                 // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"                     // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"                       // function  normL2_field_1d
#include "initial_distributions.h"             // constants PARAXIAL_1D
#include "math_functions.h"                    // functions sqr, lowest_even_number_greater_or_equal_than
#include "matrix_functions.h"                  // functions allocate_matrix, deallocate_matrix, allocate_array, deallocate_array
#include "meshes.h"                            // type      cartesian_mesh_1d
                                               // function  create_mesh_1d
#include "output.h"                            // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"                        // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG, NB_PARTICLE
#include "particle_type_chunkbags_of_aos_1d.h" // types     particle, chunk, bag
                                               // function  bag_init, bag_push, bag_append, init_freelists
#include "poisson_solvers.h"                   // function  compute_E_from_rho_1d_trapezoidal
#include "random.h"                            // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG, pic_vert_next_random_double
#include "rho.h"                               // constant  NB_CORNERS_1D
                                               // functions mpi_reduce_rho_1d, reset_charge_1d_accumulator, convert_charge_to_rho_1d_per_per

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh_creation_particles, the mesh on which to create particles.
 * @param[in]  mesh, the full mesh on which we're working (free boundaries, has to be bigger).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[num_particle] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_1d_paraxial(int mpi_world_size, unsigned int num_particle,
        cartesian_mesh_1d mesh_creation_particles, cartesian_mesh_1d mesh,
        double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t j;
    int i_cell;
    double x, vx;
    speeds_generator_1d speeds_generator = speed_generators_1d[PARAXIAL_1D];
    
    const double x_range = mesh_creation_particles.x_max - mesh_creation_particles.x_min;
    
    *weight = (float)1. / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        x = x_range * pic_vert_next_random_double() + mesh_creation_particles.x_min;
        x = (x - mesh.x_min) / mesh.delta_x;
        (*speeds_generator)(speed_params, &vx);
        i_cell = (int)x;
        bag_push(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .vx = vx }, 0);
    }
}

/*****************************************************************************
 *                             Simulation 1d                                 *
 *****************************************************************************/

// All the simulations in this file follow the 'array of structures'
// data layout. The structure doesn't contains the weight, because it is known
// to be a constant.

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5;
    double time_particle_loop, time_append, time_mpi_allreduce, time_poisson;
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Type of the simulation
    const unsigned char sim_distrib = PARAXIAL_1D;
    const char sim_initial = INIT_NOFILE;

    // Electric parameters, for the Paraxial 1d :
    const double epsilon = 0.01;
    double *params = allocate_array(0);
    
    // Other physical parameters
    const double thermal_speed =  0.0727518214392; // thermal speed
    double *speed_params;
    speed_params = malloc(1 * sizeof(double));
    speed_params[0] = thermal_speed;
    
    // Meshes
    // The particles must be created in [-0.75 ; 0.75]...
    const double le075 = 0.75;
    const double x_min_creation_particles = -le075;
    const double x_max_creation_particles =  le075;
    const int ncx_creation_particles = 64;
    cartesian_mesh_1d mesh_creation_particles = create_mesh_1d(ncx_creation_particles, x_min_creation_particles, x_max_creation_particles);

    // ...however, because we have free boundaries, they might go out of this initial mesh, hence the need for a bigger mesh.
    const double x_min = -1.5;
    const double x_max =  1.5;
    const int ncx = lowest_even_number_greater_or_equal_than((int)((x_max - x_min) / (x_max_creation_particles - x_min_creation_particles) * ncx_creation_particles));
    cartesian_mesh_1d mesh = create_mesh_1d(ncx, x_min, x_max);

    const int num_cells_1d = ncx;

    // Vectorization of the deposit
    int corner;
/*
 *    dx
 *   <———>
 *   =———|——————x
 *   =xxxO======x
 *   =———|——————x
 *
 * The "=" corner is the left corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "=" area. This fraction is equal to
 * (1. - dx), hence the LEFT coefficients.
 * The "x" corner is the right corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "x" area. This fraction is equal to
 * (     dx), hence the RIGHT coefficients.
 */
    // Space coeffs                                                             LEFT  RIGHT
    const float coeffs_x[NB_CORNERS_1D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  0.};
    const float  signs_x[NB_CORNERS_1D] __attribute__((aligned(VEC_ALIGN))) = { -1.,  1.};
    
    // Simulation parameters
    const double delta_t             = DELTA_T;          // time step
    const double dt_over_dx          = delta_t / mesh.delta_x;
    const unsigned int num_iteration = NB_ITER;          // number of time steps
    char simulation_name[42]     = "Highly oscillatory Vlasov equation 1d";
    char data_structure_name[99] = "Array of Chunkbags of Array of Structures (num_threads private / cell)";
    char sort_name[42]           = "always sort";
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = allocate_array(ncx);
    double* recv_buf = allocate_array(ncx);
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    int thread_id;
    int offset;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    const int NB_BAGS_PER_CELL = num_threads; // Total number of chunkbags.
    
    // Temporary variables.
    double x;            // store the new position values
    int ic_x;            // store the new position index values
    size_t i, j, i_time; // loop indices
    
    // The following matrices are (ncx+1) arrays, with the periodicity :
    //     M[ncx] = M[0]
    // rho the charge density
    // Ex the electric field on the x-axis
    double* rho_1d = allocate_array(ncx+1);
    double* Ex = allocate_array(ncx+1);
    double* E_field = allocate_array(ncx+1);
    
    /* For each cell, the 2 corners values ; for vectorization, each thread has its own copy */
    double* charge_accu;
    if (posix_memalign((void**)&charge_accu, VEC_ALIGN, num_cells_1d * NB_CORNERS_1D * num_threads * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize charge_accu.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(charge_accu, VEC_ALIGN);
#else
    charge_accu = __builtin_assume_aligned(charge_accu, VEC_ALIGN);
#endif
    
    // Diagnostic energy.
    double val_ee, t;
    const int diag_energy_size = 3;
    const int diag_speed_size  = 5;
    double** diag_energy = allocate_matrix(num_iteration, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration, diag_speed_size);
    
    // Particle data structure.
    init_freelists(NB_PARTICLE);
    // Bags in cell i are taken from free list i % num_threads to
    // equally distribute the pressure on the different free lists.
    bag* particles = malloc(num_cells_1d * sizeof(bag));
    for (i = 0; i < num_cells_1d; i++)
        bag_init(&(particles[i]), i % num_threads);
    bag** particlesNext = malloc(NB_BAGS_PER_CELL * sizeof(bag*));
    for (j = 0; j < NB_BAGS_PER_CELL; j++) {
        particlesNext[j] = malloc(num_cells_1d * sizeof(bag));
        for (i = 0; i < num_cells_1d; i++)
            bag_init(&(particlesNext[j][i]), i % num_threads);
    }
    bag* chunk_bag;
    chunk* next_chunk;
    chunk* my_chunk;
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    if (sim_initial == INIT_READ) {
    // WARNING : if you read NB_PARTICLE from a file, the particle loop will not
    // be vectorized with gcc because it doesn't know the value of NB_PARTICLE !
        char filename[30];
        sprintf(filename, "initial_particles_%dkk.dat", NB_PARTICLE / 1000000);
        FILE* file_read_particles = fopen(filename, "r");
        if (!file_read_particles) {
            printf("%s doesn't exist.\n", filename);
            exit(EXIT_FAILURE);
        }
        int throw_that_number_x;
        unsigned int throw_that_number_also;
        if (fscanf(file_read_particles, "%d %u", &throw_that_number_x,
                &throw_that_number_also) < 2) {
            printf("%s should begin with ncx NB_PARTICLE.\n", filename);
            exit(EXIT_FAILURE);
        }
        if (throw_that_number_x != ncx) {
            printf("I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
            exit(EXIT_FAILURE);
        }
        if (throw_that_number_also != NB_PARTICLE) {
            fprintf(stderr, "I expected NB_PARTICLE = %d but found %d in the input file.\n", NB_PARTICLE, throw_that_number_also);
            exit(EXIT_FAILURE);
        }
        weight = (float)1. / ((float)mpi_world_size * (float)NB_PARTICLE);
        time_start = omp_get_wtime();
        float dx;
        double vx;
        for (i = 0; i < NB_PARTICLE; i++) {
            if (fscanf(file_read_particles, "%d %f %lf", &ic_x, &dx, &vx) < 3) {
                fprintf(stderr, "I expected %d particles but there are less in the input file.\n", NB_PARTICLE);
                exit(EXIT_FAILURE);
            }
            bag_push(&(particles[ic_x]), (particle){ .dx = dx, .vx = vx }, 0);
        }
        fclose(file_read_particles);
        if (mpi_rank == 0)
            printf("Read time (%d particles) : %g sec\n", NB_PARTICLE, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
        create_particle_array_1d_paraxial(mpi_world_size, NB_PARTICLE, mesh_creation_particles, mesh,
            params, speed_params, &weight, &particles);
        if (mpi_rank == 0)
            printf("Creation time (%d particles) : %g sec\n", NB_PARTICLE, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%dkk.dat", NB_PARTICLE / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d\n", ncx);
            fprintf(file_write_particles, "%d\n", NB_PARTICLE);
            for (j = 0; j < num_cells_1d; j++) {
                chunk_bag = &(particles[j]);
                bag_store_size_of_back(chunk_bag);
                for (my_chunk = chunk_bag->front; my_chunk; my_chunk = my_chunk->next) {
                    for (i = 0; i < my_chunk->size; i++) {
                        fprintf(file_write_particles, "%ld %.*g %.*g\n", j,
                          FLT_DECIMAL_DIG, my_chunk->array[i].dx,
                          DBL_DECIMAL_DIG, my_chunk->array[i].vx);
                    }
                }
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
/*
    // The particles were initialized with icells in [0 ; ncx_creation_particles] which corresponds to
    // [-le075 ; le075] but are now in [-1 ; 1] which corresponds to a bigger interval [0 ; ncx].
    // Thus we have to move them all by (ncx - ncx_creation_particles) / 2.
    int shift = (ncx - ncx_creation_particles) / 2;
    bag tmp_bag;
    // Don't use a size_t because 0 - 1 is still >= 0, but the maximum integer possible...
    for (int i_bag = ncx_creation_particles - 1; i_bag >= 0; i_bag--) {
        tmp_bag = particles[i_bag + shift];
        particles[i_bag + shift] = particles[i_bag];
        particles[i_bag] = tmp_bag;
    }
*/
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x);
    // We just use the electric fields to update the speed, with always the same multiply.
    const double x_field_factor = delta_t * dt_over_dx;
    const double eps_factor     = delta_t * dt_over_dx / epsilon;
    
  if (mpi_rank == 0) {
    printf("#CHUNK_SIZE = %d\n", CHUNK_SIZE);
    printf("#VEC_ALIGN = %d\n", VEC_ALIGN);
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_1d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#NB_PARTICLE = %d\n", NB_PARTICLE);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    reset_charge_1d_accumulator(ncx, num_threads, charge_accu);
    // Computes rho at initial time.
    #pragma omp parallel private(thread_id, offset)
    {
        thread_id = omp_get_thread_num();
        offset = thread_id * NB_CORNERS_1D * num_cells_1d;
        #pragma omp for private(i, j, chunk_bag, my_chunk, corner)
        for (j = 0; j < num_cells_1d; j++) {
            chunk_bag = &(particles[j]);
            bag_store_size_of_back(chunk_bag);
            for (my_chunk = chunk_bag->front; my_chunk; my_chunk = my_chunk->next) {
                for (i = 0; i < my_chunk->size; i++) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd aligned(coeffs_x, signs_x:VEC_ALIGN)
#endif
                    for (corner = 0; corner < NB_CORNERS_1D; corner++) {
                        charge_accu[offset + NB_CORNERS_1D * j + corner] +=
                            coeffs_x[corner] + signs_x[corner] * my_chunk->array[i].dx;
                    }
                }
            }
        }
    } // End parallel region
    convert_charge_to_rho_1d_per_per(charge_accu, num_threads, ncx, charge_factor, rho_1d);
    mpi_reduce_rho_1d(mpi_world_size, send_buf, recv_buf, ncx, rho_1d);
    
    // Computes E at initial time.
    compute_E_from_rho_1d_trapezoidal(mesh, rho_1d, Ex);
    for (i = 0; i < ncx + 1; i++)
        E_field[i] = x_field_factor * Ex[i];
    
/*    
  if (mpi_rank == 0) {
    // Test the initial rho.
    FILE* file_diag_rho = fopen("test_rho_1d.dat", "w");
    for (i = 0; i < ncx; i++)
        fprintf(file_diag_rho, "%ld %f\n", i, rho_1d[i]);
    fclose(file_diag_rho);
    // Test the initial Electric field.
    FILE* file_diag_Ex = fopen("test_Ex_1d.dat", "w");
    for (i = 0; i < ncx; i++)
        fprintf(file_diag_Ex, "%ld %g\n", i, Ex[i]);
    fclose(file_diag_Ex);
  }
    MPI_Finalize();
    exit(0);
*/
    
    // Computes speeds half time-step backward (leap-frog method).
    // WARNING : starting from here, v doesn't represent the speed, but speed * dt / dx.
    #pragma omp parallel for private(i, j, chunk_bag, my_chunk)
    for (j = 0; j < num_cells_1d; j++) {
        chunk_bag = &(particles[j]);
        bag_store_size_of_back(chunk_bag);
        for (my_chunk = chunk_bag->front; my_chunk; my_chunk = my_chunk->next) {
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp simd
#endif
            for (i = 0; i < my_chunk->size; i++) {
                my_chunk->array[i].vx = my_chunk->array[i].vx * dt_over_dx - 0.5 * (
                                      (     my_chunk->array[i].dx) * E_field[j + 1]
                                    + (1. - my_chunk->array[i].dx) * E_field[j    ]
                                    - (x_min + (j + my_chunk->array[i].dx) * mesh.delta_x) * eps_factor);
            }
        }
    }
    
    /********************************************************************************************
     *                               Beginning of main time loop                                *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    FILE* file_write_particles = fopen("particles_with_time.dat", "w");
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics energy
        t = i_time * delta_t;
        val_ee = normL2_field_1d(mesh, Ex);
        diag_energy[i_time][0] = t;                 // time
        diag_energy[i_time][1] = 0.5 * log(val_ee); // E_field's log(L2-norm) (simulated)
        diag_energy[i_time][2] = val_ee;            // E_field's L2-norm (simulated)
        for (j = 0; j < num_cells_1d; j++) {
            chunk_bag = &(particles[j]);
            bag_store_size_of_back(chunk_bag);
            for (my_chunk = chunk_bag->front; my_chunk; my_chunk = my_chunk->next) {
                for (i = 0; i < my_chunk->size; i++) {
                    fprintf(file_write_particles, "%ld %.*g %.*g\n", j,
                      DBL_DECIMAL_DIG, x_min + (j + my_chunk->array[i].dx) * mesh.delta_x,
                      DBL_DECIMAL_DIG, my_chunk->array[i].vx);
                }
            }
        }
        fprintf(file_write_particles, "\n\n");
        
        time_mark1 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif

        reset_charge_1d_accumulator(ncx, num_threads, charge_accu);
        #pragma omp parallel private(thread_id, offset, my_chunk, chunk_bag, next_chunk)
        {
            thread_id = omp_get_thread_num();
            offset = thread_id * NB_CORNERS_1D * num_cells_1d;
            // Particle loop
            #pragma omp for private(i, j, corner, x, ic_x)
            for (j = 0; j < num_cells_1d; j++) {
                chunk_bag = &(particles[j]);
                bag_store_size_of_back(chunk_bag);
                for (my_chunk = chunk_bag->front; my_chunk; ) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd
#endif
                    for (i = 0; i < my_chunk->size; i++) {
                        my_chunk->array[i].vx +=
                                 (     my_chunk->array[i].dx) * E_field[j + 1]
                               + (1. - my_chunk->array[i].dx) * E_field[j    ]
                               - (x_min + (j + my_chunk->array[i].dx) * mesh.delta_x) * eps_factor;
                    }
                    for (i = 0; i < my_chunk->size; i++) {
                        x = j + my_chunk->array[i].dx + my_chunk->array[i].vx / epsilon;
                        ic_x = (int)x;
                        if (ic_x < 0 || ic_x >= ncx) {
                            fprintf(stderr, "A particle fell out of [0; ncx], ic_x = %d.\n", ic_x);
                            exit(EXIT_FAILURE);
                        }
                        bag_push(&(particlesNext[thread_id][ic_x]), (particle){ .dx = (float)(x - ic_x), .vx = my_chunk->array[i].vx }, thread_id);
#ifdef PIC_VERT_OPENMP_4_0
                        #pragma omp simd aligned(coeffs_x, signs_x:VEC_ALIGN)
#endif
                        for (corner = 0; corner < NB_CORNERS_1D; corner++) {
                            charge_accu[offset + NB_CORNERS_1D * ic_x + corner] +=
                                coeffs_x[corner] + signs_x[corner] * (x - ic_x);
                        }
                    }
                    next_chunk = my_chunk->next;
                    chunk_free(my_chunk, thread_id);
                    my_chunk = next_chunk;
                }
            }
            time_mark2 = omp_get_wtime();
            #pragma omp for private(i, j)
            for (j = 0; j < num_cells_1d; j++) {
                particles[j] = particlesNext[0][j];
                bag_init(&(particlesNext[0][j]), thread_id);
                for (i = 1; i < num_threads; i++)
                    bag_append(&(particles[j]), &(particlesNext[i][j]), thread_id);
            }
        } // End parallel region
        time_mark3 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif

        // Converts accumulator to rho
        convert_charge_to_rho_1d_per_per(charge_accu, num_threads, ncx, charge_factor, rho_1d);
        mpi_reduce_rho_1d(mpi_world_size, send_buf, recv_buf, ncx, rho_1d);
        time_mark4 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        compute_E_from_rho_1d_trapezoidal(mesh, rho_1d, Ex);
        for (i = 0; i < ncx + 1; i++)
            E_field[i] = x_field_factor * Ex[i];
        time_mark5 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after update v / x / deposit
        diag_speed[i_time][2] = time_mark3; // after append
        diag_speed[i_time][3] = time_mark4; // after all_reduce
        diag_speed[i_time][4] = time_mark5; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_particle_loop = 0.;
    time_append        = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_particle_loop += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_append        += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_mpi_allreduce += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_poisson       += diag_speed[i_time][4] - diag_speed[i_time][3];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    fclose(file_write_particles);
    diag_energy_and_speed_chunkbags(mpi_rank,
        "diag_lee_2corners.txt",   num_iteration, diag_energy_size, diag_energy,
        "diag_speed_2corners.txt", num_iteration, diag_speed_size,  diag_speed);
    print_time_chunkbags(mpi_rank, mpi_world_size, NB_PARTICLE, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_particle_loop, time_append, time_mpi_allreduce, time_poisson);
    
    free(speed_params);
    deallocate_array(rho_1d, ncx+1);
    deallocate_array(Ex, ncx+1);
    deallocate_array(E_field, ncx+1);
    deallocate_matrix(diag_energy, num_iteration, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration, diag_speed_size);
    free(charge_accu);

    free(send_buf);
    free(recv_buf);
    pic_vert_free_RNG();
    MPI_Finalize();
    
    return 0;
}

