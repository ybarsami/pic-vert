/**
 * PIC simulations in 2d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                  // functions cos, log
#include <mpi.h>                   // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                   // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                   // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                 // functions printf, fprintf (output strings on a stream), sprintf
                                   // constant  stderr (standard error output stream)
#include <stdlib.h>                // functions malloc, free ((de)allocate memory)
                                   //           exit (error handling)
                                   // constant  EXIT_FAILURE (error handling)
                                   // type      size_t
#include <string.h>                // function  strcmp
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>              // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                   // functions PAPI_read_counters
                                   // type      long_long
#    include "papi_handlers.h"     // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"         // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"           // function  normL2_field_2d, get_damping_values
#include "initial_distributions.h" // constants LANDAU_1D_PROJ2D, LANDAU_2D, KELVIN_HELMHOLTZ, TWO_STREAM_BERNIER,
                                   //           TWO_BEAMS_FIJALKOW, TWO_STREAM_1D_PROJ2D, TWO_STREAM_2D
#include "math_functions.h"        // functions sqr, max
#include "matrix_functions.h"      // functions allocate_matrix, deallocate_matrix, allocateMatrix, deallocateMatrix
#include "meshes.h"                // type      cartesian_mesh_2d
                                   // function  create_mesh_2d
#include "output.h"                // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"            // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG, NB_PARTICLE
#include "parameter_reader.h"      // type      simulation_parameters
                                   // constants STRING_NOT_SET, INT_NOT_SET, DOUBLE_NOT_SET
                                   // function  read_parameters_from_file
#include "particle_type_soa_2d.h"  // types     particle_sorter_oop_2d
                                   // functions create_particle_arrays_2d, new_particle_sorter_oop_2d, sort_particles_oop_2d
#include "poisson_solvers.h"       // type      poisson_2d_solver
                                   // function  new_poisson_2d_fft_solver, compute_E_from_rho_2d_fft, free_poisson_2d
#include "random.h"                // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG
#include "rho.h"                   // constant  NB_CORNERS_2D
                                   // functions mpi_reduce_rho_2d, reset_charge_2d_accumulator, convert_charge_to_rho_2d_per_per
#include "space_filling_curves.h"  // macros    COMPUTE_I_CELL_2D, ICX_FROM_I_CELL_2D, ICY_FROM_I_CELL_2D
                                   // constant  I_CELL_2D_TYPE, space_filling_curves_names

/*
 * Sets the values of the field accumulator according to Ex and Ey.
 * @param[in]  Ex[ncx+1][ncy+1] the electric field on the x-axis.
 * @param[in]  Ey[ncx+1][ncy+1] the electric field on the y-axis.
 * @param[in]  ncx mesh size on the x-axis.
 * @param[in]  ncy mesh size on the y-axis.
 * @param[out] E_fieldx[ncx+1][ncy+1] the electric field on the x-axis updated.
 * @param[out] E_fieldy[ncx+1][ncy+1] the electric field on the y-axis updated.
 */
void optimize_field_2d(double** Ex, double** Ey,
        int ncx, int ncy, double x_factor, double y_factor, double** E_fieldx, double** E_fieldy) {
    int i, j;
    
    for (i = 0; i < ncx + 1; i++) {
        for (j = 0; j < ncy + 1; j++) {
            E_fieldx[i][j] = Ex[i][j] * x_factor;
            E_fieldy[i][j] = Ey[i][j] * y_factor;
        }
    }
}

/*****************************************************************************
 *                             Simulation 2d                                 *
 *****************************************************************************/

#define SORT_IN_PLACE_COUNTING_SORT     0 // Time = O(n), Memory = nb_particles * sizeof(particle)
#define SORT_OUT_OF_PLACE_COUNTING_SORT 1 // Time = O(n), Memory = 2 * nb_particles * sizeof(particle)
#define SORT_QUICK_SORT                 2 // Time = O(n*log(n)), Memory = nb_particles * sizeof(particle)
                                          // Not implemented (multiple arrays to sort, cannot use qsort from stdlib).
#define SORT_NO_SORT                    3

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

// Strip-mining.
#ifndef STRIP_SIZE
#   define STRIP_SIZE 512
#endif

// Number of iterations between two consecutive sorts.
#ifndef NB_ITER_BETWEEN_SORT
#   define NB_ITER_BETWEEN_SORT 20
#endif

// Initial distribution
#ifndef INITIAL_DISTRIBUTION
#    define INITIAL_DISTRIBUTION LANDAU_2D
#endif

#ifndef THERMAL_SPEED
#    define THERMAL_SPEED 1.
#endif

// Perturbation.
#ifndef ALPHA
#   define ALPHA 0.01
#endif

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5;
    double time_sort, time_particle_loop, time_mpi_allreduce, time_poisson;
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Automatic values for the parameters.
    unsigned char sim_distrib = INITIAL_DISTRIBUTION; // Physical test case (LANDAU_1D_PROJ2D, TWO_BEAMS_FIJALKOW, LANDAU_2D,
                                                      // TWO_STREAM_2D, TWO_STREAM_BERNIER or TWO_STREAM_1D_PROJ2D).
    int ncx               = NCX;                      // Number of grid points, x-axis
    int ncy               = NCY;                      // Number of grid points, y-axis
    long int nb_particles = NB_PARTICLE;              // Number of particles
    int num_iteration     = NB_ITER;                  // Number of time steps
    double delta_t        = DELTA_T;                  // Time step
    double thermal_speed  = THERMAL_SPEED;            // Thermal speed
    double alpha   = ALPHA; // Landau perturbation amplitude
    double kmode_x = 0.5;   // Landau perturbation mode, x-axis
    double kmode_y = 0.5;   // Landau perturbation mode, y-axis
    
    // Read parameters from file.
    if (argc >= 2) {
        simulation_parameters parameters = read_parameters_from_file(argv[1], "2D");
        if (strcmp(parameters.sim_distrib_name, STRING_NOT_SET) != 0)
            sim_distrib = parameters.sim_distrib;
        if (parameters.ncx != INT_NOT_SET)
            ncx             = parameters.ncx;
        if (parameters.ncy != INT_NOT_SET)
            ncy             = parameters.ncy;
        if (parameters.nb_particles != INT_NOT_SET)
            nb_particles    = parameters.nb_particles;
        if (parameters.num_iteration != INT_NOT_SET)
            num_iteration   = parameters.num_iteration;
        if (parameters.delta_t != DOUBLE_NOT_SET)
            delta_t       = parameters.delta_t;
        if (parameters.thermal_speed != DOUBLE_NOT_SET)
            thermal_speed = parameters.thermal_speed;
        if (parameters.alpha != DOUBLE_NOT_SET)
            alpha   = parameters.alpha;
        if (parameters.kmode_x != DOUBLE_NOT_SET)
            kmode_x = parameters.kmode_x;
        if (parameters.kmode_y != DOUBLE_NOT_SET)
            kmode_y = parameters.kmode_y;
    } else
        printf("No parameter file was passed through the command line. I will use the default parameters.\n");
    
    // Random initialization or read from file.
    const char sim_initial = INIT_NOFILE;
    const char sim_sort    = SORT_OUT_OF_PLACE_COUNTING_SORT;
    
    // Spatial parameters for initial density function.
    double *params;
    if (sim_distrib == LANDAU_1D_PROJ2D || sim_distrib == TWO_BEAMS_FIJALKOW) {
        params = malloc(2 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
    } else if (sim_distrib == LANDAU_2D || sim_distrib == TWO_STREAM_2D) {
        params = malloc(3 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
    } else if (sim_distrib == TWO_STREAM_BERNIER) {
        params = malloc(7 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 0.;
        params[4] = 1.;
        params[5] = 1.;
        params[6] = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        params = malloc(5 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 3.5; // i_modes_x = 1, 2, 3
        params[4] = 3.5; // i_modes_y = 1, 2, 3
    } else
        params = malloc(0 * sizeof(double));
    
    // Velocity parameters for initial density function.
    double *speed_params;
    if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        speed_params = malloc(2 * sizeof(double));
        speed_params[0] = thermal_speed;
        speed_params[1] = 2. * sqrt(2.) * thermal_speed;
    } else {
        speed_params = malloc(1 * sizeof(double));
        speed_params[0] = thermal_speed;
    }
    
    // Mesh
    double x_min, y_min, x_max, y_max;
    const int num_cells_2d = ncx * ncy;
    if (sim_distrib == LANDAU_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 26. * PI;
        y_max = 1.;
    } else {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 2 * PI / kmode_y;
    }
    cartesian_mesh_2d mesh = create_mesh_2d(ncx, ncy, x_min, x_max, y_min, y_max);
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    const int ncxminusone = ncx - 1;
    const int ncyminusone = ncy - 1;

    // Vectorization of the deposit
    int corner;
/*
 *    dx
 *   <———>
 *   +——————————*
 *   |xxx|======|
 *   |———O——————| ^
 *   |***|++++++| |
 *   |***|++++++| |dy
 *   =——————————x v
 *
 * The "=" corner is the south west corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "=" area. This fraction is equal to
 * (1. - dx) * (1. - dy), hence the SW coefficients.
 * The "+" corner is the north west corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "+" area. This fraction is equal to
 * (1. - dx) * (     dy), hence the NW coefficients.
 * The "x" corner is the south east corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "x" area. This fraction is equal to
 * (     dx) * (1. - dy), hence the SE coefficients.
 * The "*" corner is the north east corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "*" area. This fraction is equal to
 * (     dx) * (     dy), hence the NE coefficients.
 */
    // Space coeffs                                                 SW   NW   SE   NE
    const float coeffs_x[4] __attribute__((aligned(VEC_ALIGN))) = {  1.,  1.,  0.,  0.};
    const float  signs_x[4] __attribute__((aligned(VEC_ALIGN))) = { -1., -1.,  1.,  1.};
    const float coeffs_y[4] __attribute__((aligned(VEC_ALIGN))) = {  1.,  0.,  1.,  0.};
    const float  signs_y[4] __attribute__((aligned(VEC_ALIGN))) = { -1.,  1., -1.,  1.};
    
    // Simulation parameters
    const double q             = -1.; // particle charge
    const double m             =  1.; // particle mass
    const double dt_q_over_m   = delta_t * q / m;
    const double dt_over_dx    = delta_t / mesh.delta_x;
    const double dt_over_dy    = delta_t / mesh.delta_y;
    const unsigned int sort_nb = NB_ITER_BETWEEN_SORT; // number of time steps between two sorts of the particles
    char simulation_name[42] = "Vlasov-Poisson 2d";
    char data_structure_name[42];
    char sort_name[42];
    sprintf(data_structure_name, "Structure of Arrays (%s)", space_filling_curves_names[I_CELL_2D_TYPE]);
    if (sim_sort == SORT_NO_SORT)
        sprintf(sort_name, "no sorting");
    else if (sim_sort == SORT_QUICK_SORT)
        sprintf(sort_name, "quick sort every %d", sort_nb);
    else if (sim_sort == SORT_OUT_OF_PLACE_COUNTING_SORT)
        sprintf(sort_name, "out of place counting sort every %d", sort_nb);
    else if (sim_sort == SORT_IN_PLACE_COUNTING_SORT)
        sprintf(sort_name, "in place counting sort every %d", sort_nb);
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = allocateMatrix(ncx, ncy);
    double* recv_buf = allocateMatrix(ncx, ncy);
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    int thread_id;
    int offset;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Temporary variables.
    double x, y;                                          // store the new position values
    int ic_x, ic_y;                                       // store the new position index values
    size_t i, j, i_time;                                  // loop indices
    double** q_times_rho = allocate_matrix(ncx+1, ncy+1); // to store q * rho = -rho
    
    // Strip-mining
    size_t big_i;
    const int strip_size = STRIP_SIZE;
    
    // The following three matrices are (ncx+1) * (ncy+1) arrays, with the periodicity :
    //     M[ncx][ . ] = M[0][.]
    //     M[ . ][ncy] = M[.][0]
    // rho the charge density
    // Ex the electric field on the x-axis
    // Ey the electric field on the y-axis
    double** rho_2d = allocate_matrix(ncx+1, ncy+1);
    double** Ex = allocate_matrix(ncx+1, ncy+1);
    double** Ey = allocate_matrix(ncx+1, ncy+1);
    // To avoid multiplications inside the particle loop.
    double** E_fieldx = allocate_matrix(ncx+1, ncy+1);
    double** E_fieldy = allocate_matrix(ncx+1, ncy+1);
    
    // Diagnostic.
    double kmode = 0.;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
            kmode = kmode_x;
            break;
        case LANDAU_2D:
            kmode = sqrt(sqr(kmode_x) + sqr(kmode_y));
            break;
    }
    damping_values* landau_values = get_damping_values(kmode);
    const double er         = landau_values->er;
    const double psi        = landau_values->psi;
    const double omega_real = landau_values->omega_real;
    const double omega_imag = landau_values->omega_imag;
    double exval_ee, val_ee, t;
    const double landau_mult_cstt = sqr(4. * alpha * er) * PI / kmode_x; // Landau
    const int diag_energy_size = 5;
    const int diag_speed_size  = 5;
    double** diag_energy = allocate_matrix(num_iteration, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration, diag_speed_size);
    
    // Poisson solver.
    poisson_2d_solver solver = new_poisson_2d_fft_solver(mesh);
    
    // Particle sorter.
    particle_sorter_oop_2d sorter_oop = new_particle_sorter_oop_2d(nb_particles, num_cells_2d);
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    /* Cell index of the particle in [|0 ; ncx*ncy|[ (see the drawing above) */
    int* i_cell;
    /* x_mapped - index_x, a number in [0 ; 1[       (see the drawing above) */
    float* dx;
    /* y_mapped - index_y, a number in [0 ; 1[       (see the drawing above) */
    float* dy;
    /* Speed of the particle on the x-axis */
    double* vx;
    /* Speed of the particle on the y-axis */
    double* vy;
    /* For each cell, the 4 corners values ; for vectorization, each thread has its own copy */
    double* charge_accu;
    if (posix_memalign((void**)&i_cell, VEC_ALIGN, nb_particles * sizeof(int))) {
        fprintf(stderr, "posix_memalign failed to initialize i_cell.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&dx, VEC_ALIGN, nb_particles * sizeof(float))) {
        fprintf(stderr, "posix_memalign failed to initialize dx.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&dy, VEC_ALIGN, nb_particles * sizeof(float))) {
        fprintf(stderr, "posix_memalign failed to initialize dy.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&vx, VEC_ALIGN, nb_particles * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize vx.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&vy, VEC_ALIGN, nb_particles * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize vy.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&charge_accu, VEC_ALIGN, num_cells_2d * NB_CORNERS_2D * num_threads * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize charge_accu.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(i_cell, VEC_ALIGN);
    __assume_aligned(dx, VEC_ALIGN);
    __assume_aligned(vx, VEC_ALIGN);
    __assume_aligned(dy, VEC_ALIGN);
    __assume_aligned(vy, VEC_ALIGN);
    __assume_aligned(charge_accu, VEC_ALIGN);
#else
    i_cell = __builtin_assume_aligned(i_cell, VEC_ALIGN);
    dx = __builtin_assume_aligned(dx, VEC_ALIGN);
    dy = __builtin_assume_aligned(dy, VEC_ALIGN);
    vx = __builtin_assume_aligned(vx, VEC_ALIGN);
    vy = __builtin_assume_aligned(vy, VEC_ALIGN);
    charge_accu = __builtin_assume_aligned(charge_accu, VEC_ALIGN);
#endif

#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
    /* index_x, a number in [|0 ; ncx |[             (see the drawing above) */
    short int* icx;
    /* index_y, a number in [|0 ; ncy |[             (see the drawing above) */
    short int* icy;
    if (posix_memalign((void**)&icx, VEC_ALIGN, nb_particles * sizeof(short int))) {
        fprintf(stderr, "posix_memalign failed to initialize icx.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&icy, VEC_ALIGN, nb_particles * sizeof(short int))) {
        fprintf(stderr, "posix_memalign failed to initialize icy.\n");
        exit(EXIT_FAILURE);
    }
#    ifdef __INTEL_COMPILER
    __assume_aligned(icx, VEC_ALIGN);
    __assume_aligned(icy, VEC_ALIGN);
#    else
    icx = __builtin_assume_aligned(icx, VEC_ALIGN);
    icy = __builtin_assume_aligned(icy, VEC_ALIGN);
#    endif
#else
    const int icx_param = ICX_PARAM_2D(ncx, ncy);
    const int icy_param = ICY_PARAM_2D(ncx, ncy);
#endif
    
    if (sim_initial == INIT_READ) {
        time_start = omp_get_wtime();
        read_particle_arrays_2d(mpi_world_size, nb_particles, mesh, &weight, &i_cell, &dx, &vx, &dy, &vy);
        if (mpi_rank == 0)
            printf("Read time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
        create_particle_arrays_2d(mpi_world_size, nb_particles, mesh, sim_distrib,
            params, speed_params, &weight, &i_cell, &icx, &dx, &vx, &icy, &dy, &vy);
        sort_particles_oop_2d(&sorter_oop, &i_cell, &icx, &dx, &vx, &icy, &dy, &vy); // Initial sort
#else
        create_particle_arrays_2d(mpi_world_size, nb_particles, mesh, sim_distrib,
            params, speed_params, &weight, &i_cell, &dx, &vx, &dy, &vy);
        sort_particles_oop_2d(&sorter_oop, &i_cell, &dx, &vx, &dy, &vy); // Initial sort
#endif
        if (mpi_rank == 0)
            printf("Creation time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%ldkk.dat", nb_particles / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d %d\n", ncx, ncy);
            fprintf(file_write_particles, "%ld\n", nb_particles);
            for (i = 0; i < nb_particles; i++) {
                fprintf(file_write_particles, "%d %.*g %.*g %.*g %.*g\n", i_cell[i],
                  FLT_DECIMAL_DIG, dx[i], FLT_DECIMAL_DIG, dy[i],
                  DBL_DECIMAL_DIG, vx[i], DBL_DECIMAL_DIG, vy[i]);
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x * mesh.delta_y);
    // We just use the electric fields to update the speed, with always the same multiply.
    const double x_field_factor = dt_q_over_m * dt_over_dx;
    const double y_field_factor = dt_q_over_m * dt_over_dy;
    
  if (mpi_rank == 0) {
    printf("#STRIP_SIZE = %d\n", STRIP_SIZE);
    printf("#VEC_ALIGN = %d\n", VEC_ALIGN);
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#alpha = %.*g\n", DBL_DECIMAL_DIG, alpha);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#y_min = %.*g\n", DBL_DECIMAL_DIG, y_min);
    printf("#y_max = %.*g\n", DBL_DECIMAL_DIG, y_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_2d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
//    printf("#y_field_factor = %.*g\n", DBL_DECIMAL_DIG, y_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#ncy = %d\n", ncy);
    printf("#nb_particles = %ld\n", nb_particles);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    reset_charge_2d_accumulator(ncx, ncy, num_threads, charge_accu);
    // Computes rho at initial time.
    #pragma omp parallel private(thread_id, offset)
    {
        thread_id = omp_get_thread_num();
        offset = thread_id * NB_CORNERS_2D * num_cells_2d;
        #pragma omp for private(i, corner)
        for (i = 0; i < nb_particles; i++) {
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp simd aligned(dx, dy, coeffs_x, coeffs_y, signs_x, signs_y:VEC_ALIGN)
#endif
            for (corner = 0; corner < NB_CORNERS_2D; corner++) {
                charge_accu[offset + NB_CORNERS_2D * i_cell[i] + corner] +=
                    (coeffs_x[corner] + signs_x[corner] * dx[i]) *
                    (coeffs_y[corner] + signs_y[corner] * dy[i]);
            }
        }
    } // End parallel region
    convert_charge_to_rho_2d_per_per(charge_accu, num_threads, ncx, ncy, charge_factor, rho_2d);
    mpi_reduce_rho_2d(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
    
    // Computes E at initial time.
    for (i = 0; i < ncx + 1; i++)
        for (j = 0; j < ncy + 1; j++)
            q_times_rho[i][j] = q * rho_2d[i][j];
    compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
    optimize_field_2d(Ex, Ey, ncx, ncy, x_field_factor, y_field_factor, E_fieldx, E_fieldy);
    
    // Computes speeds half time-step backward (leap-frog method).
    // WARNING : starting from here, v doesn't represent the speed, but speed * dt / dx.
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
    #pragma omp parallel for private(i, ic_x, ic_y) firstprivate(dt_over_dx, dt_over_dy)
#else
    #pragma omp parallel for private(i, ic_x, ic_y) firstprivate(dt_over_dx, dt_over_dy, icx_param, icy_param)
#endif
    for (i = 0; i < nb_particles; i++) {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
        ic_x = icx[i];
        ic_y = icy[i];
#else
        ic_x = ICX_FROM_I_CELL_2D(icx_param, i_cell[i]);
        ic_y = ICY_FROM_I_CELL_2D(icy_param, i_cell[i]);
#endif
        vx[i] = vx[i] * dt_over_dx - 0.5 * (
              (     dx[i]) * (     dy[i]) * E_fieldx[ic_x + 1][ic_y + 1]
            + (1. - dx[i]) * (     dy[i]) * E_fieldx[ic_x    ][ic_y + 1]
            + (     dx[i]) * (1. - dy[i]) * E_fieldx[ic_x + 1][ic_y    ]
            + (1. - dx[i]) * (1. - dy[i]) * E_fieldx[ic_x    ][ic_y    ]);
        vy[i] = vy[i] * dt_over_dy - 0.5 * (
              (     dx[i]) * (     dy[i]) * E_fieldy[ic_x + 1][ic_y + 1]
            + (1. - dx[i]) * (     dy[i]) * E_fieldy[ic_x    ][ic_y + 1]
            + (     dx[i]) * (1. - dy[i]) * E_fieldy[ic_x + 1][ic_y    ]
            + (1. - dx[i]) * (1. - dy[i]) * E_fieldy[ic_x    ][ic_y    ]);
    }
    
    /********************************************************************************************
     *                               Beginning of main time loop                                *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics
        t = i_time * delta_t;
        exval_ee = landau_mult_cstt * exp(2. * omega_imag * t) *
               (0.5 + 0.5 * cos(2. * (omega_real * t - psi)));
        val_ee = normL2_field_2d(mesh, Ex) + normL2_field_2d(mesh, Ey);
        diag_energy[i_time][0] = t;                   // time
        diag_energy[i_time][1] = 0.5 * log(val_ee);   // Ex's log(L2-norm) (simulated)
        diag_energy[i_time][2] = 0.5 * log(exval_ee); // Ex's log(L2-norm) (expected)
        diag_energy[i_time][3] = val_ee;              // Ex's L2-norm (simulated)
        diag_energy[i_time][4] = exval_ee;            // Ex's L2-norm (expected)
        
        time_mark1 = omp_get_wtime();
        
        // Sorts the particles to be faster
        if (i_time % sort_nb == 0 && i_time > 0)
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
            sort_particles_oop_2d(&sorter_oop, &i_cell, &icx, &dx, &vx, &icy, &dy, &vy);
#else
            sort_particles_oop_2d(&sorter_oop, &i_cell,       &dx, &vx,       &dy, &vy);
#endif
        time_mark2 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif
        
        reset_charge_2d_accumulator(ncx, ncy, num_threads, charge_accu);
        #pragma omp parallel private(thread_id, offset)
        {
            thread_id = omp_get_thread_num();
            offset = thread_id * NB_CORNERS_2D * num_cells_2d;
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
            #pragma omp for private(big_i, i, corner, x, y, ic_x, ic_y) firstprivate(ncxminusone, ncyminusone, icell_param)
#else
            #pragma omp for private(big_i, i, corner, x, y, ic_x, ic_y) firstprivate(ncxminusone, ncyminusone, icell_param, icx_param, icy_param)
#endif
            for (big_i = 0; big_i < nb_particles; big_i += strip_size) {
                // Update v
                for (i = big_i; i < min(nb_particles, big_i + strip_size); i++) {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
                    ic_x = icx[i];
                    ic_y = icy[i];
#else
                    ic_x = ICX_FROM_I_CELL_2D(icx_param, i_cell[i]);
                    ic_y = ICY_FROM_I_CELL_2D(icy_param, i_cell[i]);
#endif
                    vx[i] += (     dx[i]) * (     dy[i]) * E_fieldx[ic_x + 1][ic_y + 1]
                           + (1. - dx[i]) * (     dy[i]) * E_fieldx[ic_x    ][ic_y + 1]
                           + (     dx[i]) * (1. - dy[i]) * E_fieldx[ic_x + 1][ic_y    ]
                           + (1. - dx[i]) * (1. - dy[i]) * E_fieldx[ic_x    ][ic_y    ];
                    vy[i] += (     dx[i]) * (     dy[i]) * E_fieldy[ic_x + 1][ic_y + 1]
                           + (1. - dx[i]) * (     dy[i]) * E_fieldy[ic_x    ][ic_y + 1]
                           + (     dx[i]) * (1. - dy[i]) * E_fieldy[ic_x + 1][ic_y    ]
                           + (1. - dx[i]) * (1. - dy[i]) * E_fieldy[ic_x    ][ic_y    ];
                }
                // Update x
#if defined(PIC_VERT_OPENMP_4_0) && defined (PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS)
                #pragma omp simd aligned(i_cell, icx, icy, dx, dy, vx, vy:VEC_ALIGN)
#elif defined(PIC_VERT_OPENMP_4_0)
                #pragma omp simd aligned(i_cell, dx, dy, vx, vy:VEC_ALIGN)
#endif
                for (i = big_i; i < min(nb_particles, big_i + strip_size); i++) {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
                    x = icx[i] + dx[i] + vx[i];
                    y = icy[i] + dy[i] + vy[i];
#else
                    x = ICX_FROM_I_CELL_2D(icx_param, i_cell[i]) + dx[i] + vx[i];
                    y = ICY_FROM_I_CELL_2D(icy_param, i_cell[i]) + dy[i] + vy[i];
#endif
                    ic_x = (int)x - (x < 0.);
                    ic_y = (int)y - (y < 0.);
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
                    icx[i] = ic_x & ncxminusone;
                    icy[i] = ic_y & ncyminusone;
#endif
                    dx[i] = (float)(x - ic_x);
                    dy[i] = (float)(y - ic_y);
                    i_cell[i] = COMPUTE_I_CELL_2D(icell_param, ic_x & ncxminusone, ic_y & ncyminusone);
                }
                // Deposit
                for (i = big_i; i < min(nb_particles, big_i + strip_size); i++) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd aligned(i_cell, dx, dy, coeffs_x, coeffs_y, signs_x, signs_y:VEC_ALIGN)
#endif
                    for (corner = 0; corner < NB_CORNERS_2D; corner++) {
                        charge_accu[offset + NB_CORNERS_2D * i_cell[i] + corner] +=
                            (coeffs_x[corner] + signs_x[corner] * dx[i]) *
                            (coeffs_y[corner] + signs_y[corner] * dy[i]);
                    }
                }
            }
        } // End parallel region
        time_mark3 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif
        
        // Converts accumulator to rho
        convert_charge_to_rho_2d_per_per(charge_accu, num_threads, ncx, ncy, charge_factor, rho_2d);
        mpi_reduce_rho_2d(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
        time_mark4 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        for (i = 0; i < ncx + 1; i++)
            for (j = 0; j < ncy + 1; j++)
                q_times_rho[i][j] = q * rho_2d[i][j];
        compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
        optimize_field_2d(Ex, Ey, ncx, ncy, x_field_factor, y_field_factor, E_fieldx, E_fieldy);
        time_mark5 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after sort
        diag_speed[i_time][2] = time_mark3; // after particle loop
        diag_speed[i_time][3] = time_mark4; // after all_reduce
        diag_speed[i_time][4] = time_mark5; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_sort          = 0.;
    time_particle_loop = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_sort          += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_particle_loop += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_mpi_allreduce += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_poisson       += diag_speed[i_time][4] - diag_speed[i_time][3];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    diag_energy_and_speed_sorting(mpi_rank,
        "diag_lee_4corners-opt-1loop.txt",   num_iteration, diag_energy_size, diag_energy,
        "diag_speed_4corners-opt-1loop.txt", num_iteration, diag_speed_size,  diag_speed);
    print_time_sorting(mpi_rank, mpi_world_size, nb_particles, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_particle_loop, 0., 0., time_sort, time_mpi_allreduce, time_poisson);
    
    free(params);
    free(speed_params);
    deallocate_matrix(q_times_rho, ncx+1, ncy+1);
    deallocate_matrix(rho_2d, ncx+1, ncy+1);
    deallocate_matrix(Ex, ncx+1, ncy+1);
    deallocate_matrix(Ey, ncx+1, ncy+1);
    deallocate_matrix(E_fieldx, ncx+1, ncy+1);
    deallocate_matrix(E_fieldy, ncx+1, ncy+1);
    deallocate_matrix(diag_energy, num_iteration, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration, diag_speed_size);
    free(i_cell);
    free(dx);
    free(dy);
    free(vx);
    free(vy);
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
    free(icx);
    free(icy);
#endif
    free(charge_accu);
    
    free(send_buf);
    free(recv_buf);
    free_poisson_2d(&solver);
    free_particle_sorter_oop_2d(sorter_oop, num_cells_2d);
    pic_vert_free_RNG();
    MPI_Finalize();
    
    return 0;
}

