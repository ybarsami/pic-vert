/**
 * PIC simulations in 2d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                                         // functions cos, log
#include <mpi.h>                                          // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                                          // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                                          // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                                        // functions printf, fprintf (output strings on a stream), sprintf
                                                          // constant  stderr (standard error output stream)
#include <stdlib.h>                                       // functions malloc, free ((de)allocate memory)
                                                          //           exit (error handling)
                                                          // constant  EXIT_FAILURE (error handling)
                                                          // type      size_t
#include <string.h>                                       // function  strcmp
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>                                     // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                                          // functions PAPI_read_counters
                                                          // type      long_long
#    include "papi_handlers.h"                            // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"                                // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"                                  // function  normL2_field_2d, get_damping_values
#include "fields.h"                                       // type      field_2d
                                                          // functions create_field_2d, free_field_2d, accumulate_field_2d
#include "hdf5_io.h"                                      // function  plot_f_cartesian_mesh_2d
#include "initial_distributions.h"                        // constants LANDAU_1D_PROJ2D, LANDAU_2D, KELVIN_HELMHOLTZ, TWO_STREAM_BERNIER,
                                                          //           TWO_BEAMS_FIJALKOW, TWO_STREAM_1D_PROJ2D, TWO_STREAM_2D
#include "math_functions.h"                               // functions sqr, min
#include "matrix_functions.h"                             // functions allocate_matrix, deallocate_matrix, allocateMatrix, deallocateMatrix,
                                                          //           allocate_aligned_int_array_array, deallocate_aligned_int_array_array
#include "meshes.h"                                       // type      cartesian_mesh_2d, tiled_cartesian_mesh_2d
                                                          // function  create_mesh_2d, tiled_create_mesh_2d
#include "output.h"                                       // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"                                   // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG, NB_PARTICLE
#include "parameter_reader.h"                             // type      simulation_parameters
                                                          // constants STRING_NOT_SET, INT_NOT_SET, DOUBLE_NOT_SET
                                                          // function  read_parameters_from_file
#include "particle_type_concurrent_chunkbags_of_soa_2d.h" // types     chunk, bag
                                                          // functions create_particle_array_2d, init_all_chunks, bag_push_concurrent, bag_push_serial,
                                                          //           bag_init, bag_append
#include "poisson_solvers.h"                              // type      poisson_2d_solver
                                                          // function  new_poisson_2d_fft_solver, compute_E_from_rho_2d_fft, free_poisson_2d
#include "random.h"                                       // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG
#include "rho.h"                                          // constant  NB_CORNERS_2D
                                                          // functions mpi_reduce_rho_2d, reset_charge_2d_accumulator, convert_charge_to_rho_2d_per_per
#include "space_filling_curves.h"                         // macro     COMPUTE_I_CELL_2D
                                                          // constant  I_CELL_2D_TYPE


typedef struct simulation_variables simulation_variables;
struct simulation_variables {
    int num_iteration;
    double delta_t;
    double q;
    double dt_over_dx;
    double dt_over_dy;
    cartesian_mesh_2d mesh;
};

typedef struct diag_energy_variables diag_energy_variables;
struct diag_energy_variables {
    unsigned char sim_distrib;
    int diag_nb_outputs;
    int diag_delta_step_output;
    damping_values* landau_values;
    double landau_mult_cstt;
    int nb_mpi_diagnostics; // Kinetic energy, momentum in each of 2 directions
    double* my_diagnostics; // Values of diagnostics local to each MPI process
    double* diagnostics;    // MPI reduction of all diagnostics
    double** local_diagnostics;
    double** sum;
    double** compensation;
    double** sum2;
    double** term_plus_compensation;
    FILE* file_diag_energy_update;
    float weight_xy;
    int nb_fourier_modes;
    couple* fourier_modes;
};

typedef struct diag_hdf5_variables diag_hdf5_variables;
struct diag_hdf5_variables {
    int hdf5_nb_outputs;
    int hdf5_delta_step_output;
    cartesian_mesh_2d mesh_xvx;
    double* send_buf_hdf5;
    double* recv_buf_hdf5;
    float weight_x_vx;
};

// #define PIC_VERT_TEST_INITIAL
// #define PIC_VERT_KINETIC_ENERGY
// #define PIC_VERT_OUTPUT_X_VX

void diagnostics(size_t i_time, int mpi_rank, int mpi_world_size,
        double** Ex, double** Ey, field_2d E_field, double** rho_2d, bag* particles,
        diag_energy_variables* energy_variables, diag_hdf5_variables* hdf5_variables, simulation_variables* sim_variables,
        double** diag_energy, int diag_energy_size) {
    size_t i, j;
    double time;
    bag* chunkbag;
    chunk* my_chunk;
    time = sim_variables->delta_t * i_time;
    double q = sim_variables->q;
    double dt_over_dx = sim_variables->dt_over_dx;
    double dt_over_dy = sim_variables->dt_over_dy;
    cartesian_mesh_2d mesh = sim_variables->mesh;
    int ncx = mesh.num_cell_x;
    int ncy = mesh.num_cell_y;
    int num_cells_2d = ncx * ncy;
    
    // Diagnostics energy
    if ((i_time % energy_variables->diag_delta_step_output == 0) || (i_time == sim_variables->num_iteration)) {
        double exval_ee, val_ee;
        int i_diag_output = (i_time == sim_variables->num_iteration)
            ? energy_variables->diag_nb_outputs - 1
            : i_time / energy_variables->diag_delta_step_output;
        
        diag_energy[i_diag_output][0] = time;                              // time
        switch(energy_variables->sim_distrib) {
            case LANDAU_1D_PROJ2D:
                exval_ee = energy_variables->landau_mult_cstt * exp(2. * energy_variables->landau_values->omega_imag * time) *
                       (0.5 + 0.5 * cos(2. * (energy_variables->landau_values->omega_real * time - energy_variables->landau_values->psi)));
                val_ee = normL2_field_2d(mesh, Ex);
                diag_energy[i_diag_output][1] = 0.5 * log(val_ee);         // Ex_field's log(L2-norm) (simulated)
                diag_energy[i_diag_output][2] = 0.5 * log(exval_ee);       // Ex_field's log(L2-norm) (expected)
                diag_energy[i_diag_output][3] = val_ee;                    // Ex_field's L2-norm (simulated)
                diag_energy[i_diag_output][4] = exval_ee;                  // Ex_field's L2-norm (expected)
                break;
            default:
                diag_energy[i_diag_output][1] = normL2_field_2d(mesh, Ex); // Ex_field's L2-norm (simulated)
                diag_energy[i_diag_output][2] = normL2_field_2d(mesh, Ey); // Ey_field's L2-norm (simulated)
#ifdef PIC_VERT_KINETIC_ENERGY
                size_t k;
                double vx_local, vy_local;
                int num_threads;
                int thread_id;
                // Computes kinetic energy. Uses Kahan Summation Formula, cf.
                // Kahan, "Further remarks on reducing truncation errors", 1965
                // https://en.wikipedia.org/wiki/Kahan_summation_algorithm
                #pragma omp parallel private(k, thread_id)
                {
                    thread_id = omp_get_thread_num();
                    num_threads = omp_get_num_threads();
                    for (k = 0; k < energy_variables->nb_mpi_diagnostics; k++) {
                        energy_variables->sum[thread_id][k]          = 0.;
                        energy_variables->compensation[thread_id][k] = 0.;
                    }
                    #pragma omp for private(i, j, chunkbag, my_chunk, vx_local, vy_local)
                    for (j = 0; j < num_cells_2d; j++) {
                        chunkbag = &(particles[j]);
                        for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                            for (i = 0; i < my_chunk->size; i++) {
                                vx_local = my_chunk->vx[i] + 0.5 * (
                                         (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_east
                                       + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_west
                                       + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_east
                                       + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_west);
                                vy_local = my_chunk->vy[i] + 0.5 * (
                                         (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_east
                                       + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_west
                                       + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_east
                                       + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_west);
                                energy_variables->term_plus_compensation[thread_id][0] = sqr(vx_local / dt_over_dx) + sqr(vy_local / dt_over_dy)
                                                          - energy_variables->compensation[thread_id][0];
                                energy_variables->term_plus_compensation[thread_id][1] = vx_local / dt_over_dx - energy_variables->compensation[thread_id][1];
                                energy_variables->term_plus_compensation[thread_id][2] = vy_local / dt_over_dy - energy_variables->compensation[thread_id][2];
                                for (k = 0; k < energy_variables->nb_mpi_diagnostics; k++) {
                                    energy_variables->sum2[thread_id][k] = energy_variables->sum[thread_id][k] + energy_variables->term_plus_compensation[thread_id][k];
                                    energy_variables->compensation[thread_id][k] = (energy_variables->sum2[thread_id][k] - energy_variables->sum[thread_id][k]) - energy_variables->term_plus_compensation[thread_id][k];
                                    energy_variables->sum[thread_id][k] = energy_variables->sum2[thread_id][k];
                                }
                            }
                        }
                    } // End parallel region.
                    energy_variables->local_diagnostics[thread_id][0] = 0.5 * energy_variables->sum[thread_id][0] * energy_variables->weight_xy;
                    for (k = 1; k < energy_variables->nb_mpi_diagnostics; k++)
                        energy_variables->local_diagnostics[thread_id][k] = energy_variables->sum[thread_id][k] * energy_variables->weight_xy;
                }
                for (k = 0; k < energy_variables->nb_mpi_diagnostics; k++) {
                    energy_variables->my_diagnostics[k] = 0.;
                    for (thread_id = 0; thread_id < num_threads; thread_id++)
                        energy_variables->my_diagnostics[k] += energy_variables->local_diagnostics[thread_id][k];
                }
                MPI_Allreduce(&(energy_variables->my_diagnostics[0]), &(energy_variables->diagnostics[0]), energy_variables->nb_mpi_diagnostics, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);
#endif
                if (energy_variables->file_diag_energy_update) {
                    fprintf(energy_variables->file_diag_energy_update, "%f", diag_energy[i_diag_output][0]);
                    for (j = 1; j < diag_energy_size; j++)
                        fprintf(energy_variables->file_diag_energy_update, " %.*g", DBL_DECIMAL_DIG, diag_energy[i_diag_output][j]);
#ifdef PIC_VERT_KINETIC_ENERGY
                    fprintf(energy_variables->file_diag_energy_update, " %.*g %.*g",
                            DBL_DECIMAL_DIG, energy_variables->diagnostics[0],
                            DBL_DECIMAL_DIG, energy_variables->diagnostics[0] + 0.5 * (diag_energy[i_diag_output][1] + diag_energy[i_diag_output][2]));
                    for (k = 1; k < energy_variables->nb_mpi_diagnostics; k++)
                        fprintf(energy_variables->file_diag_energy_update, " %.*g", DBL_DECIMAL_DIG, energy_variables->diagnostics[k]);
#endif
                    for (i = 0; i < energy_variables->nb_fourier_modes; i++)
                        fprintf(energy_variables->file_diag_energy_update, " %.*g", DBL_DECIMAL_DIG,
                                energy_fourier_mode_2d(mesh, energy_variables->fourier_modes[i].value1, energy_variables->fourier_modes[i].value2, Ex, Ey));
                    fprintf(energy_variables->file_diag_energy_update, "\n");
                }
        }
    }
    
    // Diagnostics hdf5.
    if ((hdf5_variables->hdf5_nb_outputs > 0) &&
            ((i_time % hdf5_variables->hdf5_delta_step_output == 0) || (i_time == sim_variables->num_iteration))) {
        int i_hdf5_output = (i_time == sim_variables->num_iteration)
            ? hdf5_variables->hdf5_nb_outputs + 1
            : i_time / hdf5_variables->hdf5_delta_step_output + 1;
        double f_x_y[ncx+1][ncy+1]; // Array allocated contiguously for hdf5 outputs.
        
        if (mpi_rank == 0) {
            for (i = 0; i <= ncx; i++)
                for (j = 0; j <= ncy; j++)
                    f_x_y[i][j] = q * (rho_2d[i][j] - 1.);
            plot_f_cartesian_mesh_2d(i_hdf5_output, f_x_y[0], mesh, time, "rho");
            for (i = 0; i <= ncx; i++)
                for (j = 0; j <= ncy; j++)
                    f_x_y[i][j] = Ex[i][j];
            plot_f_cartesian_mesh_2d(i_hdf5_output, f_x_y[0], mesh, time, "Ex");
            for (i = 0; i <= ncx; i++)
                for (j = 0; j <= ncy; j++)
                    f_x_y[i][j] = Ey[i][j];
            plot_f_cartesian_mesh_2d(i_hdf5_output, f_x_y[0], mesh, time, "Ey");
        }
#ifdef PIC_VERT_OUTPUT_X_VX
        int hdf5_ncx  = hdf5_variables->mesh_xvx.num_cell_x;
        int hdf5_ncvx = hdf5_variables->mesh_xvx.num_cell_y;
        double hdf5_vx;
        int hdf5_ic_x, hdf5_ic_vx;
        float hdf5_dvx;
        double hdf5_vx_min = hdf5_variables->mesh_xvx.y_min;
        double f_x_vx[hdf5_ncx+1][hdf5_ncvx+1]; // Array allocated contiguously for hdf5 outputs.
        // First, all MPI processes participate in the reduce
        for (i = 0; i <= hdf5_ncx; i++)
            for (j = 0; j <= hdf5_ncvx; j++)
                f_x_vx[i][j] = 0;
        // TODO: Parallelize with OpenMP / Vectorization this deposit (like in the "real" deposit).
        for (j = 0; j < num_cells_2d; j++) {
            chunkbag = &(particles[j]);
            for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                for (i = 0; i < my_chunk->size; i++) {
                    hdf5_vx = (my_chunk->vx[i] / dt_over_dx - hdf5_vx_min) / hdf5_variables->mesh_xvx.delta_y;
                    hdf5_ic_vx = (int)hdf5_vx;
                    hdf5_dvx = (float)(hdf5_vx - hdf5_ic_vx);
                    hdf5_ic_x = (j / ncy) + (hdf5_ncx - ncx) / 2;
                    if ((hdf5_ic_vx >= 0) && (hdf5_ic_vx < hdf5_ncvx) && (hdf5_ic_x >= 0) && (hdf5_ic_x < hdf5_ncx)) {
                        f_x_vx[hdf5_ic_x    ][hdf5_ic_vx    ] += (1. - my_chunk->dx[i]) * (1. - hdf5_dvx);
                        f_x_vx[hdf5_ic_x    ][hdf5_ic_vx + 1] += (1. - my_chunk->dx[i]) * (     hdf5_dvx);
                        f_x_vx[hdf5_ic_x + 1][hdf5_ic_vx    ] += (     my_chunk->dx[i]) * (1. - hdf5_dvx);
                        f_x_vx[hdf5_ic_x + 1][hdf5_ic_vx + 1] += (     my_chunk->dx[i]) * (     hdf5_dvx);
                    }
                }
            }
        }
        // Periodicity
        for (i = 0; i < hdf5_ncx + 1; i++) {
            f_x_vx[i][    0    ] += f_x_vx[i][hdf5_ncvx];
            f_x_vx[i][hdf5_ncvx]  = f_x_vx[i][    0    ];
        }
        for (j = 0; j < hdf5_ncvx + 1; j++) {
            f_x_vx[   0    ][j] += f_x_vx[hdf5_ncx][j];
            f_x_vx[hdf5_ncx][j]  = f_x_vx[   0    ][j];
        }
        // Reduce
        if (mpi_world_size > 1) {
            for (i = 0; i < hdf5_ncx; i++) {
                for (j = 0; j < hdf5_ncvx; j++) {
                    hdf5_variables->send_buf_hdf5[i * hdf5_ncvx + j] = f_x_vx[i][j];
                    hdf5_variables->recv_buf_hdf5[i * hdf5_ncvx + j] = 0.;
                }
            }
            MPI_Allreduce(hdf5_variables->send_buf_hdf5, hdf5_variables->recv_buf_hdf5, hdf5_ncx * hdf5_ncvx, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);
            for (i = 0; i < hdf5_ncx; i++)
                for (j = 0; j < hdf5_ncvx; j++)
                    f_x_vx[i][j] = hdf5_variables->recv_buf_hdf5[i * hdf5_ncvx + j];
            // Periodicity
            for (i = 0; i < hdf5_ncx + 1; i++)
                f_x_vx[i][hdf5_ncvx] = f_x_vx[i][0];
            for (j = 0; j < hdf5_ncvx + 1; j++)
                f_x_vx[hdf5_ncx][j] = f_x_vx[0][j];
        }
        for (i = 0; i <= hdf5_ncx; i++)
            for (j = 0; j <= hdf5_ncvx; j++)
                f_x_vx[i][j] *= hdf5_variables->weight_x_vx / (hdf5_variables->mesh_xvx.delta_x * hdf5_variables->mesh_xvx.delta_y);
        // Then only one MPI process outputs the reduce
        if (mpi_rank == 0)
            plot_f_cartesian_mesh_2d(i_hdf5_output, f_x_vx[0], hdf5_variables->mesh_xvx, time, "f_x_vx");
#endif
    }
}

/*****************************************************************************
 *                             Simulation 2d                                 *
 *****************************************************************************/

// All the simulations in this file follow the 'array of structures'
// data layout. The structure doesn't contains the weight, because it is known
// to be a constant.

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

// Initial distribution
#ifndef INITIAL_DISTRIBUTION
#    define INITIAL_DISTRIBUTION LANDAU_2D
#endif

#ifndef THERMAL_SPEED
#    define THERMAL_SPEED 1.
#endif

// Perturbation.
#ifndef ALPHA
#   define ALPHA 0.01
#endif

#define ID_PRIVATE_BAG   0
#define ID_SHARED_BAG    1
#define NB_BAGS_PER_CELL 2

// OpenMP tiling.
// Each thread has private chunkbags for the cells in the tile
// it's working on + borders = TILE_SIZE / 2.
// We still have shared chunkbags, but they should not be used
// because particles should not move further than half-tile away.
// We use a coloring scheme to avoid races.
#ifndef OMP_TILE_SIZE
#   define OMP_TILE_SIZE 4
#endif
#define OMP_TILE_BORDERS (OMP_TILE_SIZE / 2)

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5;
    double time_particle_loop, time_append, time_mpi_allreduce, time_poisson;
    
    // Temporary variables.
    double x, y;            // store the new position values
    int ic_x, ic_y, i_cell; // store the new position index values
    size_t i, j, i_time;    // loop indices
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Automatic values for the parameters.
    int nb_fourier_modes = 0;
    couple fourier_modes[PICVERT_MAX_NB_FOURIER_MODES];
    for (i = 0; i < PICVERT_MAX_NB_FOURIER_MODES; i++)
        fourier_modes[i] = (couple) {.value1 = INT_NOT_SET, .value2 = INT_NOT_SET};
    unsigned char sim_distrib = INITIAL_DISTRIBUTION; // Physical test case (LANDAU_1D_PROJ2D, TWO_BEAMS_FIJALKOW, LANDAU_2D,
                                                      // TWO_STREAM_2D, TWO_STREAM_BERNIER or TWO_STREAM_1D_PROJ2D).
    int ncx               = NCX;                      // Number of grid points, x-axis
    int ncy               = NCY;                      // Number of grid points, y-axis
    long int nb_particles = NB_PARTICLE;              // Number of particles
    int num_iteration     = NB_ITER;                  // Number of time steps
    int diag_nb_outputs   = NB_ITER;                  // Number of diagnostic energy outputs
    int hdf5_nb_outputs   = 1000;                     // Number of hdf5 outputs
    double delta_t        = DELTA_T;                  // Time step
    double thermal_speed  = THERMAL_SPEED;            // Thermal speed
    double alpha   = ALPHA; // Landau perturbation amplitude
    double kmode_x = 0.5;   // Landau perturbation mode, x-axis
    double kmode_y = 0.5;   // Landau perturbation mode, y-axis
    
    // Read parameters from file.
    if (argc >= 2) {
        simulation_parameters parameters = read_parameters_from_file(argv[1], "2D");
        if (parameters.nb_fourier_modes != INT_NOT_SET) {
            nb_fourier_modes = parameters.nb_fourier_modes;
            for (i = 0; i < nb_fourier_modes; i++)
                fourier_modes[i] = parameters.fourier_modes[i];
        }
        if (strcmp(parameters.sim_distrib_name, STRING_NOT_SET) != 0)
            sim_distrib = parameters.sim_distrib;
        if (parameters.ncx != INT_NOT_SET)
            ncx             = parameters.ncx;
        if (parameters.ncy != INT_NOT_SET)
            ncy             = parameters.ncy;
        if (parameters.nb_particles != INT_NOT_SET)
            nb_particles    = parameters.nb_particles;
        if (parameters.num_iteration != INT_NOT_SET)
            num_iteration   = parameters.num_iteration;
        if (parameters.diag_nb_outputs != INT_NOT_SET)
            diag_nb_outputs = parameters.diag_nb_outputs;
        if (parameters.hdf5_nb_outputs != INT_NOT_SET)
            hdf5_nb_outputs = parameters.hdf5_nb_outputs;
        if (parameters.delta_t != DOUBLE_NOT_SET)
            delta_t       = parameters.delta_t;
        if (parameters.thermal_speed != DOUBLE_NOT_SET)
            thermal_speed = parameters.thermal_speed;
        if (parameters.alpha != DOUBLE_NOT_SET)
            alpha   = parameters.alpha;
        if (parameters.kmode_x != DOUBLE_NOT_SET)
            kmode_x = parameters.kmode_x;
        if (parameters.kmode_y != DOUBLE_NOT_SET)
            kmode_y = parameters.kmode_y;
    } else
        printf("No parameter file was passed through the command line. I will use the default parameters.\n");
    
    // Random initialization or read from file.
    const char sim_initial = INIT_NOFILE;
    
    // Spatial parameters for initial density function.
    double *params;
    if (sim_distrib == LANDAU_1D_PROJ2D || sim_distrib == TWO_BEAMS_FIJALKOW) {
        params = malloc(2 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
    } else if (sim_distrib == LANDAU_2D || sim_distrib == TWO_STREAM_2D) {
        params = malloc(3 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
    } else if (sim_distrib == TWO_STREAM_BERNIER) {
        params = malloc(7 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 0.;
        params[4] = 1.;
        params[5] = 1.;
        params[6] = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        params = malloc(5 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 3.5; // i_modes_x = 1, 2, 3
        params[4] = 3.5; // i_modes_y = 1, 2, 3
    } else
        params = malloc(0 * sizeof(double));
    
    // Velocity parameters for initial density function.
    double *speed_params;
    if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        speed_params = malloc(2 * sizeof(double));
        speed_params[0] = thermal_speed;
        speed_params[1] = 2. * sqrt(2.) * thermal_speed;
    } else {
        speed_params = malloc(1 * sizeof(double));
        speed_params[0] = thermal_speed;
    }
    
    // Mesh
    ncy = (sim_distrib == LANDAU_1D_PROJ2D || sim_distrib == TWO_STREAM_1D_PROJ2D) ? 1 : ncy; // For 1D tests
    double x_min, y_min, x_max, y_max;
    const int num_cells_2d = ncx * ncy;
    if (sim_distrib == LANDAU_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 26. * PI;
        y_max = 1.;
    } else {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 2 * PI / kmode_y;
    }
    cartesian_mesh_1d mesh_1d = create_mesh_1d(ncx, x_min, x_max); // For 1D tests
    cartesian_mesh_2d mesh    = create_mesh_2d(ncx, ncy, x_min, x_max, y_min, y_max);
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    const int ncxminusone = ncx - 1;
    const int ncyminusone = ncy - 1;

    // Vectorization of the deposit
    int corner;
/*
 *    dx
 *   <———>
 *   +——————————*
 *   |xxx|======|
 *   |———O——————| ^
 *   |***|++++++| |
 *   |***|++++++| |dy
 *   =——————————x v
 *
 * The "=" corner is the south west corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "=" area. This fraction is equal to
 * (1. - dx) * (1. - dy), hence the SW coefficients.
 * The "+" corner is the north west corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "+" area. This fraction is equal to
 * (1. - dx) * (     dy), hence the NW coefficients.
 * The "x" corner is the south east corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "x" area. This fraction is equal to
 * (     dx) * (1. - dy), hence the SE coefficients.
 * The "*" corner is the north east corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "*" area. This fraction is equal to
 * (     dx) * (     dy), hence the NE coefficients.
 */
    // Space coeffs                                                             SW   NW   SE   NE
    const float coeffs_x[NB_CORNERS_2D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  1.,  0.,  0.};
    const float  signs_x[NB_CORNERS_2D] __attribute__((aligned(VEC_ALIGN))) = { -1., -1.,  1.,  1.};
    const float coeffs_y[NB_CORNERS_2D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  0.,  1.,  0.};
    const float  signs_y[NB_CORNERS_2D] __attribute__((aligned(VEC_ALIGN))) = { -1.,  1., -1.,  1.};
    
    // Simulation parameters
    const double q           = -1.; // particle charge
    const double m           =  1.; // particle mass
    const double dt_q_over_m = delta_t * q / m;
    const double dt_over_dx  = delta_t / mesh.delta_x;
    const double dt_over_dy  = delta_t / mesh.delta_y;
    char simulation_name[42]     = "Vlasov-Poisson 2d";
    char data_structure_name[99] = "Array of Concurrent Chunkbags of SoA (1 private + 1 shared / cell)";
    char sort_name[42]           = "always sort";
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = allocateMatrix(ncx, ncy);
    double* recv_buf = allocateMatrix(ncx, ncy);
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    int thread_id;
    int offset;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    aligned_int_array* i_cells = allocate_aligned_int_array_array(num_threads); // to vectorize the computation of the i_cells
    
    // Hdf5 outputs.
    H5open(); // Initializes the HDF5 library.
    hdf5_nb_outputs = min(1000, hdf5_nb_outputs);                       // With more than 1,000 outputs, VisIt crashes.
    hdf5_nb_outputs = min(num_iteration, hdf5_nb_outputs);              // In case we put more outputs than iterations...
    const int hdf5_delta_step_output = (hdf5_nb_outputs == 0) ? 0 : num_iteration / hdf5_nb_outputs;
    hdf5_nb_outputs = (hdf5_nb_outputs == 0) ? 0 : hdf5_nb_outputs + 1; // For the last diagnostic
    // Hdf5 outputs for x-vx cuts.
    const double hdf5_x_min  = x_min;
    const double hdf5_x_max  = x_max;
    const double hdf5_vx_min = -6.;
    const double hdf5_vx_max = 6.;
    const int hdf5_ncx = min(256, lowest_even_number_greater_or_equal_than((int)((hdf5_x_max - hdf5_x_min) / (x_max - x_min) * ncx)));
    const int hdf5_ncvx = 128;
    diag_hdf5_variables hdf5_variables;
    hdf5_variables.hdf5_nb_outputs = hdf5_nb_outputs;
    hdf5_variables.hdf5_delta_step_output = hdf5_delta_step_output;
    hdf5_variables.mesh_xvx = create_mesh_2d(hdf5_ncx, hdf5_ncvx, hdf5_x_min, hdf5_x_max, hdf5_vx_min, hdf5_vx_max);
    hdf5_variables.send_buf_hdf5 = allocateMatrix(hdf5_ncx, hdf5_ncvx);
    hdf5_variables.recv_buf_hdf5 = allocateMatrix(hdf5_ncx, hdf5_ncvx);
    hdf5_variables.weight_x_vx = (float)(x_max - x_min) / ((float)mpi_world_size * (float)nb_particles); // Weight for deposit on the x-vx plane.
    
    // The following matrices are (ncx+1) * (ncy+1) arrays, with the periodicity :
    //     M[ncx][ . ] = M[0][.]
    //     M[ . ][ncy] = M[.][0]
    // rho the charge density
    // Ex the electric field on the x-axis
    // Ey the electric field on the y-axis
    double** rho_2d = allocate_matrix(ncx+1, ncy+1);
    double** Ex = allocate_matrix(ncx+1, ncy+1);
    double** Ey = allocate_matrix(ncx+1, ncy+1);
    double** q_times_rho = allocate_matrix(ncx+1, ncy+1); // to store q * rho = -rho
    double* Ex_1d          = malloc((ncx + 1) * sizeof(double)); // For 1D tests
    double* q_times_rho_1d = malloc((ncx + 1) * sizeof(double)); // For 1D tests
    
    // accumulators are num_cells_2d arrays : for each cell, the 4 corners values
    field_2d E_field = create_field_2d(ncx, ncy);
    /* For each cell, the 4 corners values ; for vectorization, each thread has its own copy */
    double* charge_accu;
    if (posix_memalign((void**)&charge_accu, VEC_ALIGN, num_cells_2d * NB_CORNERS_2D * num_threads * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize charge_accu.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(charge_accu, VEC_ALIGN);
#else
    charge_accu = __builtin_assume_aligned(charge_accu, VEC_ALIGN);
#endif
    
    // Diagnostic energy.
    double kmode = 0.;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
            kmode = kmode_x;
            break;
        case LANDAU_2D:
            kmode = sqrt(sqr(kmode_x) + sqr(kmode_y));
            break;
    }
    diag_nb_outputs = min(num_iteration, diag_nb_outputs);    // In case we put more outputs than iterations...
    const int diag_delta_step_output = num_iteration / diag_nb_outputs;
    diag_nb_outputs = num_iteration / diag_delta_step_output; // In case the above division does not give integer value
    diag_nb_outputs = diag_nb_outputs + 1;                    // For the last diagnostic
    const int diag_energy_size = (sim_distrib == LANDAU_1D_PROJ2D) ? 5 : 3;
    const int diag_speed_size  = 5;
    double** diag_energy = allocate_matrix(diag_nb_outputs, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration,   diag_speed_size);
    FILE* file_diag_energy_update = (void*)0; // Updated all simulation long, contrary to the previous ones which are only created at the end
    if (ncy == 1) { // For 1D tests
        bool modes_were_removed = false;
        for (i = 0; i < nb_fourier_modes; i++) {
            if (fourier_modes[i].value2 != 0) {
                for (j = i; j < nb_fourier_modes - 1; j++) {
                    fourier_modes[j].value1 = fourier_modes[j+1].value1;
                    fourier_modes[j].value2 = fourier_modes[j+1].value2;
                }
                nb_fourier_modes--;
                i--;
                modes_were_removed = true;
            }
        }
        if (modes_were_removed && mpi_rank == 0)
            printf("Running on 1d in space. Fourier modes that had non-zero values on the y-axis were removed.");
    }
    if (mpi_rank == 0) {
        file_diag_energy_update = fopen("diag_energy.txt", "w");
        if (sim_distrib == LANDAU_1D_PROJ2D)
            fprintf(file_diag_energy_update, "Time | log(Int(E^2))-simu | log(Int(E^2))-theo | Int(E^2)-simu | Int(E^2)-theo");
        else
            fprintf(file_diag_energy_update, "Time | Int(Ex^2) | Int(Ey^2)");
#ifdef PIC_VERT_KINETIC_ENERGY
        fprintf(file_diag_energy_update, " | Kinetic Energy | Total Energy (= Kinetic Energy + 0.5 * (Int(Ex^2) + Int(Ey^2)))");
        fprintf(file_diag_energy_update, " | Momentum (x) | Momentum (y)");
#endif
        if (ncy == 1) // For 1D tests
            for (i = 0; i < nb_fourier_modes; i++)
                fprintf(file_diag_energy_update, " | Fourier mode(%d)", fourier_modes[i].value1);
        else
            for (i = 0; i < nb_fourier_modes; i++)
                fprintf(file_diag_energy_update, " | Energy Fourier mode(%d, %d)", fourier_modes[i].value1, fourier_modes[i].value2);
        fprintf(file_diag_energy_update, "\n");
    }
    
  if (mpi_rank == 0) {
    printf("#CHUNK_SIZE = %d\n", CHUNK_SIZE);
    printf("#VEC_ALIGN = %d\n", VEC_ALIGN);
    printf("#OMP_TILE_SIZE = %d\n", OMP_TILE_SIZE);
    printf("#OMP_TILE_BORDERS = %d\n", OMP_TILE_BORDERS);
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#alpha = %.*g\n", DBL_DECIMAL_DIG, alpha);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#y_min = %.*g\n", DBL_DECIMAL_DIG, y_min);
    printf("#y_max = %.*g\n", DBL_DECIMAL_DIG, y_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_2d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
//    printf("#y_field_factor = %.*g\n", DBL_DECIMAL_DIG, y_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#ncy = %d\n", ncy);
    printf("#nb_particles = %ld\n", nb_particles);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    // Poisson solver.
    poisson_1d_solver solver_1d = new_poisson_1d_fft_solver(mesh_1d); // For 1D tests
    poisson_2d_solver solver    = new_poisson_2d_fft_solver(mesh);
    
    // Coloring
    int i_color;
    int nb_color_2d = 4;
    
    // Particle data structure.
    bag* chunkbag;
    chunk* next_chunk;
    chunk* my_chunk;
    int ix_min, ix_max, iy_min, iy_max, ix, iy;
    bag* particles = malloc(num_cells_2d * sizeof(bag));
    bag** particlesNext = malloc(NB_BAGS_PER_CELL * sizeof(bag*));
    for (j = 0; j < NB_BAGS_PER_CELL; j++)
        particlesNext[j] = malloc(num_cells_2d * sizeof(bag));
    init_all_chunks(NB_BAGS_PER_CELL, nb_particles, mesh, OMP_TILE_SIZE, OMP_TILE_BORDERS, &particlesNext);
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    if (sim_initial == INIT_READ) {
        time_start = omp_get_wtime();
        read_particle_array_2d(mpi_rank, mpi_world_size, nb_particles, mesh, &weight, &particles);
        if (mpi_rank == 0)
            printf("Read time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
        create_particle_array_2d(mpi_world_size, nb_particles, mesh, sim_distrib,
            params, speed_params, &weight, &particles);
        if (mpi_rank == 0)
            printf("Creation time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%ldkk.dat", nb_particles / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d %d\n", ncx, ncy);
            fprintf(file_write_particles, "%ld\n", nb_particles);
            for (j = 0; j < num_cells_2d; j++) {
                chunkbag = &(particles[j]);
                for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                    for (i = 0; i < my_chunk->size; i++) {
                        fprintf(file_write_particles, "%ld %.*g %.*g %.*g %.*g\n", j,
                          FLT_DECIMAL_DIG, my_chunk->dx[i], FLT_DECIMAL_DIG, my_chunk->dy[i],
                          DBL_DECIMAL_DIG, my_chunk->vx[i], DBL_DECIMAL_DIG, my_chunk->vy[i]);
                    }
                }
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x * mesh.delta_y);
    // We just use the electric fields to update the speed, with always the same multiply.
    const double x_field_factor = dt_q_over_m * dt_over_dx;
    const double y_field_factor = dt_q_over_m * dt_over_dy;
    
    simulation_variables sim_variables;
    sim_variables.num_iteration = num_iteration;
    sim_variables.delta_t = delta_t;
    sim_variables.q = q;
    sim_variables.dt_over_dx = dt_over_dx;
    sim_variables.dt_over_dy = dt_over_dy;
    sim_variables.mesh = mesh;
    
    diag_energy_variables energy_variables;
    energy_variables.sim_distrib = sim_distrib;
    energy_variables.diag_nb_outputs = diag_nb_outputs;
    energy_variables.diag_delta_step_output = diag_delta_step_output;
    energy_variables.landau_values = get_damping_values(kmode);
    energy_variables.landau_mult_cstt = sqr(4. * alpha * energy_variables.landau_values->er) * PI / kmode; // Landau
    energy_variables.nb_mpi_diagnostics = 3; // Kinetic energy, momentum in each of 2 directions
    energy_variables.my_diagnostics = malloc(energy_variables.nb_mpi_diagnostics * sizeof(double)); // Values of diagnostics local to each MPI process
    energy_variables.diagnostics    = malloc(energy_variables.nb_mpi_diagnostics * sizeof(double)); // MPI reduction of all diagnostics
    energy_variables.local_diagnostics      = allocate_aligned_double_matrix(num_threads, energy_variables.nb_mpi_diagnostics);
    energy_variables.sum                    = allocate_aligned_double_matrix(num_threads, energy_variables.nb_mpi_diagnostics);
    energy_variables.compensation           = allocate_aligned_double_matrix(num_threads, energy_variables.nb_mpi_diagnostics);
    energy_variables.sum2                   = allocate_aligned_double_matrix(num_threads, energy_variables.nb_mpi_diagnostics);
    energy_variables.term_plus_compensation = allocate_aligned_double_matrix(num_threads, energy_variables.nb_mpi_diagnostics);
    energy_variables.file_diag_energy_update = file_diag_energy_update;
    energy_variables.weight_xy = weight;
    energy_variables.nb_fourier_modes = nb_fourier_modes;
    energy_variables.fourier_modes = malloc(nb_fourier_modes * sizeof(couple));
    for (i = 0; i < nb_fourier_modes; i++) {
        energy_variables.fourier_modes[i].value1 = fourier_modes[i].value1;
        energy_variables.fourier_modes[i].value2 = fourier_modes[i].value2;
    }
    
#ifdef PIC_VERT_TEST_INITIAL
    diagnostics(0, mpi_rank, mpi_world_size, Ex, Ey, E_field, rho_2d, particles,
        &energy_variables, &hdf5_variables, &sim_variables, diag_energy, diag_energy_size);
    MPI_Finalize();
    return 0;
#endif
    
    reset_charge_2d_accumulator(ncx, ncy, num_threads, charge_accu);
    // Computes rho at initial time.
    #pragma omp parallel private(thread_id, offset)
    {
        thread_id = omp_get_thread_num();
        offset = thread_id * NB_CORNERS_2D * num_cells_2d;
        #pragma omp for private(i, j, chunkbag, my_chunk, corner)
        for (j = 0; j < num_cells_2d; j++) {
            chunkbag = &(particles[j]);
            for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
                for (i = 0; i < my_chunk->size; i++) {
#ifdef PIC_VERT_OPENMP_4_0
                    #pragma omp simd aligned(coeffs_x, coeffs_y, signs_x, signs_y:VEC_ALIGN)
#endif
                    for (corner = 0; corner < NB_CORNERS_2D; corner++) {
                        charge_accu[offset + NB_CORNERS_2D * j + corner] +=
                            (coeffs_x[corner] + signs_x[corner] * my_chunk->dx[i]) *
                            (coeffs_y[corner] + signs_y[corner] * my_chunk->dy[i]);
                    }
                }
            }
        }
    } // End parallel region
    convert_charge_to_rho_2d_per_per(charge_accu, num_threads, ncx, ncy, charge_factor, rho_2d);
    mpi_reduce_rho_2d(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
    
    // Computes E at initial time.
    if (ncy == 1) { // For 1D tests
        for (i = 0; i < ncx + 1; i++)
            q_times_rho_1d[i] = q * rho_2d[i][0];
        compute_E_from_rho_1d_fft(solver_1d, q_times_rho_1d, Ex_1d);
        for (i = 0; i < ncx + 1; i++) {
            Ex[i][0] = Ex_1d[i];
            Ex[i][1] = Ex_1d[i];
            Ey[i][0] = 0.;
            Ey[i][1] = 0.;
        }
    } else {
        for (i = 0; i < ncx + 1; i++)
            for (j = 0; j < ncy + 1; j++)
                q_times_rho[i][j] = q * rho_2d[i][j];
        compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
    }
    accumulate_field_2d(Ex, Ey, ncx, ncy, x_field_factor, y_field_factor, E_field);
    
    // Computes speeds half time-step backward (leap-frog method).
    // WARNING : starting from here, v doesn't represent the speed, but speed * dt / dx.
    #pragma omp parallel for private(i, j, chunkbag, my_chunk)
    for (j = 0; j < num_cells_2d; j++) {
        chunkbag = &(particles[j]);
        for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp simd
#endif
            for (i = 0; i < my_chunk->size; i++) {
                my_chunk->vx[i] = my_chunk->vx[i] * dt_over_dx - 0.5 * (
                                      (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_east
                                    + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_west
                                    + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_east
                                    + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_west);
                my_chunk->vy[i] = my_chunk->vy[i] * dt_over_dy - 0.5 * (
                                      (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_east
                                    + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_west
                                    + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_east
                                    + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_west);
            }
        }
    }
    
    /********************************************************************************************
     *                               Beginning of main time loop                                *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics energy and hdf5
        diagnostics(i_time, mpi_rank, mpi_world_size, Ex, Ey, E_field, rho_2d, particles,
            &energy_variables, &hdf5_variables, &sim_variables, diag_energy, diag_energy_size);
        
        time_mark1 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif

        reset_charge_2d_accumulator(ncx, ncy, num_threads, charge_accu);
        #pragma omp parallel private(thread_id, offset, my_chunk, chunkbag, next_chunk, i_color) firstprivate(nb_color_2d)
        {
            thread_id = omp_get_thread_num();
            offset = thread_id * NB_CORNERS_2D * num_cells_2d;
            // Loop on the 4 colors (in 2d), with synchronisation each time.
            for (i_color = 0; i_color < nb_color_2d; i_color++) {
                // Loop on the tiles of the grid, for the chosen color.
                #pragma omp for private(ix_min, ix_max, iy_min, iy_max, ix, iy, i, j, corner, x, y, ic_x, ic_y, i_cell) firstprivate(ncxminusone, ncyminusone, icell_param) collapse(2)
                for (ix_min = (i_color & 1)     * OMP_TILE_SIZE; ix_min <= ncxminusone; ix_min += 2 * OMP_TILE_SIZE) {
                for (iy_min = (i_color & 2) / 2 * OMP_TILE_SIZE; iy_min <= ncyminusone; iy_min += 2 * OMP_TILE_SIZE) {
                    ix_max = min(ix_min + OMP_TILE_SIZE - 1, ncxminusone);
                    iy_max = min(iy_min + OMP_TILE_SIZE - 1, ncyminusone);
                    // Nested loops on the cells of the tile.
                    for (ix = ix_min; ix <= ix_max; ix++) {
                    for (iy = iy_min; iy <= iy_max; iy++) {
                        j = COMPUTE_I_CELL_2D(icell_param, ix, iy);
                        chunkbag = &(particles[j]);
                        // Loop on the chunks of the cell, nested with loops on the particles in those chunks.
                        for (my_chunk = chunkbag->front; my_chunk; ) {
#ifdef PIC_VERT_OPENMP_4_0
                            #pragma omp simd
#endif
                            for (i = 0; i < my_chunk->size; i++) {
                                my_chunk->vx[i] +=
                                         (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_east
                                       + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_x.north_west
                                       + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_east
                                       + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_x.south_west;
                                my_chunk->vy[i] +=
                                         (     my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_east
                                       + (1. - my_chunk->dx[i]) * (     my_chunk->dy[i]) * E_field[j].field_y.north_west
                                       + (     my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_east
                                       + (1. - my_chunk->dx[i]) * (1. - my_chunk->dy[i]) * E_field[j].field_y.south_west;
                            }
#    ifdef PIC_VERT_OPENMP_4_0
                            #pragma omp simd
#    endif
                            for (i = 0; i < my_chunk->size; i++) {
                                x = (j / ncy        ) + my_chunk->dx[i] + my_chunk->vx[i];
                                y = (j & ncyminusone) + my_chunk->dy[i] + my_chunk->vy[i];
                                ic_x = (int)x - (x < 0.);
                                ic_y = (int)y - (y < 0.);
                                i_cells[thread_id].array[i] = COMPUTE_I_CELL_2D(icell_param, ic_x & ncxminusone, ic_y & ncyminusone);
                                my_chunk->dx[i] = (float)(x - ic_x);
                                my_chunk->dy[i] = (float)(y - ic_y);
                            }
                            for (i = 0; i < my_chunk->size; i++) {
                                i_cell = i_cells[thread_id].array[i];
                                ic_x = i_cell / ncy        ;
                                ic_y = i_cell & ncyminusone;
                                if (((ic_x >= ix_min - OMP_TILE_BORDERS && ic_x <= ix_max + OMP_TILE_BORDERS) || (ix_min == 0 && ic_x >= ncx - OMP_TILE_BORDERS) || (ix_max == ncxminusone && ic_x <= OMP_TILE_BORDERS - 1)) && ((ic_y >= iy_min - OMP_TILE_BORDERS && ic_y <= iy_max + OMP_TILE_BORDERS) || (iy_min == 0 && ic_y >= ncy - OMP_TILE_BORDERS) || (iy_max == ncyminusone && ic_y <= OMP_TILE_BORDERS - 1)))
                                    bag_push_serial(&(particlesNext[ID_PRIVATE_BAG][i_cell]), my_chunk->dx[i], my_chunk->dy[i], my_chunk->vx[i], my_chunk->vy[i], thread_id);
                                else
                                    bag_push_concurrent(&(particlesNext[ID_SHARED_BAG][i_cell]), my_chunk->dx[i], my_chunk->dy[i], my_chunk->vx[i], my_chunk->vy[i], thread_id);
#ifdef PIC_VERT_OPENMP_4_0
                                #pragma omp simd aligned(coeffs_x, coeffs_y, signs_x, signs_y:VEC_ALIGN)
#endif
                                for (corner = 0; corner < NB_CORNERS_2D; corner++) {
                                    charge_accu[offset + NB_CORNERS_2D * i_cell + corner] +=
                                        (coeffs_x[corner] + signs_x[corner] * my_chunk->dx[i]) *
                                        (coeffs_y[corner] + signs_y[corner] * my_chunk->dy[i]);
                                }
                            }
                            next_chunk = my_chunk->next;
                            chunk_free(my_chunk, thread_id);
                            my_chunk = next_chunk;
                        }
                    }}
                }}
            }
            #pragma omp single
            {
                time_mark2 = omp_get_wtime();
                compute_cumulative_free_list_sizes();
            }
            #pragma omp for private(j) schedule(static)
            for (j = 0; j < num_cells_2d; j++) {
                particles[j] = particlesNext[ID_SHARED_BAG][j];
                bag_init(&(particlesNext[ID_SHARED_BAG][j]), ID_SHARED_BAG, j, thread_id);
                bag_append(&(particles[j]), &(particlesNext[ID_PRIVATE_BAG][j]), ID_PRIVATE_BAG, j, thread_id);
            }
        } // End parallel region
        update_free_list_sizes();
        time_mark3 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif
        
        // Converts accumulator to rho
        convert_charge_to_rho_2d_per_per(charge_accu, num_threads, ncx, ncy, charge_factor, rho_2d);
        mpi_reduce_rho_2d(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
        time_mark4 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        if (ncy == 1) { // For 1D tests
            for (i = 0; i < ncx + 1; i++)
                q_times_rho_1d[i] = q * rho_2d[i][0];
            compute_E_from_rho_1d_fft(solver_1d, q_times_rho_1d, Ex_1d);
            for (i = 0; i < ncx + 1; i++) {
                Ex[i][0] = Ex_1d[i];
                Ex[i][1] = Ex_1d[i];
                Ey[i][0] = 0.;
                Ey[i][1] = 0.;
            }
        } else {
            for (i = 0; i < ncx + 1; i++)
                for (j = 0; j < ncy + 1; j++)
                    q_times_rho[i][j] = q * rho_2d[i][j];
            compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
        }
        accumulate_field_2d(Ex, Ey, ncx, ncy, x_field_factor, y_field_factor, E_field);
        time_mark5 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after update v / x / deposit
        diag_speed[i_time][2] = time_mark3; // after append
        diag_speed[i_time][3] = time_mark4; // after all_reduce
        diag_speed[i_time][4] = time_mark5; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_particle_loop = 0.;
    time_append        = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_particle_loop += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_append        += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_mpi_allreduce += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_poisson       += diag_speed[i_time][4] - diag_speed[i_time][3];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    // Last diagnostic.
    diagnostics(num_iteration, mpi_rank, mpi_world_size, Ex, Ey, E_field, rho_2d, particles,
        &energy_variables, &hdf5_variables, &sim_variables, diag_energy, diag_energy_size);
    if (file_diag_energy_update)
        fclose(file_diag_energy_update);
    diag_energy_and_speed_chunkbags(mpi_rank,
        "diag_lee_4corners.txt",   diag_nb_outputs, diag_energy_size, diag_energy,
        "diag_speed_4corners.txt", num_iteration,   diag_speed_size,  diag_speed);
    print_time_chunkbags(mpi_rank, mpi_world_size, nb_particles, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_particle_loop, time_append, time_mpi_allreduce, time_poisson);
    
    free(params);
    free(speed_params);
    deallocate_matrix(q_times_rho, ncx+1, ncy+1);
    deallocate_matrix(rho_2d, ncx+1, ncy+1);
    deallocate_matrix(Ex, ncx+1, ncy+1);
    deallocate_matrix(Ey, ncx+1, ncy+1);
    deallocate_matrix(diag_energy, diag_nb_outputs, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration,   diag_speed_size);
    free(charge_accu);
    deallocate_aligned_int_array_array(i_cells, num_threads);
    
    free(send_buf);
    free(recv_buf);
    free(hdf5_variables.send_buf_hdf5);
    free(hdf5_variables.recv_buf_hdf5);
    free_poisson_2d(&solver);
    free_field_2d(E_field);
    
    // For 1D tests
    free(Ex_1d);
    free(q_times_rho_1d);
    free_poisson_1d(&solver_1d);
    pic_vert_free_RNG();
    MPI_Finalize();
    
    return 0;
}

