/**
 * PIC simulations in 2d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                  // functions cos, log
#include <mpi.h>                   // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                   // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                   // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                 // functions printf, fprintf (output strings on a stream), sprintf
                                   // constant  stderr (standard error output stream)
#include <stdlib.h>                // functions malloc, free ((de)allocate memory)
                                   //           exit (error handling)
                                   // constant  EXIT_FAILURE (error handling)
                                   // type      size_t
#include <string.h>                // function  strcmp
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>              // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                   // functions PAPI_read_counters
                                   // type      long_long
#    include "papi_handlers.h"     // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"         // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"           // function  normL2_field_2d, get_damping_values
#include "initial_distributions.h" // constants LANDAU_1D_PROJ2D, LANDAU_2D, KELVIN_HELMHOLTZ, TWO_STREAM_BERNIER,
                                   //           TWO_BEAMS_FIJALKOW, TWO_STREAM_1D_PROJ2D, TWO_STREAM_2D
#include "math_functions.h"        // functions sqr, max
#include "matrix_functions.h"      // functions allocate_matrix, deallocate_matrix, allocateMatrix, deallocateMatrix
#include "meshes.h"                // type      cartesian_mesh_2d
                                   // function  create_mesh_2d
#include "output.h"                // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"            // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG, NB_PARTICLE
#include "parameter_reader.h"      // type      simulation_parameters
                                   // constants STRING_NOT_SET, INT_NOT_SET, DOUBLE_NOT_SET
                                   // function  read_parameters_from_file
#include "particle_type_aos_2d.h"  // types     particle_2d, particle_sorter_oop_2d
                                   // functions create_particle_arrays_2d, new_particle_sorter_oop_2d, sort_particles_oop_2d
#include "poisson_solvers.h"       // type      poisson_2d_solver
                                   // function  new_poisson_2d_fft_solver, compute_E_from_rho_2d_fft, free_poisson_2d
#include "random.h"                // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG
#include "space_filling_curves.h"  // macros    COMPUTE_I_CELL_2D, ICX_FROM_I_CELL_2D, ICY_FROM_I_CELL_2D
                                   // constant  I_CELL_2D_TYPE, space_filling_curves_names

/*
 * Conversion from charge accumulator to rho, with periodicity.
 * @param[in]     ncx mesh size on the x-axis.
 * @param[in]     ncy mesh size on the y-axis.
 * @param[in]     factor is a number by which the whole array is multiplicated
 *                (done in this function to gain performance).
 * @param[in,out] rho[ncx+1][ncy+1], the charge density.
 */
void convert_charge_to_rho_2d_contiguous(int ncx, int ncy, double factor, double rho[ncx+1][ncy+1]) {
    size_t i, j;
    
    // Periodicity
    for (i = 0; i < ncx + 1; i++) {
        rho[i][ 0 ] += rho[i][ncy];
        rho[i][ncy]  = rho[i][ 0 ];
    }
    for (j = 0; j < ncy + 1; j++) {
        rho[ 0 ][j] += rho[ncx][j];
        rho[ncx][j]  = rho[ 0 ][j];
    }
    
    // Charge is the charge MASS and not the charge DENSITY, this explains the
    // factor = 1. / (mesh.delta_x * mesh.delta_y);
    for (i = 0; i < ncx + 1; i++)
        for (j = 0; j < ncy + 1; j++)
            rho[i][j] *= factor;
}

/*
 * Conversion from local rho to global rho, with periodicity.
 *
 * @param[in]      mpi_world_size the number of MPI processors.
 * @param[in, out] send_buf[ncx*ncy], recv_buf[ncx*ncy] the MPI buffers.
 * @param[in]      ncx mesh size on the x-axis.
 * @param[in]      ncy mesh size on the y-axis.
 * @param[in, out] rho[ncx+1][ncy+1], the particle density
 *                    in  : as computed locally
 *                    out : reduced from all the local ones
 */
void mpi_reduce_rho_2d_contiguous(int mpi_world_size, double* send_buf, double* recv_buf,
        int ncx, int ncy, double rho[ncx+1][ncy+1]) {
    if (mpi_world_size > 1) {
        size_t i, j;
        for (i = 0; i < ncx; i++)
            for (j = 0; j < ncy; j++)
                send_buf[i * ncy + j] = rho[i][j];
        MPI_Allreduce(send_buf, recv_buf, ncx * ncy, MPI_DOUBLE_PRECISION, MPI_SUM, MPI_COMM_WORLD);
        for (i = 0; i < ncx; i++)
            for (j = 0; j < ncy; j++)
                rho[i][j] = recv_buf[i * ncy + j];
        // Periodicity
        for (i = 0; i < ncx + 1; i++)
            rho[i][ncy] = rho[i][0];
        for (j = 0; j < ncy + 1; j++)
            rho[ncx][j] = rho[0][j];
    }
}

/*****************************************************************************
 *                             Simulation 2d                                 *
 *****************************************************************************/

#define SORT_IN_PLACE_COUNTING_SORT     0 // Time = O(n), Memory = nb_particles * sizeof(particle)
#define SORT_OUT_OF_PLACE_COUNTING_SORT 1 // Time = O(n), Memory = 2 * nb_particles * sizeof(particle)
#define SORT_QUICK_SORT                 2 // Time = O(n*log(n)), Memory = nb_particles * sizeof(particle)
                                          // Not implemented (multiple arrays to sort, cannot use qsort from stdlib).
#define SORT_NO_SORT                    3

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

// Number of iterations between two consecutive sorts.
#ifndef NB_ITER_BETWEEN_SORT
#   define NB_ITER_BETWEEN_SORT 20
#endif

// Initial distribution
#ifndef INITIAL_DISTRIBUTION
#    define INITIAL_DISTRIBUTION LANDAU_2D
#endif

#ifndef THERMAL_SPEED
#    define THERMAL_SPEED 1.
#endif

// Perturbation.
#ifndef ALPHA
#   define ALPHA 0.01
#endif

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5;
    double time_sort, time_particle_loop, time_mpi_allreduce, time_poisson;
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Automatic values for the parameters.
    unsigned char sim_distrib = INITIAL_DISTRIBUTION; // Physical test case (LANDAU_1D_PROJ2D, TWO_BEAMS_FIJALKOW, LANDAU_2D,
                                                      // TWO_STREAM_2D, TWO_STREAM_BERNIER or TWO_STREAM_1D_PROJ2D).
    int ncx               = NCX;                      // Number of grid points, x-axis
    int ncy               = NCY;                      // Number of grid points, y-axis
    long int nb_particles = NB_PARTICLE;              // Number of particles
    int num_iteration     = NB_ITER;                  // Number of time steps
    double delta_t        = DELTA_T;                  // Time step
    double thermal_speed  = THERMAL_SPEED;            // Thermal speed
    double alpha   = ALPHA; // Landau perturbation amplitude
    double kmode_x = 0.5;   // Landau perturbation mode, x-axis
    double kmode_y = 0.5;   // Landau perturbation mode, y-axis
    
    // Read parameters from file.
    if (argc >= 2) {
        simulation_parameters parameters = read_parameters_from_file(argv[1], "2D");
        if (strcmp(parameters.sim_distrib_name, STRING_NOT_SET) != 0)
            sim_distrib = parameters.sim_distrib;
        if (parameters.ncx != INT_NOT_SET)
            ncx             = parameters.ncx;
        if (parameters.ncy != INT_NOT_SET)
            ncy             = parameters.ncy;
        if (parameters.nb_particles != INT_NOT_SET)
            nb_particles    = parameters.nb_particles;
        if (parameters.num_iteration != INT_NOT_SET)
            num_iteration   = parameters.num_iteration;
        if (parameters.delta_t != DOUBLE_NOT_SET)
            delta_t       = parameters.delta_t;
        if (parameters.thermal_speed != DOUBLE_NOT_SET)
            thermal_speed = parameters.thermal_speed;
        if (parameters.alpha != DOUBLE_NOT_SET)
            alpha   = parameters.alpha;
        if (parameters.kmode_x != DOUBLE_NOT_SET)
            kmode_x = parameters.kmode_x;
        if (parameters.kmode_y != DOUBLE_NOT_SET)
            kmode_y = parameters.kmode_y;
    } else
        printf("No parameter file was passed through the command line. I will use the default parameters.\n");
    
    // Random initialization or read from file.
    const char sim_initial = INIT_NOFILE;
    const char sim_sort    = SORT_OUT_OF_PLACE_COUNTING_SORT;
    
    // Spatial parameters for initial density function.
    double *params;
    if (sim_distrib == LANDAU_1D_PROJ2D || sim_distrib == TWO_BEAMS_FIJALKOW) {
        params = malloc(2 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
    } else if (sim_distrib == LANDAU_2D || sim_distrib == TWO_STREAM_2D) {
        params = malloc(3 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
    } else if (sim_distrib == TWO_STREAM_BERNIER) {
        params = malloc(7 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 0.;
        params[4] = 1.;
        params[5] = 1.;
        params[6] = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        params = malloc(5 * sizeof(double));
        params[0] = alpha;
        params[1] = kmode_x;
        params[2] = kmode_y;
        params[3] = 3.5; // i_modes_x = 1, 2, 3
        params[4] = 3.5; // i_modes_y = 1, 2, 3
    } else
        params = malloc(0 * sizeof(double));
    
    // Velocity parameters for initial density function.
    double *speed_params;
    if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        speed_params = malloc(2 * sizeof(double));
        speed_params[0] = thermal_speed;
        speed_params[1] = 2. * sqrt(2.) * thermal_speed;
    } else {
        speed_params = malloc(1 * sizeof(double));
        speed_params[0] = thermal_speed;
    }
    
    // Mesh
    double x_min, y_min, x_max, y_max;
    const int num_cells_2d = ncx * ncy;
    if (sim_distrib == LANDAU_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 1.;
    } else if (sim_distrib == TWO_STREAM_1D_PROJ2D) {
        x_min = 0.;
        y_min = 0.;
        x_max = 26. * PI;
        y_max = 1.;
    } else {
        x_min = 0.;
        y_min = 0.;
        x_max = 2 * PI / kmode_x;
        y_max = 2 * PI / kmode_y;
    }
    cartesian_mesh_2d mesh = create_mesh_2d(ncx, ncy, x_min, x_max, y_min, y_max);
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    // Simulation parameters
    const double q             = -1.; // particle charge
    const double m             =  1.; // particle mass
    const double dt_q_over_m   = delta_t * q / m;
    const double dt_over_dx    = delta_t / mesh.delta_x;
    const double dt_over_dy    = delta_t / mesh.delta_y;
    const unsigned int sort_nb = NB_ITER_BETWEEN_SORT; // number of time steps between two sorts of the particles
    char simulation_name[42] = "Vlasov-Poisson 2d";
    char data_structure_name[42];
    char sort_name[42];
    sprintf(data_structure_name, "Structure of Arrays (%s)", space_filling_curves_names[I_CELL_2D_TYPE]);
    if (sim_sort == SORT_NO_SORT)
        sprintf(sort_name, "no sorting");
    else if (sim_sort == SORT_QUICK_SORT)
        sprintf(sort_name, "quick sort every %d", sort_nb);
    else if (sim_sort == SORT_OUT_OF_PLACE_COUNTING_SORT)
        sprintf(sort_name, "out of place counting sort every %d", sort_nb);
    else if (sim_sort == SORT_IN_PLACE_COUNTING_SORT)
        sprintf(sort_name, "in place counting sort every %d", sort_nb);
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = allocateMatrix(ncx, ncy);
    double* recv_buf = allocateMatrix(ncx, ncy);
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Temporary variables.
    double x, y;                                          // store the new position values
    int ic_x, ic_y;                                       // store the new position index values
    size_t i, j, i_time;                                  // loop indices
    double** q_times_rho = allocate_matrix(ncx+1, ncy+1); // to store q * rho = -rho
    
    // The following three matrices are (ncx+1) * (ncy+1) arrays, with the periodicity :
    //     M[ncx][ . ] = M[0][.]
    //     M[ . ][ncy] = M[.][0]
    // rho the charge density
    // Ex the electric field on the x-axis
    // Ey the electric field on the y-axis
    double rho_2d[ncx+1][ncy+1]; // Array allocated contiguously for reduction in OpenMP.
    double** Ex = allocate_matrix(ncx+1, ncy+1);
    double** Ey = allocate_matrix(ncx+1, ncy+1);
    
    // Diagnostic.
    double kmode = 0.;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
            kmode = kmode_x;
            break;
        case LANDAU_2D:
            kmode = sqrt(sqr(kmode_x) + sqr(kmode_y));
            break;
    }
    damping_values* landau_values = get_damping_values(kmode);
    const double er         = landau_values->er;
    const double psi        = landau_values->psi;
    const double omega_real = landau_values->omega_real;
    const double omega_imag = landau_values->omega_imag;
    double exval_ee, val_ee, t;
    const double landau_mult_cstt = sqr(4. * alpha * er) * PI / kmode_x; // Landau
    const int diag_energy_size = 5;
    const int diag_speed_size  = 5;
    double** diag_energy = allocate_matrix(num_iteration, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration, diag_speed_size);
    
    // Poisson solver.
    poisson_2d_solver solver = new_poisson_2d_fft_solver(mesh);
    
    // Particle sorter.
    particle_sorter_oop_2d sorter_oop = new_particle_sorter_oop_2d(nb_particles, num_cells_2d);
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    particle_2d* particles;
    particles = malloc(nb_particles * sizeof(particle_2d));
    if (!particles) {
        fprintf(stderr, "malloc failed to initialize particles.\n");
        exit(EXIT_FAILURE);
    }
    
    const int icx_param = ICX_PARAM_2D(ncx, ncy);
    const int icy_param = ICY_PARAM_2D(ncx, ncy);
    if (sim_initial == INIT_READ) {
        time_start = omp_get_wtime();
        read_particle_arrays_2d(mpi_world_size, nb_particles, mesh, &weight, &particles);
        if (mpi_rank == 0)
            printf("Read time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
        create_particle_arrays_2d(mpi_world_size, nb_particles, mesh, sim_distrib,
            params, speed_params, &weight, &particles);
        sort_particles_oop_2d(&sorter_oop, &particles); // Initial sort
        if (mpi_rank == 0)
            printf("Creation time (%ld particles) : %g sec\n", nb_particles, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%ldkk.dat", nb_particles / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d %d\n", ncx, ncy);
            fprintf(file_write_particles, "%ld\n", nb_particles);
            for (i = 0; i < nb_particles; i++) {
                fprintf(file_write_particles, "%d %.*g %.*g %.*g %.*g\n", particles[i].i_cell,
                  FLT_DECIMAL_DIG, particles[i].dx, FLT_DECIMAL_DIG, particles[i].dy,
                  DBL_DECIMAL_DIG, particles[i].vx, DBL_DECIMAL_DIG, particles[i].vy);
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x * mesh.delta_y);
    
  if (mpi_rank == 0) {
    printf("#VEC_ALIGN = %d\n", VEC_ALIGN);
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#alpha = %.*g\n", DBL_DECIMAL_DIG, alpha);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#y_min = %.*g\n", DBL_DECIMAL_DIG, y_min);
    printf("#y_max = %.*g\n", DBL_DECIMAL_DIG, y_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_2d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
//    printf("#y_field_factor = %.*g\n", DBL_DECIMAL_DIG, y_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#ncy = %d\n", ncy);
    printf("#nb_particles = %ld\n", nb_particles);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    for (i = 0; i < ncx + 1; i++)
        for (j = 0; j < ncy + 1; j++)
            rho_2d[i][j] = 0.;
    // Computes rho at initial time.
    #pragma omp parallel for private(i) reduction(+:rho_2d[0:ncx+1][0:ncy+1])
    for (i = 0; i < nb_particles; i++) {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
        ic_x = particles[i].icx;
        ic_y = particles[i].icy;
#else
        ic_x = ICX_FROM_I_CELL_2D(icx_param, particles[i].i_cell);
        ic_y = ICY_FROM_I_CELL_2D(icy_param, particles[i].i_cell);
#endif
        rho_2d[ic_x    ][ic_y    ] += (1. - particles[i].dx) * (1. - particles[i].dy);
        rho_2d[ic_x    ][ic_y + 1] += (1. - particles[i].dx) * (     particles[i].dy);
        rho_2d[ic_x + 1][ic_y    ] += (     particles[i].dx) * (1. - particles[i].dy);
        rho_2d[ic_x + 1][ic_y + 1] += (     particles[i].dx) * (     particles[i].dy);
    }
    convert_charge_to_rho_2d_contiguous(ncx, ncy, charge_factor, rho_2d);
    mpi_reduce_rho_2d_contiguous(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
    
    // Computes E at initial time.
    for (i = 0; i < ncx + 1; i++)
        for (j = 0; j < ncy + 1; j++)
            q_times_rho[i][j] = q * rho_2d[i][j];
    compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
    
    // Computes speeds half time-step backward (leap-frog method).
    #pragma omp parallel for private(i) firstprivate(dt_q_over_m)
    for (i = 0; i < nb_particles; i++) {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
        ic_x = particles[i].icx;
        ic_y = particles[i].icy;
#else
        ic_x = ICX_FROM_I_CELL_2D(icx_param, particles[i].i_cell);
        ic_y = ICY_FROM_I_CELL_2D(icy_param, particles[i].i_cell);
#endif
        particles[i].vx -= 0.5 * dt_q_over_m * (
              (     particles[i].dx) * (     particles[i].dy) * Ex[ic_x + 1][ic_y + 1]
            + (1. - particles[i].dx) * (     particles[i].dy) * Ex[ic_x    ][ic_y + 1]
            + (     particles[i].dx) * (1. - particles[i].dy) * Ex[ic_x + 1][ic_y    ]
            + (1. - particles[i].dx) * (1. - particles[i].dy) * Ex[ic_x    ][ic_y    ]);
        particles[i].vy -= 0.5 * dt_q_over_m * (
              (     particles[i].dx) * (     particles[i].dy) * Ey[ic_x + 1][ic_y + 1]
            + (1. - particles[i].dx) * (     particles[i].dy) * Ey[ic_x    ][ic_y + 1]
            + (     particles[i].dx) * (1. - particles[i].dy) * Ey[ic_x + 1][ic_y    ]
            + (1. - particles[i].dx) * (1. - particles[i].dy) * Ey[ic_x    ][ic_y    ]);
    }
    
    /********************************************************************************************
     *                               Beginning of main time loop                                *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics
        t = i_time * delta_t;
        exval_ee = landau_mult_cstt * exp(2. * omega_imag * t) *
               (0.5 + 0.5 * cos(2. * (omega_real * t - psi)));
        val_ee = normL2_field_2d(mesh, Ex) + normL2_field_2d(mesh, Ey);
        diag_energy[i_time][0] = t;                   // time
        diag_energy[i_time][1] = 0.5 * log(val_ee);   // Ex's log(L2-norm) (simulated)
        diag_energy[i_time][2] = 0.5 * log(exval_ee); // Ex's log(L2-norm) (expected)
        diag_energy[i_time][3] = val_ee;              // Ex's L2-norm (simulated)
        diag_energy[i_time][4] = exval_ee;            // Ex's L2-norm (expected)
        
        time_mark1 = omp_get_wtime();
        
        // Sorts the particles to be faster
        if (i_time % sort_nb == 0 && i_time > 0)
            sort_particles_oop_2d(&sorter_oop, &particles);
        time_mark2 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif
        
        for (i = 0; i < ncx + 1; i++)
            for (j = 0; j < ncy + 1; j++)
                rho_2d[i][j] = 0.;
        #pragma omp parallel reduction(+:rho_2d[0:ncx+1][0:ncy+1])
        {
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
            #pragma omp for private(i, x, y, ic_x, ic_y) firstprivate(icell_param)
#else
            #pragma omp for private(i, x, y, ic_x, ic_y) firstprivate(icell_param, icx_param, icy_param)
#endif
            for (i = 0; i < nb_particles; i++) {
                // Update v
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
                ic_x = particles[i].icx;
                ic_y = particles[i].icy;
#else
                ic_x = ICX_FROM_I_CELL_2D(icx_param, particles[i].i_cell);
                ic_y = ICY_FROM_I_CELL_2D(icy_param, particles[i].i_cell);
#endif
                particles[i].vx += dt_q_over_m * (
                         (     particles[i].dx) * (     particles[i].dy) * Ex[ic_x + 1][ic_y + 1]
                       + (1. - particles[i].dx) * (     particles[i].dy) * Ex[ic_x    ][ic_y + 1]
                       + (     particles[i].dx) * (1. - particles[i].dy) * Ex[ic_x + 1][ic_y    ]
                       + (1. - particles[i].dx) * (1. - particles[i].dy) * Ex[ic_x    ][ic_y    ]);
                particles[i].vy += dt_q_over_m * (
                         (     particles[i].dx) * (     particles[i].dy) * Ey[ic_x + 1][ic_y + 1]
                       + (1. - particles[i].dx) * (     particles[i].dy) * Ey[ic_x    ][ic_y + 1]
                       + (     particles[i].dx) * (1. - particles[i].dy) * Ey[ic_x + 1][ic_y    ]
                       + (1. - particles[i].dx) * (1. - particles[i].dy) * Ey[ic_x    ][ic_y    ]);
                // Update x
                x = ic_x + particles[i].dx + dt_over_dx * particles[i].vx;
                y = ic_y + particles[i].dy + dt_over_dy * particles[i].vy;
                if (x < 0. || x >= ncx)
                    x = modulo(x, ncx);
                if (y < 0. || y >= ncy)
                    y = modulo(y, ncy);
                ic_x = (int)floor(x);
                ic_y = (int)floor(y);
#ifdef PIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS
                particles[i].icx = ic_x;
                particles[i].icy = ic_y;
#endif
                particles[i].dx = (float)(x - ic_x);
                particles[i].dy = (float)(y - ic_y);
                particles[i].i_cell = COMPUTE_I_CELL_2D(icell_param, ic_x, ic_y);
                // Deposit
                rho_2d[ic_x    ][ic_y    ] += (1. - particles[i].dx) * (1. - particles[i].dy);
                rho_2d[ic_x    ][ic_y + 1] += (1. - particles[i].dx) * (     particles[i].dy);
                rho_2d[ic_x + 1][ic_y    ] += (     particles[i].dx) * (1. - particles[i].dy);
                rho_2d[ic_x + 1][ic_y + 1] += (     particles[i].dx) * (     particles[i].dy);
            }
        } // End parallel region
        time_mark3 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif
        
        // Converts accumulator to rho
        convert_charge_to_rho_2d_contiguous(ncx, ncy, charge_factor, rho_2d);
        mpi_reduce_rho_2d_contiguous(mpi_world_size, send_buf, recv_buf, ncx, ncy, rho_2d);
        time_mark4 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        for (i = 0; i < ncx + 1; i++)
            for (j = 0; j < ncy + 1; j++)
                q_times_rho[i][j] = q * rho_2d[i][j];
        compute_E_from_rho_2d_fft(solver, q_times_rho, Ex, Ey);
        time_mark5 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after sort
        diag_speed[i_time][2] = time_mark3; // after particle loop
        diag_speed[i_time][3] = time_mark4; // after all_reduce
        diag_speed[i_time][4] = time_mark5; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_sort          = 0.;
    time_particle_loop = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_sort          += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_particle_loop += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_mpi_allreduce += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_poisson       += diag_speed[i_time][4] - diag_speed[i_time][3];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    diag_energy_and_speed_sorting(mpi_rank,
        "diag_lee_4corners-opt-1loop.txt",   num_iteration, diag_energy_size, diag_energy,
        "diag_speed_4corners-opt-1loop.txt", num_iteration, diag_speed_size,  diag_speed);
    print_time_sorting(mpi_rank, mpi_world_size, nb_particles, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_particle_loop, 0., 0., time_sort, time_mpi_allreduce, time_poisson);
    
    free(params);
    free(speed_params);
    deallocate_matrix(q_times_rho, ncx+1, ncy+1);
    deallocate_matrix(Ex, ncx+1, ncy+1);
    deallocate_matrix(Ey, ncx+1, ncy+1);
    deallocate_matrix(diag_energy, num_iteration, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration, diag_speed_size);
    free(particles);

    free(send_buf);
    free(recv_buf);
    free_poisson_2d(&solver);
    free_particle_sorter_oop_2d(sorter_oop, num_cells_2d);
    pic_vert_free_RNG();
    MPI_Finalize();
    
    return 0;
}

