/**
 * PIC simulations in 1d.
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

//#define PAPI_LIB_INSTALLED

#include <math.h>                  // functions cos, log
#include <mpi.h>                   // constants MPI_COMM_WORLD, MPI_THREAD_FUNNELED
                                   // functions MPI_Init, MPI_Finalize, MPI_Comm_size, MPI_Comm_rank
#include <omp.h>                   // functions omp_get_wtime, omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                 // functions printf, fprintf (output strings on a stream), sprintf
                                   // constant  stderr (standard error output stream)
#include <stdlib.h>                // functions malloc, free ((de)allocate memory)
                                   //           exit (error handling)
                                   // constant  EXIT_FAILURE (error handling)
                                   // type      size_t
#ifdef PAPI_LIB_INSTALLED
#    include <papi.h>              // constants PAPI_OK, PAPI_L1_DCM, PAPI_L2_DCM, PAPI_L3_TCM...
                                   // functions PAPI_read_counters
                                   // type      long_long
#    include "papi_handlers.h"     // functions start_diag_papi, stop_diag_papi
#endif
#include "compiler_test.h"         // constant  PIC_VERT_OPENMP_4_0
#include "diagnostics.h"           // function  normL2_field_1d
#include "hdf5_io.h"               // function  plot_f_cartesian_mesh_2d
#include "initial_distributions.h" // constants PARAXIAL_1D
#include "math_functions.h"        // function  sqr, lowest_even_number_greater_or_equal_than
#include "matrix_functions.h"      // functions allocate_matrix, deallocate_matrix, allocate_array, deallocate_array
#include "meshes.h"                // type      cartesian_mesh_1d, cartesian_mesh_2d
                                   // function  create_mesh_1d, create_mesh_2d
#include "output.h"                // functions print_time_chunkbags, diag_energy_and_speed_chunkbags
#include "parameters.h"            // constants PI, EPSILON, VEC_ALIGN, DBL_DECIMAL_DIG, FLT_DECIMAL_DIG
#include "particle_type_soa_1d.h"  // types     particle_sorter_oop_1d
                                   // functions create_particle_arrays_1d, new_particle_sorter_oop_1d, sort_particles_oop_1d
#include "poisson_solvers.h"       // function  compute_E_from_rho_1d_trapezoidal
#include "random.h"                // macros    pic_vert_seed_double_RNG, pic_vert_free_RNG, pic_vert_next_random_double
#include "rho.h"                   // constant  NB_CORNERS_1D
                                   // functions mpi_reduce_rho_1d, reset_charge_1d_accumulator, convert_charge_to_rho_1d_per_per

/*
 * Initializes arrays of num_particle random particles following a given distribution
 * for positions, and following the gaussian distribution for speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] i_cell, dx, vx[num_particle] newly allocated arrays of randomized particles.
 */
void create_particle_arrays_1d_paraxial(int mpi_world_size, unsigned int num_particle,
        cartesian_mesh_1d mesh_creation_particles, cartesian_mesh_1d mesh,
        double* spatial_params, double* speed_params, float* weight,
        int* restrict * i_cell, float* restrict * dx, double* restrict * vx) {
    size_t j;
    double x, v_x;
    speeds_generator_1d speeds_generator = speed_generators_1d[PARAXIAL_1D];
    
    const double x_range = mesh_creation_particles.x_max - mesh_creation_particles.x_min;
    
    *weight = (float)1. / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        x = x_range * pic_vert_next_random_double() + mesh_creation_particles.x_min;
        x = (x - mesh.x_min) / mesh.delta_x;
        (*speeds_generator)(speed_params, &v_x);
        (*i_cell)[j] = (int)x;
        (*dx)[j]     = (float)(x - (int)x);
        (*vx)[j]     = v_x;
    }
}

/*****************************************************************************
 *                             Simulation 1d                                 *
 *****************************************************************************/

#define SORT_IN_PLACE_COUNTING_SORT     0 // Time = O(n), Memory = NB_PARTICLE * sizeof(particle)
#define SORT_OUT_OF_PLACE_COUNTING_SORT 1 // Time = O(n), Memory = 2 * NB_PARTICLE * sizeof(particle)
#define SORT_QUICK_SORT                 2 // Time = O(n*log(n)), Memory = NB_PARTICLE * sizeof(particle)
                                          // Not implemented (multiple arrays to sort, cannot use qsort from stdlib).
#define SORT_NO_SORT                    3

#define INIT_READ   0
#define INIT_WRITE  1
#define INIT_NOFILE 2

int main(int argc, char** argv) {
    // Timing
    double time_start, time_simu;
    double time_mark1, time_mark2, time_mark3, time_mark4, time_mark5, time_mark6, time_mark7;
    double time_sort, time_update_v, time_update_x, time_deposit, time_mpi_allreduce, time_poisson;
    
#ifdef PAPI_LIB_INSTALLED
    // Performance counters
    int papi_num_events = 3;
    int Events[papi_num_events];
    Events[0] = PAPI_L1_DCM;
    Events[1] = PAPI_L2_DCM;
    Events[2] = PAPI_L3_TCM;
    long_long values[papi_num_events];
    FILE* file_diag_papi;
#endif
    
    // Type of the simulation
    const unsigned char sim_distrib = PARAXIAL_1D;
    const char sim_initial = INIT_NOFILE;
    const char sim_sort    = SORT_OUT_OF_PLACE_COUNTING_SORT;

    // Electric parameters, for the Paraxial 1d :
    const double epsilon = 0.01;
    double *params = allocate_array(0);
    
    // Other physical parameters
    const double thermal_speed =  0.0727518214392; // thermal speed
    double *speed_params;
    speed_params = malloc(1 * sizeof(double));
    speed_params[0] = thermal_speed;
    
    // Meshes
    // The particles must be created in [-0.75 ; 0.75]...
    const double le075 = 0.75;
    const double x_min_creation_particles = -le075;
    const double x_max_creation_particles =  le075;
    const int ncx_creation_particles = 64;
    cartesian_mesh_1d mesh_creation_particles = create_mesh_1d(ncx_creation_particles, x_min_creation_particles, x_max_creation_particles);
    
    // ...however, because we have free boundaries, they might go out of this initial mesh, hence the need for a bigger mesh.
    const double x_min = -1.5;
    const double x_max =  1.5;
    const int ncx = lowest_even_number_greater_or_equal_than((int)((x_max - x_min) / (x_max_creation_particles - x_min_creation_particles) * ncx_creation_particles));
    cartesian_mesh_1d mesh = create_mesh_1d(ncx, x_min, x_max);
    
    const int num_cells_1d = ncx;
    
    // Vectorization of the deposit
    int corner;
/*
 *    dx
 *   <———>
 *   =———|——————x
 *   =xxxO======x
 *   =———|——————x
 *
 * The "=" corner is the left corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "=" area. This fraction is equal to
 * (1. - dx), hence the LEFT coefficients.
 * The "x" corner is the right corner. We deposit on this corner the fraction of the
 * particle charge corresponding to the surface of the "x" area. This fraction is equal to
 * (     dx), hence the RIGHT coefficients.
 */
    // Space coeffs                                                             LEFT  RIGHT
    const float coeffs_x[NB_CORNERS_1D] __attribute__((aligned(VEC_ALIGN))) = {  1.,  0.};
    const float  signs_x[NB_CORNERS_1D] __attribute__((aligned(VEC_ALIGN))) = { -1.,  1.};
    
    // Simulation parameters
    const double delta_t             = DELTA_T;       // time step
    const double dt_over_dx          = delta_t / mesh.delta_x;
    const unsigned int num_iteration = NB_ITER;       // number of time steps
    const unsigned int sort_nb       = 20;            // number of time steps between two sorts of the particles
    char simulation_name[42]     = "Highly oscillatory Vlasov equation 1d";
    char data_structure_name[42] = "Structure of Arrays";
    char sort_name[42];
    if (sim_sort == SORT_NO_SORT)
        sprintf(sort_name, "no sorting");
    else if (sim_sort == SORT_QUICK_SORT)
        sprintf(sort_name, "quick sort every %d", sort_nb);
    else if (sim_sort == SORT_OUT_OF_PLACE_COUNTING_SORT)
        sprintf(sort_name, "out of place counting sort every %d", sort_nb);
    else if (sim_sort == SORT_IN_PLACE_COUNTING_SORT)
        sprintf(sort_name, "in place counting sort every %d", sort_nb);
    
    // MPI + OpenMP parallelism
    int mpi_world_size, mpi_rank;
    double* send_buf = allocate_array(ncx);
    double* recv_buf = allocate_array(ncx);
    int mpi_thread_support;
    MPI_Init_thread(&argc, &argv, MPI_THREAD_FUNNELED, &mpi_thread_support);
    MPI_Comm_size(MPI_COMM_WORLD, &mpi_world_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
    int num_threads;
    int thread_id;
    int offset;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Hdf5 outputs.
    H5open(); // Initializes the HDF5 library.
    const double hdf5_x_min  =  -1.2 * le075;
    const double hdf5_x_max  =   1.2 * le075;
    const double hdf5_vx_min = -12.0 * thermal_speed;
    const double hdf5_vx_max =  12.0 * thermal_speed;
    const int hdf5_ncx = lowest_even_number_greater_or_equal_than((int)((hdf5_x_max - hdf5_x_min) / (x_max_creation_particles - x_min_creation_particles) * ncx_creation_particles));
    const int hdf5_ncvx = 256;
    cartesian_mesh_2d mesh_xvx = create_mesh_2d(hdf5_ncx, hdf5_ncvx, hdf5_x_min, hdf5_x_max, hdf5_vx_min, hdf5_vx_max);
    const int hdf5_nb_outputs = 1000; // With more than 1,000 outputs, VisIt crashes.
    int hdf5_delta_step_output = num_iteration / hdf5_nb_outputs;
    if (hdf5_delta_step_output == 0)
        hdf5_delta_step_output = 1;
    double distribution_function[hdf5_ncx+1][hdf5_ncvx+1]; // Array allocated contiguously for hdf5 outputs.
    double hdf5_vx;
    int hdf5_ic_vx, hdf5_ic_x;
    float hdf5_dvx;
    size_t j;
    float weight_x_vx = (float)(x_max - x_min) / ((float)mpi_world_size * (float)NB_PARTICLE); // Weight for deposit on the x-vx plane.
    
    // Temporary variables.
    double x;         // store the new position values
    size_t i, i_time; // loop indices
    
    // The following matrices are (ncx+1) arrays, with the periodicity :
    //     M[ncx] = M[0]
    // rho the charge density
    // Ex the electric field on the x-axis
    double* rho_1d = allocate_array(ncx+1);
    double* Ex = allocate_array(ncx+1);
    double* E_field = allocate_array(ncx+1);
    
    /* For each cell, the 2 corners values ; for vectorization, each thread has its own copy */
    double* charge_accu;
    if (posix_memalign((void**)&charge_accu, VEC_ALIGN, num_cells_1d * NB_CORNERS_1D * num_threads * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize charge_accu.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(charge_accu, VEC_ALIGN);
#else
    charge_accu = __builtin_assume_aligned(charge_accu, VEC_ALIGN);
#endif
    
    // Diagnostic energy.
    double val_ee, t;
    const int diag_energy_size = 3;
    const int diag_speed_size  = 7;
    double** diag_energy = allocate_matrix(num_iteration, diag_energy_size);
    double** diag_speed  = allocate_matrix(num_iteration, diag_speed_size);
    
    // Particle sorter.
    particle_sorter_oop_1d sorter_oop = new_particle_sorter_oop_1d(NB_PARTICLE, num_cells_1d);
    
    /* A "numerical particle" (we also say "macro particle") represents several
     * physical particles. The weight is the number of physical particles it
     * represents. The more particles we have in the simulation, the less this
     * weight will be. A numerical particle may represent a different number of
     * physical particles than another numerical particle, even though in this
     * simulation it's not the case.
     */
    float weight;
    /* Cell index of the particle in [|0 ; ncx|[ (see the drawing above) */
    int* i_cell;
    /* x_mapped - index_x, a number in [0 ; 1[       (see the drawing above) */
    float* dx;
    /* Speed of the particle on the x-axis */
    double* vx;
    if (posix_memalign((void**)&i_cell, VEC_ALIGN, NB_PARTICLE * sizeof(int))) {
        fprintf(stderr, "posix_memalign failed to initialize i_cell.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&dx, VEC_ALIGN, NB_PARTICLE * sizeof(float))) {
        fprintf(stderr, "posix_memalign failed to initialize dx.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&vx, VEC_ALIGN, NB_PARTICLE * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize vx.\n");
        exit(EXIT_FAILURE);
    }
#ifdef __INTEL_COMPILER
    __assume_aligned(i_cell, VEC_ALIGN);
    __assume_aligned(dx,  VEC_ALIGN);
    __assume_aligned(vx,  VEC_ALIGN);
#else
    i_cell = __builtin_assume_aligned(i_cell, VEC_ALIGN);
    dx  = __builtin_assume_aligned(dx,  VEC_ALIGN);
    vx  = __builtin_assume_aligned(vx,  VEC_ALIGN);
#endif
    if (sim_initial == INIT_READ) {
    // WARNING : if you read NB_PARTICLE from a file, the particle loop will not
    // be vectorized with gcc because it doesn't know the value of NB_PARTICLE !
        char filename[30];
        sprintf(filename, "initial_particles_%dkk.dat", NB_PARTICLE / 1000000);
        FILE* file_read_particles = fopen(filename, "r");
        if (!file_read_particles) {
            printf("%s doesn't exist.\n", filename);
            exit(EXIT_FAILURE);
        }
        int throw_that_number_x;
        unsigned int throw_that_number_also;
        if (fscanf(file_read_particles, "%d %u", &throw_that_number_x,
                &throw_that_number_also) < 2) {
            printf("%s should begin with ncx NB_PARTICLE.\n", filename);
            exit(EXIT_FAILURE);
        }
        if (throw_that_number_x != ncx) {
            printf("I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
            exit(EXIT_FAILURE);
        }
        if (throw_that_number_also != NB_PARTICLE) {
            fprintf(stderr, "I expected NB_PARTICLE = %d but found %d in the input file.\n", NB_PARTICLE, throw_that_number_also);
            exit(EXIT_FAILURE);
        }
        weight = (float)1. / ((float)mpi_world_size * (float)NB_PARTICLE);
        time_start = omp_get_wtime();
        for (i = 0; i < NB_PARTICLE; i++) {
            if (fscanf(file_read_particles, "%d %f %lf", &i_cell[i], &dx[i], &vx[i]) < 3) {
                fprintf(stderr, "I expected %d particles but there are less in the input file.\n", NB_PARTICLE);
                exit(EXIT_FAILURE);
            }
        }
        fclose(file_read_particles);
        if (mpi_rank == 0)
            printf("Read time (%d particles) : %g sec\n", NB_PARTICLE, (double) (omp_get_wtime() - time_start));
    } else {
        pic_vert_seed_double_RNG(mpi_rank);
//         Different random numbers at each run.
//         pic_vert_seed_double_RNG(seed_64bits(mpi_rank));
        // Creation of random particles and sorting.
        time_start = omp_get_wtime();
        create_particle_arrays_1d_paraxial(mpi_world_size, NB_PARTICLE, mesh_creation_particles, mesh,
            params, speed_params, &weight, &i_cell, &dx, &vx);
        sort_particles_oop_1d(&sorter_oop, &i_cell, &dx, &vx); // Initial sort
        if (mpi_rank == 0)
            printf("Creation time (%d particles) : %g sec\n", NB_PARTICLE, (double) (omp_get_wtime() - time_start));
        if (sim_initial == INIT_WRITE) {
            // Export the particles.
            char filename[30];
            sprintf(filename, "initial_particles_%dkk.dat", NB_PARTICLE / 1000000);
            FILE* file_write_particles = fopen(filename, "w");
            fprintf(file_write_particles, "%d\n", ncx);
            fprintf(file_write_particles, "%d\n", NB_PARTICLE);
            for (i = 0; i < NB_PARTICLE; i++) {
                fprintf(file_write_particles, "%d %.*g %.*g\n", i_cell[i],
                  FLT_DECIMAL_DIG, dx[i], DBL_DECIMAL_DIG, vx[i]);
            }
            fclose(file_write_particles);
            MPI_Finalize();
            return 0;
        }
    }
    
/*
    // The particles were initialized with icells in [0 ; ncx_creation_particles] which corresponds to
    // [-le075 ; le075] but are now in [-1 ; 1] which corresponds to a bigger interval [0 ; ncx].
    // Thus we have to move them all by (ncx - ncx_creation_particles) / 2.
    int shift = (ncx - ncx_creation_particles) / 2;
    for (i = 0; i < NB_PARTICLE; i++)
        i_cell[i] += shift;
*/
    
    // Because the weight is constant, the whole array can be multiplied by weight just once.
    // Because charge is the charge MASS and not the charge DENSITY, we have to divide.
    const double charge_factor = weight / (mesh.delta_x);
    // We just use the electric fields to update the speed, with always the same multiply.
    const double x_field_factor = delta_t * dt_over_dx;
    const double eps_factor     = delta_t * dt_over_dx / epsilon;
    
  if (mpi_rank == 0) {
    printf("#mpi_world_size = %d\n", mpi_world_size);
    printf("#num_threads = %d\n", num_threads);
    printf("#x_min = %.*g\n", DBL_DECIMAL_DIG, x_min);
    printf("#x_max = %.*g\n", DBL_DECIMAL_DIG, x_max);
    printf("#delta_t = %.*g\n", DBL_DECIMAL_DIG, delta_t);
    printf("#thermal_speed = %.*g\n", DBL_DECIMAL_DIG, thermal_speed);
    printf("#initial_function_case = %s\n", distribution_names_1d[sim_distrib]);
//    printf("#weight = %.*g\n", DBL_DECIMAL_DIG, weight);
//    printf("#charge_factor = %.*g\n", DBL_DECIMAL_DIG, charge_factor);
//    printf("#x_field_factor = %.*g\n", DBL_DECIMAL_DIG, x_field_factor);
    printf("#ncx = %d\n", ncx);
    printf("#NB_PARTICLE = %d\n", NB_PARTICLE);
    printf("#num_iteration = %d\n", num_iteration);
  }
    
    reset_charge_1d_accumulator(ncx, num_threads, charge_accu);
    // Computes rho at initial time.
    #pragma omp parallel private(thread_id, offset)
    {
        thread_id = omp_get_thread_num();
        offset = thread_id * NB_CORNERS_1D * num_cells_1d;
        #pragma omp for private(i, corner)
        for (i = 0; i < NB_PARTICLE; i++) {
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp simd aligned(dx, coeffs_x, signs_x:VEC_ALIGN)
#endif
            for (corner = 0; corner < NB_CORNERS_1D; corner++) {
                charge_accu[offset + NB_CORNERS_1D * i_cell[i] + corner] +=
                    coeffs_x[corner] + signs_x[corner] * dx[i];
            }
        }
    } // End parallel region
    convert_charge_to_rho_1d_per_per(charge_accu, num_threads, ncx, charge_factor, rho_1d);
    mpi_reduce_rho_1d(mpi_world_size, send_buf, recv_buf, ncx, rho_1d);
    
    // Computes E at initial time.
    compute_E_from_rho_1d_trapezoidal(mesh, rho_1d, Ex);
    for (i = 0; i < ncx + 1; i++)
        E_field[i] = x_field_factor * Ex[i];
    
/*    
  if (mpi_rank == 0) {
    // Test the initial rho.
    FILE* file_diag_rho = fopen("test_rho_1d.dat", "w");
    for (i = 0; i < ncx; i++)
        fprintf(file_diag_rho, "%ld %f\n", i, rho_1d[i]);
    fclose(file_diag_rho);
    // Test the initial Electric field.
    FILE* file_diag_Ex = fopen("test_Ex_1d.dat", "w");
    for (i = 0; i < ncx; i++)
        fprintf(file_diag_Ex, "%ld %g\n", i, Ex[i]);
    fclose(file_diag_Ex);
  }
    MPI_Finalize();
    exit(0);
*/
    
    // Computes speeds half time-step backward (leap-frog method).
    // WARNING : starting from here, v doesn't represent the speed, but speed * dt / dx.
    #pragma omp parallel for private(i)
    for (i = 0; i < NB_PARTICLE; i++) {
        vx[i] = vx[i] * dt_over_dx - 0.5 * (
                      (     dx[i]) * E_field[i_cell[i] + 1]
                    + (1. - dx[i]) * E_field[i_cell[i]    ]
                    - (x_min + (i_cell[i] + dx[i]) * mesh.delta_x) * eps_factor);
    }
    
    /********************************************************************************************
     *                               Beginning of main time loop                                *
     ********************************************************************************************/
#ifdef PAPI_LIB_INSTALLED
    start_diag_papi(&file_diag_papi, "diag_papi_4corners-opt.txt", papi_num_events, Events);
#endif
    
    time_start = omp_get_wtime();
    for (i_time = 0; i_time < num_iteration; i_time++) {
        // Diagnostics energy
        t = i_time * delta_t;
        val_ee = normL2_field_1d(mesh, Ex);
        diag_energy[i_time][0] = t;                 // time
        diag_energy[i_time][1] = 0.5 * log(val_ee); // E_field's log(L2-norm) (simulated)
        diag_energy[i_time][2] = val_ee;            // E_field's L2-norm (simulated)
        // Diagnostics hdf5.
        if (i_time % hdf5_delta_step_output == 0) {
            for (i = 0; i <= hdf5_ncx; i++)
                for (j = 0; j <= hdf5_ncvx; j++)
                    distribution_function[i][j] = 0;
            // We might parallelize with OpenMP / Vectorization this deposit (like in 2d simulations),
            // but in fact we have so many iterations (50,000) for so few outputs (1,000) that it's
            // not useful.
            for (i = 0; i < NB_PARTICLE; i++) {
                hdf5_vx = (vx[i] / dt_over_dx - hdf5_vx_min) / mesh_xvx.delta_y;
                hdf5_ic_vx = (int)hdf5_vx;
                hdf5_dvx = (float)(hdf5_vx - hdf5_ic_vx);
                hdf5_ic_x = i_cell[i] + (hdf5_ncx - ncx) / 2;
                if ((hdf5_ic_vx >= 0) && (hdf5_ic_vx < hdf5_ncvx) && (hdf5_ic_x >= 0) && (hdf5_ic_x < hdf5_ncx)) {
                    distribution_function[hdf5_ic_x    ][hdf5_ic_vx    ] += (1. - dx[i]) * (1. - hdf5_dvx);
                    distribution_function[hdf5_ic_x    ][hdf5_ic_vx + 1] += (1. - dx[i]) * (     hdf5_dvx);
                    distribution_function[hdf5_ic_x + 1][hdf5_ic_vx    ] += (     dx[i]) * (1. - hdf5_dvx);
                    distribution_function[hdf5_ic_x + 1][hdf5_ic_vx + 1] += (     dx[i]) * (     hdf5_dvx);
                }
            }
            // Periodicity
            for (i = 0; i < hdf5_ncx + 1; i++) {
                distribution_function[i][    0    ] += distribution_function[i][hdf5_ncvx];
                distribution_function[i][hdf5_ncvx]  = distribution_function[i][    0    ];
            }
            for (j = 0; j < hdf5_ncvx + 1; j++) {
                distribution_function[   0    ][j] += distribution_function[hdf5_ncx][j];
                distribution_function[hdf5_ncx][j]  = distribution_function[   0    ][j];
            }
            for (i = 0; i <= hdf5_ncx; i++)
                for (j = 0; j <= hdf5_ncvx; j++)
                    distribution_function[i][j] *= weight_x_vx / (mesh_xvx.delta_x * mesh_xvx.delta_y);
            plot_f_cartesian_mesh_2d(i_time / hdf5_delta_step_output + 1, distribution_function[0], mesh_xvx, t);
        }
        
        time_mark1 = omp_get_wtime();
        
        // Sorts the particles to be faster
        if (i_time % sort_nb == 0 && i_time > 0)
            sort_particles_oop_1d(&sorter_oop, &i_cell, &dx, &vx);
        time_mark2 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
#endif
        
        reset_charge_1d_accumulator(ncx, num_threads, charge_accu);
        #pragma omp parallel private(thread_id, offset)
        {
            thread_id = omp_get_thread_num();
            offset = thread_id * NB_CORNERS_1D * num_cells_1d;
            // Update v
            #pragma omp for private(i)
            for (i = 0; i < NB_PARTICLE; i++) {
                vx[i] += (     dx[i]) * E_field[i_cell[i] + 1]
                       + (1. - dx[i]) * E_field[i_cell[i]    ]
                       - (x_min + (i_cell[i] + dx[i]) * mesh.delta_x) * eps_factor;
            }
            time_mark3 = omp_get_wtime();
            // Update x
#ifdef PIC_VERT_OPENMP_4_0
            #pragma omp for simd private(x) aligned(i_cell, dx, vx:VEC_ALIGN)
#else
            #pragma omp for private(x)
#endif
            for (i = 0; i < NB_PARTICLE; i++) {
                x = i_cell[i] + dx[i] + vx[i] / epsilon;
                i_cell[i] = (int)x;
                dx[i] = (float)(x - (int)x);
                // This prevents vectorization but is needed to check that the free boundaries are well chosen.
                if ((int)x < 0 || (int)x >= ncx) {
                    fprintf(stderr, "A particle fell out of [0; ncx], ic_x = %d.\n", (int)x);
                    exit(EXIT_FAILURE);
                }
            }
            time_mark4 = omp_get_wtime();
            // Deposit
            #pragma omp for private(i, corner)
            for (i = 0; i < NB_PARTICLE; i++) {
#ifdef PIC_VERT_OPENMP_4_0
                #pragma omp simd aligned(dx, coeffs_x, signs_x:VEC_ALIGN)
#endif
                for (corner = 0; corner < NB_CORNERS_1D; corner++) {
                    charge_accu[offset + NB_CORNERS_1D * i_cell[i] + corner] +=
                        coeffs_x[corner] + signs_x[corner] * dx[i];
                }
            }
        } // End parallel region
        time_mark5 = omp_get_wtime();
        
#ifdef PAPI_LIB_INSTALLED
        /* Read the counters */
        if (PAPI_read_counters(values, papi_num_events) != PAPI_OK)
            handle_error(1);
        fprintf(file_diag_papi, "%ld", i_time + 1);
        for (i = 0; i < papi_num_events; i++)
            fprintf(file_diag_papi, " %lld", values[i]);
        fprintf(file_diag_papi, "\n");
#endif
        
        // Converts accumulator to rho
        convert_charge_to_rho_1d_per_per(charge_accu, num_threads, ncx, charge_factor, rho_1d);
        mpi_reduce_rho_1d(mpi_world_size, send_buf, recv_buf, ncx, rho_1d);
        time_mark6 = omp_get_wtime();
        
        // Solves Poisson and updates the field E
        compute_E_from_rho_1d_trapezoidal(mesh, rho_1d, Ex);
        for (i = 0; i < ncx + 1; i++)
            E_field[i] = x_field_factor * Ex[i];
        time_mark7 = omp_get_wtime();
        
        // Diagnostics speed
        diag_speed[i_time][0] = time_mark1; // beginining of time loop
        diag_speed[i_time][1] = time_mark2; // after sort
        diag_speed[i_time][2] = time_mark3; // after update v
        diag_speed[i_time][3] = time_mark4; // after update x
        diag_speed[i_time][4] = time_mark5; // after update deposit
        diag_speed[i_time][5] = time_mark6; // after all_reduce
        diag_speed[i_time][6] = time_mark7; // after Poisson solve
    }
    time_simu = (double) (omp_get_wtime() - time_start);
    time_sort          = 0.;
    time_update_v      = 0.;
    time_update_x      = 0.;
    time_deposit       = 0.;
    time_mpi_allreduce = 0.;
    time_poisson       = 0.;
    for (i_time = 0; i_time < num_iteration; i_time++) {
        time_sort          += diag_speed[i_time][1] - diag_speed[i_time][0];
        time_update_v      += diag_speed[i_time][2] - diag_speed[i_time][1];
        time_update_x      += diag_speed[i_time][3] - diag_speed[i_time][2];
        time_deposit       += diag_speed[i_time][4] - diag_speed[i_time][3];
        time_mpi_allreduce += diag_speed[i_time][5] - diag_speed[i_time][4];
        time_poisson       += diag_speed[i_time][6] - diag_speed[i_time][5];
    }
    
#ifdef PAPI_LIB_INSTALLED
    stop_diag_papi(file_diag_papi, papi_num_events, values);
#endif
    diag_energy_and_speed_sorting(mpi_rank,
        "diag_lee_2corners.txt",   num_iteration, diag_energy_size, diag_energy,
        "diag_speed_2corners.txt", num_iteration, diag_speed_size,  diag_speed);
    print_time_sorting(mpi_rank, mpi_world_size, NB_PARTICLE, num_iteration, time_simu, simulation_name, data_structure_name, sort_name,
        time_update_v, time_update_x, time_deposit, time_sort, time_mpi_allreduce, time_poisson);
    
    free(speed_params);
    deallocate_array(rho_1d, ncx+1);
    deallocate_array(Ex, ncx+1);
    deallocate_array(E_field, ncx+1);
    deallocate_matrix(diag_energy, num_iteration, diag_energy_size);
    deallocate_matrix(diag_speed,  num_iteration, diag_speed_size);
    free(i_cell);
    free(dx);
    free(vx);
    free(charge_accu);
    
    free(send_buf);
    free(recv_buf);
    free_particle_sorter_oop_1d(sorter_oop, num_cells_1d);
    pic_vert_free_RNG();
    MPI_Finalize();
    H5close(); // Flushes all data to disk, closes all open identifiers, and cleans up memory.
    
    return 0;
}

