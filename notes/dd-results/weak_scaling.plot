set title '50 million particles / socket - Grid 64x64 / socket - 100 iterations'

set terminal pdf color enhanced font 'Arial,14'
set output "weak_scaling.pdf"
set autoscale

set border 15 back lw 0.5# eliminer le bord inferieur du rectangle
# back = les aretes du rectangle au-dessous des courbes representees
# front= les aretes du rectangle au-dessus des courbes representees

unset colorbox

set logscale x
set xlabel "Number of sockets"
set xtics (4,16,64256,1024)
set xrange [4:64]

set logscale x2
set x2label "Number of cores"
set x2tics (96,384,1536,6144,24576)
set x2range [96:1536]

set key top left
set ylabel "Execution time (seconds)"
set ytic auto

set style line 1 linewidth 2.2 dashtype (10,10) linecolor rgb "blue"
set style line 2 linewidth 2.2 dashtype (5,5) linecolor rgb "blue"
set style increment userstyle

plot \
  "weak_scaling.dat" using 1:2 pointtype 3 pointsize 0.5 title 'Total execution time' with linespoints, \
  "weak_scaling.dat" using 1:3 title 'Communication time - MPI\_Allgatherv' with lines, \
  "weak_scaling.dat" using ($1):($3+1):4 notitle with labels textcolor rgb "blue"

