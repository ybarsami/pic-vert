Creation time (-1095017505 particles) : 118.799 sec
#VEC_ALIGN = 64
#OMP_TILE_SIZE = 4
#OMP_TILE_BORDERS = 1
#mpi_world_size = 64
#num_threads = 24
#alpha = 0.01
#x_min = 0
#x_max = 12.566370614359172
#y_min = 0
#y_max = 12.566370614359172
#delta_t = 0.025000000000000001
#thermal_speed = 1
#initial_function_case = LANDAU_2D
#ncx = 512
#ncy = 512
#num_particle = -1095017505
#num_iteration = 100
Particle lost : from (188, 82) to (181, 81), at iteration 8, in local box [188; 259] x [60; 131].
Particle lost : from (416, 195) to (415, 202), at iteration 10, in local box [380; 451] x [124; 195].
Particle lost : from (195, 314) to (202, 312), at iteration 10, in local box [124; 195] x [252; 323].
Particle lost : from (217, 323) to (216, 330), at iteration 24, in local box [188; 259] x [252; 323].
Particle lost : from (468, 60) to (468, 53), at iteration 60, in local box [444; 515] x [60; 131].
---------- Vlasov-Poisson 2d - Array of Concurrent Chunkbags of AoS (4 private that can be NULL + 1 shared / cell) ----------
---------- always sort ----------
Execution time (total)    : 19.427 s
Array of particles update : 13.357 s (68.7545%)
- Including particle loop : 13.2998 s (68.4605%)
- Including append        : 0.057121 s (0.294029%)
Reduction of rho          : 4.78058 s (24.6079%)
Poisson solver            : 0.983883 s (5.06451%)
Nb. particles / s : 1.64716e+10
Time / particule / iteration : 0.0417411 ns

