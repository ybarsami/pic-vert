#!/bin/bash
echo "#Nb of sockets | Timings, chunkbags | MPI_Allgatherv | Communication breakdown" > time.dat
for nb_sockets in 4 16 64
do
    echo -n "${nb_sockets} " >> time.dat

    grep -r "Execution time (total)    : " "std_${nb_sockets}socket.txt" > "${nb_sockets}.txt"
    sed -i 's|Execution time (total)    : ||g' "${nb_sockets}.txt"
    sed -i 's|s||g' "${nb_sockets}.txt"
    cat "${nb_sockets}.txt" | tr -d "\n" >> time.dat

    grep -r "Reduction of rho          : " "std_${nb_sockets}socket.txt" > "${nb_sockets}.txt"
    sed -i 's|Reduction of rho          : ||g' "${nb_sockets}.txt"
    sed -i 's|s||g' "${nb_sockets}.txt"
    sed -i 's|(||g' "${nb_sockets}.txt"
    sed -i 's|)||g' "${nb_sockets}.txt"
    cat "${nb_sockets}.txt" >> time.dat

    rm "${nb_sockets}.txt"
done

