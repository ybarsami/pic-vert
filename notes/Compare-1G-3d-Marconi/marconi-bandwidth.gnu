set terminal pdf color enhanced font ",10"
set output 'marconi-bandwidth.pdf'
set boxwidth 0.9 absolute
set style fill solid 1.00 border lt -1
set key inside left top vertical font ",12"
set style histogram gap 1.
set style data histograms
#set title "Marconi (Intel Skylake) - 900 million particles - 128 x 128\nMemory Bandwidth (GB/s) with respect to the number of threads" font ",12"
set tics font ",12"
set xrange [-0.6: 5.7]
set yrange [0.: 70.]
set ylabel "Memory Bandwidth (GB/s)" font ",12"
plot 'marconi-bandwidth.dat' using 2:xtic(1) ti col, '' u 3 ti col, \
     '' u ($0-0.8):($3+2):4 notitle with labels
