#include <omp.h>                                            // function  omp_get_num_threads
#include <stdbool.h>                                        // constant  true
#include <stdio.h>                                          // functions fprintf, fopen, fclose, fscanf, sprintf
                                                            // constant  stderr (standard error output stream)
#include <stdlib.h>                                         // functions exit (error handling), malloc, free ((de)allocate memory)
                                                            // constant  EXIT_FAILURE (error handling)
                                                            // type      size_t
#include "initial_distributions.h"                          // types     speeds_generator_2d, distribution_function_2d, max_distribution_function
                                                            // variables speed_generators_2d3v, distribution_funs_2d3v, distribution_maxs_2d3v
#include "matrix_functions.h"                               // functions allocate_int_array, allocate_int_matrix, allocate_aligned_int_matrix
#include "meshes.h"                                         // type      cartesian_mesh_2d
#include "parameters.h"                                     // constants DBL_DECIMAL_DIG, FLT_DECIMAL_DIG
#include "particle_type_concurrent_chunkbags_of_aos_2d3v.h" // types     particle, chunk, bag
                                                            // variables free_chunks, free_index, FREELIST_SIZE
#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
                                                            // constant  ODD_ITERATION
#endif
#include "random.h"                                         // function  pic_vert_next_random_double
#include "space_filling_curves.h"                           // macros    COMPUTE_I_CELL_2D, I_CELL_PARAM_2D

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 *            Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |       +—————————————————————————————————————+
 *           |                                             |
 *           |     +——————+     +— ...     ——+     +————+  |
 *           |     |      |     |            |     |    |  |
 *           v     |      v     |            v     |    v  v
 *       |———————| |  |———————| |        |———————| |  |———————|
 *       |       | |  |       | |        |       | |  |       |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *       |       |    |       |          |       |    |       |
 *  size |  16   |    |  11   |          |  13   |    |  17   |
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *     0 |       |    |       |          |       |    |       |
 *     1 |       |    |       |          |       |    |       |
 *     2 |       |    |       |          |       |    |       |
 *     3 |       |    |       |          |       |    |       |
 *     4 |       |    |       |          |       |    |       |
 *     5 |       |    |       |          |       |    |       |
 *     6 |       |    |       |          |       |    |       |
 *     7 |       |    |       |          |       |    |       |
 *     8 |       |    |       |          |       |    |       |
 *     9 |       |    |       |          |       |    |       |
 *    10 |       |    |       |          |       |    |       |
 *    11 |       |    |   ?   |          |       |    |       |
 *    12 |       |    |   ?   |          |       |    |       |
 *    13 |       |    |   ?   |          |   ?   |    |       |
 *    14 |       |    |   ?   |          |   ?   |    |       |
 *    15 |       |    |   ?   |          |   ?   |    |       |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *
 */

chunk*** free_chunks;

#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
// Special chunks management
int number_of_special_chunks_per_parity; // Number of chunks needed to initialize each bag in the append phase.
int** special_chunks_ids;                // Bijection between [0 ; nb_bags_per_cell[ x [0 ; num_cells_2d[ and [0 ; number_of_special_chunks_per_parity[
chunk* special_chunks;
chunk* beginning_of_special_chunks;
chunk* end_of_special_chunks;

void bag_init_special_chunks(bag* b, int id_bag, int id_cell, int iteration_parity) {
  int id_chunk = special_chunks_ids[id_bag][id_cell];
  chunk* c = &special_chunks[iteration_parity * number_of_special_chunks_per_parity + id_chunk];
  c->size = 0;
  c->next = (void*)0;
  b->front = c;
  b->back = b->front;
}
void bag_append_null_remains_special_chunks(bag* b, bag* other, int id_bag, int id_cell, int iteration_parity) {
  if (other->front) {
    b->back->next = other->front;
    b->back       = other->back;
    bag_init_special_chunks(other, id_bag, id_cell, iteration_parity);
  }
}
#endif

chunk* chunk_alloc(int thread_id) {
  if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
#ifdef PIC_VERT_TESTING
    nb_malloc[thread_id]++;
#endif
    return (chunk*) malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
  if (c >= beginning_of_special_chunks && c <= end_of_special_chunks) {
  } else
#endif
  if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
#ifdef PIC_VERT_TESTING
    nb_free[thread_id]++;
#endif
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the front of a chunk bag.
 * We have to set the field "size" to 0, because:
 *   1. When a chunk goes back to a freelist, this field is not reset.
 *   2. When a chunk is newly allocated, this field is not set neither.
 * It is simpler to set to 0 in this lone function, rather than
 * both in the allocation and in the free functions.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 */
void add_front_chunk(bag* b, int thread_id) {
  chunk* c = chunk_alloc(thread_id);
  c->size = 0;
  c->next = b->front;
  #pragma omp atomic write
  b->front = c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |  +————+
 *           |  |
 *           v  v
 *        |———————|
 *        |       |
 *   next |   X———+——>NIL
 *        |       |
 *        |———————|
 *        |       |
 *   size |   0   |
 *        |       |
 *        |———————|
 *      0 |   ?   |
 *      1 |   ?   |
 *      2 |   ?   |
 *      3 |   ?   |
 *      4 |   ?   |
 *      5 |   ?   |
 *      6 |   ?   |
 *      7 |   ?   |
 *      8 |   ?   |
 *      9 |   ?   |
 *     10 |   ?   |
 *     11 |   ?   |
 *     12 |   ?   |
 *     13 |   ?   |
 *     14 |   ?   |
 *     15 |   ?   |
 *     16 |   ?   |
 *     17 |   ?   |
 *     18 |   ?   |
 *     19 |   ?   |
 *        |———————|
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int thread_id) {
  // Initialize b->front to null before because add_front_chunk
  // uses b->front to set the front chunk's next field.
  b->front = (void*)0;
  add_front_chunk(b, thread_id);
  b->back = b->front;
}

/*
 * Nullify a bag (already allocated).
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           v       v
 *              null
 *
 * @param[in, out] b the bag to nullify.
 */
void bag_nullify(bag* b) {
  b->front = (void*)0;
  b->back  = (void*)0;
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c) {
    size += c->size;
    c = c->next;
  }
  return size;
}

/* 
 * Merge other into b; other is re-initialized.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int thread_id) {
  b->back->next = other->front;
  b->back       = other->back;
  bag_init(other, thread_id);
}

/* 
 * Merge other into b; other is nullified.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append_nullify(bag* b, bag* other) {
  if (other->front) {
    b->back->next = other->front;
    b->back       = other->back;
    bag_nullify(other);
  }
}

/* 
 * Merge other into b; other is re-initialized if not null,
 * and stays null if it was already null.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append_null_remains(bag* b, bag* other, int thread_id) {
  if (other->front)
    bag_append(b, other, thread_id);
}

/*
 * Guarantee that the entire value of *p is read atomically.
 * No part of *p can change during the read operation.
 */
chunk* atomic_read(chunk** p) {
  chunk* value;
  #pragma omp atomic read
  value = *p;
  return value;
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is thread-safe (uses atomics).
 *
 * @param[in, out] b
 * @param[in]      p
 * @return a pointer to the pushed particle.
 */
particle* bag_push_concurrent(bag* b, particle p, int thread_id) {
  chunk* c;
  int index;
  while (true) { // Until success.
    c = b->front;
    
    #pragma omp atomic capture
    index = c->size++;
    
    if (index < CHUNK_SIZE) {
      // The chunk is not full, we can write the particle.
      c->array[index] = p;
      if (index == CHUNK_SIZE - 1) {
        // The chunk is now full, we have to extend the bag.
        // Inside add_front_chunk, the update of the b-> front
        // pointer is made atomic so that other threads see the update.
        add_front_chunk(b, thread_id);
      }
      return &c->array[index];
    } else {
      // The chunk is full, another thread has just pushed a particle
      // at the end, and is now extending the bag.
      // First we have to cancel our additional "c->size++".
      c->size = CHUNK_SIZE;
      while (atomic_read(&b->front) == c) {
        // Then we wait until the other thread extends the bag.
        // The atomic_read forces the thread to read the value in the
        // main memory, and not in its temporary view.
      }
    }
  }
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is not thread-safe. Multiple threads should not
 * access the same bag.
 *
 * @param[in, out] b
 * @param[in]      p
 * @return a pointer to the pushed particle.
 */
particle* bag_push_serial(bag* b, particle p, int thread_id) {
  chunk* c = b->front;
  int index = c->size++;
  c->array[index] = p;
  if (index == CHUNK_SIZE - 1) {
    // chunk is now full, we have to extend the bag
    add_front_chunk(b, thread_id);
  }
  return &c->array[index];
}

/*
 * Initializes particlesNext with empty chunk bags where needed (null otherwise).
 * With PIC_VERT_USE_SPECIAL_CHUNKS, makes the supposition that the first iteration id
 * is 0 (at the end of iteration i, we have to take special chunks in the cell i%2,
 * thus at initialization we take the special chunks in cell 1).
 * Threads will iterate particles tile by tile. For each tile, the thread that iterates
 * over it needs to have private bags for the tile + borders (to avoid numerous atomic
 * operations, as particles will frequently move outside the tile).
 * Also initializes particlesNext with empty chunks where needed (and null bags otherwise).
 *
 * But we do not need to have as many private bags per cell as there are threads. We
 * can have only 4 private bags (in 2d) per cell, as long as we ensure that in each
 * direction, 2 * border_size <= tile_size. Then we can look at, in each direction,
 * the parity of the index of the tile to know in which private bag to work.
 *
 * To know how many private bags we will really use (we won't use 4 per cell each time),
 * there are two equivalent ways of doing the computation. In the following, we
 * denote by c = tile_size and b = border_size.
 *   1. Each tile will need (c + 2 * b)**2 private bags. Thus, in total,
 *      we need a number of private bags equal to :
 *      (c + 2 * b)**2 / (c**2) * nb_cells.
 *   2. Each cell will have a number of private bags depending of its position:
 *      corners         -> 4 *                  b**2 cells, each 4 private bags
 *      (rest of) edges -> 4 * (c - 2 * b)    * b    cells, each 2 private bags
 *      interior        -> 1 * (c - 2 * b)**2        cells, each 1 private bag
 *
 * WARNING : We make the assumption that:
 *   1. in the case of a non-colored algorithm, the id of the shared bag is 4
 * (= nb_bags_per_cell - 1), and that for each tile  beginning at
 * (ix_min, iy_min), the id of the corresponding private bag is computed by:
 * ((ix_min % (2 * tile_size)) != 0) + 2 * ((iy_min % (2 * tile_size)) != 0)
 *   2. in the case of a colored algorithm, the id of the shared bag is 1,
 * and the id of the private bag is 0.
 * In both cases, if this is not the case, you have to modify it.
 *
 * Remark : we could solve this with a macro, but this would mean that we would have to pass
 * yet another parameter in command line to know whether we are in color or non-color algorithm,
 * whereas the core of the code would not be easy to read if written with preprocessor instructions.
 *
 * @param[in] nb_bags_per_cell  the number of bags per cells (2 when using coloring scheme, 5 otherwise).
 * @param[in] num_particles     the total number of particles in this MPI process.
 * @param[in] mesh              the physical mesh we're working on.
 * @param[in] tile_size         the size of an edge of a square tile.
 * @param[in] border_size       the size of the borders allowed in a private bag (= tile_size / 2
 *                              when using coloring scheme, <= tile_size / 2 otherwise).
 * @param[in,out] particles
 * @param[in,out] particlesNext
 */
void init_all_chunks(int nb_bags_per_cell, long int num_particle, cartesian_mesh_2d mesh,
        int tile_size, int border_size, bag** particles, bag*** particlesNext) {
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int id_shared_bag = nb_bags_per_cell - 1;
    const int num_cells_2d = ncx * ncy;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    const int ncxminusone = ncx - 1;
    const int ncyminusone = ncy - 1;
    int id_private_bag, i, j;
    int ix_min, iy_min, ix_max, iy_max, ix, iy, i_cell;
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Testing that the tiling is appropriate.
    if (nb_bags_per_cell == 2) {
        if (border_size != (tile_size / 2)) {
            fprintf(stderr, "Within the coloring scheme, the borders (%d) should be equal to half the tile size (%d).\n", border_size, tile_size);
            exit(EXIT_FAILURE);
        }
    } else {
        if (border_size > (tile_size / 2)) {
            fprintf(stderr, "The borders (%d) should be at most equal to half the tile size (%d).\n", border_size, tile_size);
            exit(EXIT_FAILURE);
        }
    }
    
    // Fills free_chunks with as many chunks as possible and sets free_index accordingly.
    FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads;
    free_chunks = malloc(num_threads * sizeof(chunk**));
    free_index = allocate_aligned_int_matrix(num_threads, 1);
    for (i = 0; i < num_threads; i++) {
        // The freelists could be aligned to the cache size, but this doesn't matter:
        // their size ensure that there won't be any false sharing.
        free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
        for (j = 0; j < FREELIST_SIZE; j++)
            free_chunks[i][j] = malloc(sizeof(chunk));
        FREE_INDEX(i) = FREELIST_SIZE;
    }
#ifdef PIC_VERT_TESTING
    nb_malloc = allocate_int_array(num_threads);
    nb_free   = allocate_int_array(num_threads);
    for (i = 0; i < num_threads; i++) {
        nb_malloc[i] = 0;
        nb_free[i]   = 0;
    }
#endif
    
    // Bags in cell i are taken from free list i % num_threads to
    // equally distribute the pressure on the different free lists.
    for (i = 0; i < num_cells_2d; i++)
        bag_init(&((*particles)[i]), i % num_threads);
    
    // Nullification of the private bags (they are initialized if needed in the next loop).
    for (j = 0; j < id_shared_bag; j++)
        for (i = 0; i < num_cells_2d; i++)
            bag_nullify(&((*particlesNext)[j][i]));
#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
    special_chunks_ids = allocate_int_matrix(nb_bags_per_cell, num_cells_2d);
    number_of_special_chunks_per_parity = 0;
    // Computation of bijection between [0 ; nb_bags_per_cell[ x [0 ; num_cells_2d[ and [0 ; number_of_special_chunks_per_parity[
    // *** private part
    for (ix_min = 0; ix_min <= ncxminusone; ix_min += tile_size) {
    for (iy_min = 0; iy_min <= ncyminusone; iy_min += tile_size) {
        ix_max = min(ix_min + tile_size - 1, ncxminusone);
        iy_max = min(iy_min + tile_size - 1, ncyminusone);
        id_private_bag = ((ix_min % (2 * tile_size)) != 0) + 2 * ((iy_min % (2 * tile_size)) != 0);
        // Nested loops on the cells of the tile +/- borders.
        for (ix = ix_min - border_size; ix <= ix_max + border_size; ix++) {
        for (iy = iy_min - border_size; iy <= iy_max + border_size; iy++) {
            i_cell = COMPUTE_I_CELL_2D(icell_param, ix & ncxminusone, iy & ncyminusone);
            special_chunks_ids[id_private_bag][i_cell] = number_of_special_chunks_per_parity++;
        }}
    }}
    // *** shared part
    for (i = 0; i < num_cells_2d; i++)
        special_chunks_ids[id_shared_bag][i] = number_of_special_chunks_per_parity++;
    // Creation of the special chunks.
    special_chunks = malloc(2 * number_of_special_chunks_per_parity * sizeof(chunk));
    beginning_of_special_chunks = &(special_chunks[0]);
    end_of_special_chunks = &(special_chunks[2 * number_of_special_chunks_per_parity - 1]);
#endif
    // Initialization of the useful private bags.
    for (ix_min = 0; ix_min <= ncxminusone; ix_min += tile_size) {
    for (iy_min = 0; iy_min <= ncyminusone; iy_min += tile_size) {
        ix_max = min(ix_min + tile_size - 1, ncxminusone);
        iy_max = min(iy_min + tile_size - 1, ncyminusone);
        id_private_bag = ((ix_min % (2 * tile_size)) != 0) + 2 * ((iy_min % (2 * tile_size)) != 0);
        // Nested loops on the cells of the tile +/- borders.
        for (ix = ix_min - border_size; ix <= ix_max + border_size; ix++) {
        for (iy = iy_min - border_size; iy <= iy_max + border_size; iy++) {
            i_cell = COMPUTE_I_CELL_2D(icell_param, ix & ncxminusone, iy & ncyminusone);
#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
            bag_init_special_chunks(&((*particlesNext)[id_private_bag][i_cell]), id_private_bag, i_cell, ODD_ITERATION);
#else
            bag_init(&((*particlesNext)[id_private_bag][i_cell]), i_cell % num_threads);
#endif
        }}
    }}
    // Initialization of the shared bags.
    for (i = 0; i < num_cells_2d; i++)
#ifdef PIC_VERT_USE_SPECIAL_CHUNKS
        bag_init_special_chunks(&((*particlesNext)[id_shared_bag][i]), id_shared_bag, i, ODD_ITERATION);
#else
        bag_init(&((*particlesNext)[id_shared_bag][i]), i % num_threads);
#endif
}

/*
 * Write the num_particle particles in a file, in binary.
 *
 * @param[in] num_particle the number of particles.
 * @param[in] mesh, the mesh on which we're working.
 * @param[in] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void write_binary_particle_array_2d3v(int mpi_rank, long int num_particle, cartesian_mesh_2d mesh, bag* particles) {
    int i, j;
    char filename[99];
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int num_cells_2d = ncx * ncy;
    bag* chunkbag;
    chunk* my_chunk;
    
    // Export the particles.
    sprintf(filename, "initial_particles_%ldkk_rank%d.dat", num_particle / 1000000, mpi_rank);
    FILE* file_write_particles = fopen(filename, "w"); // "wb" ?
    if (!file_write_particles) { // Error in file opening
        printf("Error creating file %s.\n", filename);
        exit(EXIT_FAILURE);
    }
    fwrite(&ncx, sizeof(int), 1, file_write_particles);
    fwrite(&ncy, sizeof(int), 1, file_write_particles);
    fwrite(&num_particle, sizeof(long int), 1, file_write_particles);
    for (j = 0; j < num_cells_2d; j++) {
        chunkbag = &(particles[j]);
        for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
            for (i = 0; i < my_chunk->size; i++) {
                fwrite(&j, sizeof(int), 1, file_write_particles);
                fwrite(&my_chunk->array[i].dx, sizeof(float), 1, file_write_particles);
                fwrite(&my_chunk->array[i].dy, sizeof(float), 1, file_write_particles);
                fwrite(&my_chunk->array[i].vx, sizeof(double), 1, file_write_particles);
                fwrite(&my_chunk->array[i].vy, sizeof(double), 1, file_write_particles);
                fwrite(&my_chunk->array[i].vz, sizeof(double), 1, file_write_particles);
            }
        }
    }
    fclose(file_write_particles);
}

/*
 * Initializes arrays of num_particle particles from a file, in binary.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the number of particles.
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_binary_particle_array_2d3v(int mpi_rank, int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy, vz;
    size_t ret_code;
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    sprintf(filename, "initial_particles_%ldkk_rank%d.dat", num_particle / 1000000, mpi_rank);
    FILE* file_read_particles = fopen(filename, "r"); // "rb" ?
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    ret_code = fread(&throw_that_number_x, sizeof(int), 1, file_read_particles);
    ret_code += fread(&throw_that_number_y, sizeof(int), 1, file_read_particles);
    ret_code += fread(&throw_that_number_also, sizeof(long int), 1, file_read_particles);
    if (ret_code < 3) {
        fprintf(stderr, "%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        ret_code = fread(&i_cell, sizeof(int), 1, file_read_particles);
        ret_code += fread(&dx, sizeof(float), 1, file_read_particles);
        ret_code += fread(&dy, sizeof(float), 1, file_read_particles);
        ret_code += fread(&vx, sizeof(double), 1, file_read_particles);
        ret_code += fread(&vy, sizeof(double), 1, file_read_particles);
        ret_code += fread(&vz, sizeof(double), 1, file_read_particles);
        if (ret_code < 6) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        // Bags in cell i_cell are taken from free list i_cell % num_threads to
        // equally distribute the pressure on the different free lists.
        bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy,
                                                             .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
    }
    fclose(file_read_particles);
}

/*
 * Write the num_particle particles in a file, in ASCII.
 *
 * @param[in] num_particle the number of particles.
 * @param[in] mesh, the mesh on which we're working.
 * @param[in] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void write_ascii_particle_array_2d3v(long int num_particle, cartesian_mesh_2d mesh, bag* particles) {
    size_t i, j;
    char filename[99];
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int num_cells_2d = ncx * ncy;
    bag* chunkbag;
    chunk* my_chunk;
    
    // Export the particles.
    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_write_particles = fopen(filename, "w");
    if (!file_write_particles) { // Error in file opening
        printf("Error creating file %s.\n", filename);
        exit(EXIT_FAILURE);
    }
    fprintf(file_write_particles, "%d %d\n", ncx, ncy);
    fprintf(file_write_particles, "%ld\n", num_particle);
    for (j = 0; j < num_cells_2d; j++) {
        chunkbag = &(particles[j]);
        for (my_chunk = chunkbag->front; my_chunk; my_chunk = my_chunk->next) {
            for (i = 0; i < my_chunk->size; i++) {
                fprintf(file_write_particles, "%ld %.*g %.*g %.*g %.*g %.*g\n", j,
                  FLT_DECIMAL_DIG, my_chunk->array[i].dx, FLT_DECIMAL_DIG, my_chunk->array[i].dy,
                  DBL_DECIMAL_DIG, my_chunk->array[i].vx, DBL_DECIMAL_DIG, my_chunk->array[i].vy,
                  DBL_DECIMAL_DIG, my_chunk->array[i].vz);
            }
        }
    }
    fclose(file_write_particles);
}

/*
 * Initializes arrays of num_particle particles from a file, in ASCII.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the number of particles.
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_ascii_particle_array_2d3v(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy, vz;
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf %lf", &i_cell,
                &dx, &dy, &vx, &vy, &vz) < 6) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        // Bags in cell i_cell are taken from free list i_cell % num_threads to
        // equally distribute the pressure on the different free lists.
        bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy,
                                                             .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
    }
    fclose(file_read_particles);
}

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the number of particles.
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_2d3v(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t j;
    int i_cell;
    double x, y, vx, vy, vz;
    double control_point, evaluated_function;
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    max_distribution_function max_distrib_function = distribution_maxs_2d3v[sim_distrib];
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    *weight = (float)x_range * (float)y_range / ((float)mpi_world_size * (float)num_particle);
    
    if (sim_distrib < INITIALIZE_VX_WITH_X_AND_Y) {
        const double vx_min = speed_params[1];
        const double vx_max = speed_params[2];
        const double vx_range = vx_max - vx_min;
        speeds_generator_2d speeds_generator = speed_generators_2d3v_vyvz[sim_distrib];
        distribution_function_3d distrib_function = distribution_funs_2d3v_xyvx[sim_distrib];
        
        for (j = 0; j < num_particle; j++) {
            do {
                x  =  x_range * pic_vert_next_random_double() + mesh.x_min;
                y  =  y_range * pic_vert_next_random_double() + mesh.y_min;
                vx = vx_range * pic_vert_next_random_double() + vx_min;
                control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
                evaluated_function = (*distrib_function)(spatial_params, x, y, vx);
            } while (control_point > evaluated_function);
            x = (x - mesh.x_min) / mesh.delta_x;
            y = (y - mesh.y_min) / mesh.delta_y;
            (*speeds_generator)(speed_params, &vy, &vz);
            i_cell = COMPUTE_I_CELL_2D(icell_param, (int)x, (int)y);
            // Bags in cell i_cell are taken from free list i_cell % num_threads to
            // equally distribute the pressure on the different free lists.
            bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .dy = (float)(y - (int)y),
                                                                 .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
        }
    } else {
        speeds_generator_3d speeds_generator = speed_generators_2d3v_vxvyvz[sim_distrib];
        distribution_function_2d distrib_function = distribution_funs_2d3v_xy[sim_distrib];
        
        for (j = 0; j < num_particle; j++) {
            do {
                x = x_range * pic_vert_next_random_double() + mesh.x_min;
                y = y_range * pic_vert_next_random_double() + mesh.y_min;
                control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
                evaluated_function = (*distrib_function)(spatial_params, x, y);
            } while (control_point > evaluated_function);
            x = (x - mesh.x_min) / mesh.delta_x;
            y = (y - mesh.y_min) / mesh.delta_y;
            (*speeds_generator)(speed_params, &vx, &vy, &vz);
            i_cell = COMPUTE_I_CELL_2D(icell_param, (int)x, (int)y);
            // Bags in cell i_cell are taken from free list i_cell % num_threads to
            // equally distribute the pressure on the different free lists.
            bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .dy = (float)(y - (int)y),
                                                                 .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
        }
    }
    
#ifdef PIC_VERT_TEST_INITIAL_DISTRIBUTION
    size_t i;
    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_2d.dat", "w");
    switch(sim_distrib) {
        default:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++)
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)])));
    }
    fclose(file_diag_particles);
#endif
}

