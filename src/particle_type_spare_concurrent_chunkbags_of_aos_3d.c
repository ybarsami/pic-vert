#include <omp.h>                                                // function  omp_get_num_threads
#include <stdbool.h>                                            // constant  true
#include <stdio.h>                                              // functions fprintf, fopen, fclose, fscanf, sprintf
                                                                // constant  stderr (standard error output stream)
#include <stdlib.h>                                             // functions exit (error handling), malloc, free ((de)allocate memory)
                                                                // constant  EXIT_FAILURE (error handling)
                                                                // type      size_t
#include "initial_distributions.h"                              // types     speeds_generator_3d, distribution_function_3d, max_distribution_function
                                                                // variables speed_generators_3d, distribution_funs_3d, distribution_maxs_3d
#include "matrix_functions.h"                                   // function  allocate_int_matrix
#include "meshes.h"                                             // type      cartesian_mesh_3d
#include "particle_type_spare_concurrent_chunkbags_of_aos_3d.h" // types     particle, chunk, bag
                                                                // variables free_index, FREELIST_SIZE
#include "random.h"                                             // function  pic_vert_next_random_double
#include "space_filling_curves.h"                               // macros    COMPUTE_I_CELL_3D, I_CELL_PARAM1_3D, I_CELL_PARAM2_3D

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |       +—————————————————————————————————————+
 *           |                                             |
 *           |     +——————+     +— ...     ——+     +————+  |
 *           |     |      |     |            |     |    |  |
 *           v     |      v     |            v     |    v  v
 *       |———————| |  |———————| |        |———————| |  |———————|
 *       |       | |  |       | |        |       | |  |       |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *       |       |    |       |          |       |    |       |
 *  size |  16   |    |  11   |          |  13   |    |  17   |
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *     0 |       |    |       |          |       |    |       |
 *     1 |       |    |       |          |       |    |       |
 *     2 |       |    |       |          |       |    |       |
 *     3 |       |    |       |          |       |    |       |
 *     4 |       |    |       |          |       |    |       |
 *     5 |       |    |       |          |       |    |       |
 *     6 |       |    |       |          |       |    |       |
 *     7 |       |    |       |          |       |    |       |
 *     8 |       |    |       |          |       |    |       |
 *     9 |       |    |       |          |       |    |       |
 *    10 |       |    |       |          |       |    |       |
 *    11 |       |    |   ?   |          |       |    |       |
 *    12 |       |    |   ?   |          |       |    |       |
 *    13 |       |    |   ?   |          |   ?   |    |       |
 *    14 |       |    |   ?   |          |   ?   |    |       |
 *    15 |       |    |   ?   |          |   ?   |    |       |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *
 */

chunk*** free_chunks;    // Freelists

chunk* chunk_alloc(int id_bag, int id_cell, int thread_id) {
  int id_chunk = special_chunks_ids[id_bag][id_cell];
  chunk* c = &special_chunks[2 * number_of_special_chunks_per_parity + id_chunk];
  if (c->size == 0) {
    return c;
  } else if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
#ifdef PIC_VERT_TESTING
    nb_malloc[thread_id]++;
#endif
    return malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
  if (c >= beginning_of_special_chunks && c <= end_of_special_chunks) {
  } else if (c > end_of_special_chunks && c <= end_of_spare_chunks) {
    c->size = 0;
  } else if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
#ifdef PIC_VERT_TESTING
    nb_free[thread_id]++;
#endif
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the front
 * of a chunk bag.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 */
void add_front_chunk(bag* b, int id_bag, int id_cell, int thread_id) {
  chunk* c = chunk_alloc(id_bag, id_cell, thread_id);
  c->size = 0;
  c->next = b->front;
  #pragma omp atomic write
  b->front = c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |  +————+
 *           |  |
 *           v  v
 *        |———————|
 *        |       |
 *   next |   X———+——>NIL
 *        |       |
 *        |———————|
 *        |       |
 *   size |   0   |
 *        |       |
 *        |———————|
 *      0 |   ?   |
 *      1 |   ?   |
 *      2 |   ?   |
 *      3 |   ?   |
 *      4 |   ?   |
 *      5 |   ?   |
 *      6 |   ?   |
 *      7 |   ?   |
 *      8 |   ?   |
 *      9 |   ?   |
 *     10 |   ?   |
 *     11 |   ?   |
 *     12 |   ?   |
 *     13 |   ?   |
 *     14 |   ?   |
 *     15 |   ?   |
 *     16 |   ?   |
 *     17 |   ?   |
 *     18 |   ?   |
 *     19 |   ?   |
 *        |———————|
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int id_bag, int id_cell, int iteration_parity) {
  int id_chunk = special_chunks_ids[id_bag][id_cell];
  chunk* c = &special_chunks[iteration_parity * number_of_special_chunks_per_parity + id_chunk];
  c->size = 0;
  c->next = (void*)0;
  b->front = c;
  b->back = b->front;
}

/*
 * Nullify a bag (already allocated).
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           v       v
 *              null
 *
 * @param[in, out] b the bag to nullify.
 */
void bag_nullify(bag* b) {
  b->front = (void*)0;
  b->back  = (void*)0;
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c) {
    size += c->size;
    c = c->next;
  }
  return size;
}

/* 
 * Merge other into b; other is re-initialized from special chunks if not null,
 * and stays null if it was already null.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int id_bag, int id_cell, int iteration_parity) {
  if (other->front) {
    // Same code as bag_append_special_chunks inside the if.
    b->back->next = other->front;
    b->back       = other->back;
    bag_init(other, id_bag, id_cell, iteration_parity);
  }
}

/*
 * Guarantee that the entire value of *p is read atomically.
 * No part of *p can change during the read operation.
 */
chunk* atomic_read(chunk** p) {
  chunk* value;
  #pragma omp atomic read
  value = *p;
  return value;
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is thread-safe (uses atomics).
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push_concurrent(bag* b, particle p, int id_bag, int id_cell, int thread_id) {
  chunk* c;
  int index;
  while (true) { // Until success.
    c = b->front;

    #pragma omp atomic capture
    index = c->size++;

    if (index < CHUNK_SIZE) {
      // The chunk is not full, we can write the particle.
      c->array[index] = p;
      if (index == CHUNK_SIZE - 1) {
        // The chunk is now full, we have to extend the bag.
        // Inside add_front_chunk, the update of the b-> front
        // pointer is made atomic so that other threads see the update.
        add_front_chunk(b, id_bag, id_cell, thread_id);
      }
      return;
    } else {
      // The chunk is full, another thread has just pushed a particle
      // at the end, and is now extending the bag.
      // First we have to cancel our additional "c->size++".
      c->size = CHUNK_SIZE;
      while (atomic_read(&b->front) == c) {
        // Then we wait until the other thread extends the bag.
        // The atomic_read forces the thread to read the value in the
        // main memory, and not in its temporary view.
      }
    }
  }
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is not thread-safe. Multiple threads should not
 * access the same bag.
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push_serial(bag* b, particle p, int id_bag, int id_cell, int thread_id) {
  chunk* c = b->front;
  int index = c->size++;
  c->array[index] = p;
  if (index == CHUNK_SIZE - 1) {
    // chunk is now full, we have to extend the bag
    add_front_chunk(b, id_bag, id_cell, thread_id);
  }
}

// Special + spare chunks initialization.
void init_special_and_spare_chunks(long int num_particle, int nb_bags_per_cell, int ncx, int ncy, int ncz, int tile_size, int border_size) {
  const int id_shared_bag = nb_bags_per_cell - 1;
  const int num_cells_3d = ncx * ncy * ncz;
  const int icell_param1 = I_CELL_PARAM1_3D(ncx, ncy, ncz);
  const int icell_param2 = I_CELL_PARAM2_3D(ncx, ncy, ncz);
  const int ncxminusone = ncx - 1;
  const int ncyminusone = ncy - 1;
  const int nczminusone = ncz - 1;
  int id_private_bag, i, j;
  int ix_min, iy_min, iz_min, ix_max, iy_max, iz_max, ix, iy, iz, i_cell;
  
  special_chunks_ids = allocate_int_matrix(nb_bags_per_cell, num_cells_3d);
  number_of_special_chunks_per_parity = 0;
  // Computation of bijection between [0 ; nb_bags_per_cell[ x [0 ; num_cells_3d[ and [0 ; total_num_chunks[
  // *** private part
  for (ix_min = 0; ix_min < ncx; ix_min += tile_size) {
  for (iy_min = 0; iy_min < ncy; iy_min += tile_size) {
  for (iz_min = 0; iz_min < ncz; iz_min += tile_size) {
    ix_max = min(ix_min + tile_size - 1, ncxminusone);
    iy_max = min(iy_min + tile_size - 1, ncyminusone);
    iz_max = min(iz_min + tile_size - 1, nczminusone);
    id_private_bag = ((ix_min % (2 * tile_size)) != 0) + 2 * ((iy_min % (2 * tile_size)) != 0) + 4 * ((iz_min % (2 * tile_size)) != 0);
    // Nested loops on the cells of the tile +/- borders.
    for (ix = ix_min - border_size; ix <= ix_max + border_size; ix++) {
    for (iy = iy_min - border_size; iy <= iy_max + border_size; iy++) {
    for (iz = iz_min - border_size; iz <= iz_max + border_size; iz++) {
      i_cell = COMPUTE_I_CELL_3D(icell_param1, icell_param2, ix & ncxminusone, iy & ncyminusone, iz & nczminusone);
      special_chunks_ids[id_private_bag][i_cell] = number_of_special_chunks_per_parity++;
    }}}
  }}}
  // *** shared part
  for (i = 0; i < num_cells_3d; i++)
    special_chunks_ids[id_shared_bag][i] = number_of_special_chunks_per_parity++;
  // Creation of the special chunks.
  special_chunks = malloc(3 * number_of_special_chunks_per_parity * sizeof(chunk));
  beginning_of_special_chunks = &(special_chunks[0]);
  end_of_special_chunks = &(special_chunks[2 * number_of_special_chunks_per_parity - 1]);
  end_of_spare_chunks = &(special_chunks[3 * number_of_special_chunks_per_parity - 1]);
  // Say that the spare chunks can be used.
  for (i = 2 * number_of_special_chunks_per_parity; i < 3 * number_of_special_chunks_per_parity; i++)
    special_chunks[i].size = 0;
  
  int num_threads;
  #pragma omp parallel
  num_threads = omp_get_num_threads();
  
  FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads;
  free_chunks = malloc(num_threads * sizeof(chunk**));
  free_index = allocate_aligned_int_matrix(num_threads, 1);
  for (i = 0; i < num_threads; i++) {
    // The freelists could be aligned to the cache size, but this doesn't matter:
    // their size ensure that there won't be any false sharing.
    free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
    for (j = 0; j < FREELIST_SIZE; j++)
      free_chunks[i][j] = malloc(sizeof(chunk));
    FREE_INDEX(i) = FREELIST_SIZE;
  }
  
#ifdef PIC_VERT_TESTING
  nb_malloc = allocate_int_array(num_threads);
  nb_free   = allocate_int_array(num_threads);
  for (i = 0; i < num_threads; i++) {
    nb_malloc[i] = 0;
    nb_free[i]   = 0;
  }
#endif
}

void add_front_chunk_initial(bag* b, int thread_id) {
  chunk* c;
  if (FREE_INDEX(thread_id) > 0) {
    c = free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
    c = malloc(sizeof(chunk));
  }
  c->size = 0;
  c->next = b->front;
  b->front = c;
}

void bag_init_initial(bag* b, int thread_id) {
  b->front = (void*)0;
  add_front_chunk_initial(b, thread_id);
  b->back = b->front;
}

void bag_push_initial(bag* b, particle p, int thread_id) {
  chunk* c = b->front;
  int index = c->size++;
  c->array[index] = p;
  if (index == CHUNK_SIZE - 1) {
    // chunk is now full, we have to extend the bag
    add_front_chunk_initial(b, thread_id);
  }
}

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy * mesh.ncz] a newly allocated array of chunkbags of particles read from file.
 */
void read_particle_array_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[30];
    int throw_that_number_x, throw_that_number_y, throw_that_number_z;
    unsigned int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int ncz = mesh.num_cell_z;
    const int num_cells_3d = ncx * ncy * ncz;
    int i_cell;
    float dx, dy, dz;
    double vx, vy, vz;
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Bags in cell i are taken from free list i % num_threads to
    // equally distribute the pressure on the different free lists.
    for (i = 0; i < num_cells_3d; i++)
        bag_init_initial(&((*particles)[i]), i % num_threads);
    sprintf(filename, "initial_particles_%dkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (fscanf(file_read_particles, "%d %d %d %u", &throw_that_number_x, &throw_that_number_y, &throw_that_number_z,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy ncz num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_z != ncz) {
        fprintf(stderr, "I expected ncz = %d but found %d in the input file.\n", ncz, throw_that_number_z);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %d but found %d in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) * (float)(mesh.z_max - mesh.z_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %f %lf %lf %lf", &i_cell,
                &dx, &dy, &dz, &vx, &vy, &vz) < 7) {
            fprintf(stderr, "I expected %d particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        // Bags in cell i_cell are taken from free list i_cell % num_threads to
        // equally distribute the pressure on the different free lists.
        bag_push_initial(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy, .dz = dz,
                                                              .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
    }
    fclose(file_read_particles);
}

/*
 * Return an array of num_particle random particles following given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy * mesh.ncz] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t i, j;
    int i_cell;
    double x, y, z, vx, vy, vz;
    double control_point, evaluated_function;
    speeds_generator_3d speeds_generator = speed_generators_3d[sim_distrib];
    distribution_function_3d distrib_function = distribution_funs_3d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_3d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const double z_range = mesh.z_max - mesh.z_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int ncz = mesh.num_cell_z;
    const int icell_param1 = I_CELL_PARAM1_3D(ncx, ncy, ncz);
    const int icell_param2 = I_CELL_PARAM2_3D(ncx, ncy, ncz);
    const int num_cells_3d = ncx * ncy * ncz;
    int num_threads;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    
    // Bags in cell i are taken from free list i % num_threads to
    // equally distribute the pressure on the different free lists.
    for (i = 0; i < num_cells_3d; i++)
        bag_init_initial(&((*particles)[i]), i % num_threads);
    
    *weight = (float)x_range * (float)y_range * (float)z_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            z = z_range * pic_vert_next_random_double() + mesh.z_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y, z);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        z = (z - mesh.z_min) / mesh.delta_z;
        (*speeds_generator)(speed_params, &vx, &vy, &vz);
        i_cell = COMPUTE_I_CELL_3D(icell_param1, icell_param2, (int)x, (int)y, (int)z);
        // Bags in cell i_cell are taken from free list i_cell % num_threads to
        // equally distribute the pressure on the different free lists.
        bag_push_initial(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .dy = (float)(y - (int)y), .dz = (float)(z - (int)z),
                                                              .vx = vx, .vy = vy, .vz = vz }, i_cell % num_threads);
    }
    
#ifdef PIC_VERT_TEST_INITIAL_DISTRIBUTION
    // Test the initial distribution.
    size_t k;
    FILE* file_diag_particles = fopen("test_particles_3d.dat", "w");
    unsigned int nb_particle_in_cell;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ3D:
            for (i = 0; i < ncx; i++) {
                nb_particle_in_cell = 0;
                for (j = 0; j < ncy; j++)
                    for (k = 0; k < ncz; k++)
                        nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)]));
                fprintf(file_diag_particles, "%ld %d\n", i, nb_particle_in_cell);
            }
            break;
        case LANDAU_2D_PROJ3D:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++) {
                    nb_particle_in_cell = 0;
                    for (k = 0; k < ncz; k++)
                        nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)]));
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, nb_particle_in_cell);
                }
            break;
        default:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++)
                    for (k = 0; k < ncz; k++)
                        fprintf(file_diag_particles, "%ld %ld %ld %d\n", i, j, k, bag_size(&((*particles)[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)])));
    }
    fclose(file_diag_particles);
#endif
}

