#include <omp.h>                               // function  omp_get_num_threads
#include <stdio.h>                             // functions fprintf, fopen, fclose
#include <stdlib.h>                            // functions exit (error handling), malloc, free ((de)allocate memory)
                                               // constant  EXIT_FAILURE (error handling)
                                               // type      size_t
#include "initial_distributions.h"             // types     speeds_generator_1d, distribution_function_1d, max_distribution_function
                                               // variables speed_generators_1d, distribution_funs_1d, distribution_maxs_1d
#include "matrix_functions.h"                  // function  allocate_aligned_int_matrix
#include "meshes.h"                            // type      cartesian_mesh_1d
#include "particle_type_chunkbags_of_aos_1d.h" // types     particle, chunk, bag
                                               // variables free_chunks, free_index, FREELIST_SIZE
#include "random.h"                            // function  pic_vert_next_random_double

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 * ? in the size means that the size is not yet computed for the last chunk.
 *
 *                    Chunk bag
 *       { front   back    b_head  b_end };
 *       |———————|———————|———————|———————|
 *       |       |       |       |       |
 *       |   X   |   X   |   X   |   X   |
 *       |   |   |   |   |   |   |   |   |
 *       |———+———|———+———|———+———|———+———|
 *           |       |       |       |
 *           |       |       |       +——————————————————————————————————————+
 *           |       |       |                                              |
 *           |       |       +—————————————————————————————————————————+    |
 *           |       |                                                 |    |
 *           |       +—————————————————————————————————————+           |    |
 *           |                                             |           |    |
 *           |     +——————+     +— ...     ——+     +————+  |           |    |
 *           |     |      |     |            |     |    |  |           |    |
 *           v     |      v     |            v     |    v  v           |    |
 *       |———————| |  |———————| |        |———————| |  |———————|        |    |
 *       |       | |  |       | |        |       | |  |       |        |    |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL  |    |
 *       |       |    |       |          |       |    |       |        |    |
 *       |———————|    |———————|    ...   |———————|    |———————|        |    |
 *       |       |    |       |          |       |    |       |        |    |
 *  size |  16   |    |  11   |          |  13   |    |   ?   |        |    |
 *       |       |    |       |          |       |    |       |        |    |
 *       |———————|    |———————|    ...   |———————|    |———————|        |    |
 *     0 |       |    |       |          |       |    |       |        |    |
 *     1 |       |    |       |          |       |    |       |        |    |
 *     2 |       |    |       |          |       |    |       |        |    |
 *     3 |       |    |       |          |       |    |       |        |    |
 *     4 |       |    |       |          |       |    |       |        |    |
 *     5 |       |    |       |          |       |    |       |        |    |
 *     6 |       |    |       |          |       |    |       |        |    |
 *     7 |       |    |       |          |       |    |       |        |    |
 *     8 |       |    |       |          |       |    |       |        |    |
 *     9 |       |    |       |          |       |    |       |        |    |
 *    10 |       |    |       |          |       |    |       |        |    |
 *    11 |       |    |   ?   |          |       |    |       |        |    |
 *    12 |       |    |   ?   |          |       |    |       |        |    |
 *    13 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    14 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    15 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |        |    |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |<———————+    |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |             |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |             |
 *       |———————|    |———————|    ...   |———————|    |———————|<————————————+
 *
 */

chunk*** free_chunks;

/*
 * Fills free_chunks with as many chunks as possible and sets free_index accordingly.
 */
void init_freelists(long int num_particle) {
  int num_threads;
  #pragma omp parallel
  num_threads = omp_get_num_threads();
  FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads;
  free_chunks = malloc(num_threads * sizeof(chunk**));
  free_index = allocate_aligned_int_matrix(num_threads, 1);
  for (size_t i = 0; i < num_threads; i++) {
    // The freelists could be aligned to the cache size, but this doesn't matter:
    // their size ensure that there won't be any false sharing.
    free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
    for (size_t j = 0; j < FREELIST_SIZE; j++)
      free_chunks[i][j] = malloc(sizeof(chunk));
    FREE_INDEX(i) = FREELIST_SIZE;
  }
}

chunk* chunk_alloc(int thread_id) {
  if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
    return (chunk*) malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
  if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the back
 * of a chunk bag.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 * @return         the newly allocated chunk.
 */
chunk* new_back_chunk(bag* b, int thread_id) {
  chunk* c = chunk_alloc(thread_id);
  // c->size needs not be initialized
  c->next = (void*)0;
  b->back = c;
  b->back_head = &(c->array[0]);
  b->back_end  = &(c->array[CHUNK_SIZE]);
  return c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 * The size of the array is of course 0, but it will not be maintained,
 * so we don't compute it.
 *
 *                    Chunk bag
 *       { front   back    b_head  b_end };
 *       |———————|———————|———————|———————|
 *       |       |       |       |       |
 *       |   X   |   X   |   X   |   X   |
 *       |   |   |   |   |   |   |   |   |
 *       |———+———|———+———|———+———|———+———|
 *           |       |       |       |
 *           |  +————+       |       |
 *           |  |            |       |
 *           v  v            |       |
 *        |———————|          |       |
 *        |       |          |       |
 *   next |   X———+——>NIL    |       |
 *        |       |          |       |
 *        |———————|          |       |
 *        |       |          |       |
 *   size |   ?   |          |       |
 *        |       |          |       |
 *        |———————|          |       |
 *      0 |   ?   |<—————————+       |
 *      1 |   ?   |                  |
 *      2 |   ?   |                  |
 *      3 |   ?   |                  |
 *      4 |   ?   |                  |
 *      5 |   ?   |                  |
 *      6 |   ?   |                  |
 *      7 |   ?   |                  |
 *      8 |   ?   |                  |
 *      9 |   ?   |                  |
 *     10 |   ?   |                  |
 *     11 |   ?   |                  |
 *     12 |   ?   |                  |
 *     13 |   ?   |                  |
 *     14 |   ?   |                  |
 *     15 |   ?   |                  |
 *     16 |   ?   |                  |
 *     17 |   ?   |                  |
 *     18 |   ?   |                  |
 *     19 |   ?   |                  |
 *        |———————|<—————————————————+
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int thread_id) {
  chunk* c = new_back_chunk(b, thread_id);
  b->front = c;
}

/*
 * Compute the number of elements stored into the last
 * chunk of a chunk bag.
 * Uses pointer arithmetic.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b->back.
 */
int bag_back_size(bag* b) {
  return CHUNK_SIZE - (b->back_end - b->back_head);
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c != b-> back) {
    size += c->size;
    c = c->next;
  }
  return size + bag_back_size(b);
}

/*
 * Compute the number of elements stored into the last
 * chunk of a chunk bag, and store this number inside
 * b->back->size.
 *
 * @param[in] b the bag.
 */
void bag_store_size_of_back(bag* b) {
  b->back->size = bag_back_size(b);
}

/* 
 * Merge other into b; other becomes empty.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int thread_id) {
  bag_store_size_of_back(b);
  b->back->next = other->front;
  b->back       = other->back;
  b->back_end   = other->back_end;
  b->back_head  = other->back_head;
  bag_init(other, thread_id);
}

/* 
 * Add a particle into a chunk bag. Add it into the last chunk,
 * unless this chunk is full. In that case, allocate a new chunk
 * before adding it into that new chunk.
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push(bag* b, particle p, int thread_id) {
  if (b->back_head == b->back_end) {
    chunk* old_back = b->back;
    chunk* c = new_back_chunk(b, thread_id);
    old_back->size = CHUNK_SIZE;
    old_back->next = c;
  }
  *(b->back_head) = p;
  b->back_head++;
}

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_1d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_1d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t i, j;
    int i_cell;
    double x, vx;
    double control_point, evaluated_function;
    speeds_generator_1d speeds_generator = speed_generators_1d[sim_distrib];
    distribution_function_1d distrib_function = distribution_funs_1d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_1d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;

    *weight = (float)x_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        (*speeds_generator)(speed_params, &vx);
        i_cell = (int)x;
        bag_push(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .vx = vx }, 0);
    }

    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_1d.dat", "w");
    switch(sim_distrib) {
        default:
            for (i = 0; i < mesh.num_cell_x; i++)
                fprintf(file_diag_particles, "%ld %d\n", i, bag_size(&((*particles)[i])));
    }
    fclose(file_diag_particles);
}

