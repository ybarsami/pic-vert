#include <math.h>                 // function  log
#include <mpi.h>                  // constants MPI_DOUBLE, MPI_COMM_WORLD, MPI_DOUBLE_PRECISION
                                  // functions MPI_Irecv, MPI_Isend, MPI_Wait, MPI_Allgatherv
#include <stdio.h>                // functions fprintf, fopen, fclose
                                  // constant  stderr (standard error output stream)
#include <stdlib.h>               // functions malloc, free ((de)allocate memory)
                                  //           exit (error handling)
                                  // constant  EXIT_FAILURE (error handling)
                                  // type      size_t
#include <stdbool.h>              // type      bool
#include "domain_decomposition.h" // types     box_2d, mpi_parameters
                                  // constants NEIGHBOR_NORTH_WEST, NEIGHBOR_NORTH_MIDL, NEIGHBOR_NORTH_EAST,
                                  //           NEIGHBOR_MIDDL_WEST, NEIGHBOR_MIDDL_EAST, NEIGHBOR_SOUTH_WEST,
                                  //           NEIGHBOR_SOUTH_MIDL, NEIGHBOR_SOUTH_EAST, NB_NEIGHBORS_2D, OVERLAP
#include "math_functions.h"       // functions int_pow, min, max
#include "matrix_functions.h"     // functions allocate_int_matrix, deallocate_int_matrix
#include "space_filling_curves.h" // macros    COMPUTE_I_CELL_2D, I_CELL_PARAM_2D

/*****************************************************************************
 *                           Rectangular boxes                               *
 *****************************************************************************/

/*
 * Prints the limits of the box.
 *
 * @param box the box to print.
 */
void print_box_2d(box_2d box) {
    printf("[%d ; %d] x [%d ; %d]\n", box.i_min, box.i_max, box.j_min, box.j_max);
}

/*
 * Test if a box is empty.
 *
 * @param  box the box on which to check emptiness.
 * @return true iff box is empty.
 */
bool is_empty_box_2d(box_2d box) {
    return (box.i_min > box.i_max) || (box.j_min > box.j_max);
}

/*
 * Return the number of different points in the box.
 *
 * @param  box the box on which to count the points.
 * @return the number of different points in the box.
 */
int number_points_box_2d(box_2d box) {
    if (is_empty_box_2d(box))
        return 0;
    else
        return (box.i_max - box.i_min + 1) * (box.j_max - box.j_min + 1);
}

/*
 * Intersects two boxes.
 *
 * @param[in]  b1, b2 : the two boxes to intersect.
 * @param[out] ans : the intersection of the two boxes (has to be initialized).
 * @return     false iff the intersection is empty.
 */
bool intersect_boxes_2d(box_2d b1, box_2d b2, box_2d* ans) {
    ans->i_min = max(b1.i_min, b2.i_min);
    ans->i_max = min(b1.i_max, b2.i_max);
    ans->j_min = max(b1.j_min, b2.j_min);
    ans->j_max = min(b1.j_max, b2.j_max);
    
    return !is_empty_box_2d(*ans);
}



/*****************************************************************************
 *                           Parallel variables                              *
 *****************************************************************************/

/*
 *         y-axis
 *            ^        OVERLAP                             OVERLAP
 *            |       <———————>                           <———————>
 *            +       +———————+————————————————————————————+———————+ ^
 *            |       |       |                            |       | |
 *            |       |  N W  |            N M             |  N E  | | OVERLAP
 *            |       | (send)|           (send)           | (send)| |
 *      j_max +       |———————+————————————————————————————+———————| v
 *            |       |       |       !            !       |       |
 *            |       |       |  N W  !    N M     !  N E  |       |
 *            |       |       | (recv)!   (recv)   ! (recv)|       |
 *            |       |       |----------------------------|       |
 *            |       |       |       !            !       |       |
 *            |       |       |       !            !       |       |
 *            |       |  M W  |  M W  !This process!  M E  |  M E  |
 *            |       | (send)| (recv)!            ! (recv)| (send)|
 *            |       |       |       !            !       |       |
 *            |       |       |----------------------------|       |
 *            |       |       |       !            !       |       |
 *            |       |       |  S W  !            !  S E  |       |
 *            |       |       | (recv)!   (recv)   ! (recv)|       |
 *      j_min +       |———————+————————————————————————————+———————| ^
 *            |       |       |                            |       | |
 *            |       |  S W  |            S M             |  S E  | | OVERLAP
 *            |       | (send)|           (send)           | (send)| |
 *            +       +———————+————————————————————————————+———————+ v
 *            |
 *            |
 *            |
 *            +———————+———————+————————————————————————————+———————+————> x-axis
 *                           i_min                       i_max
 *
 * Each process handles the particles in a given sub-set of the physical space.
 * But it also keeps particles that leave a little bit this sub-set, to avoid unnecessary
 * communications of particles that could move back and forth the borders. This incurs
 * some overlapping of the sub-sets between processes. To compute rho, there is hence
 * the need to do a little communication between neighbors. The overlapped regions have
 * to be sent to the neighboring processes, and to be received from them.
 */

char neighbor_names_2d[NB_NEIGHBORS_2D][42] = {
    "NEIGHBOR_NORTH_WEST",
    "NEIGHBOR_NORTH_MIDL",
    "NEIGHBOR_NORTH_EAST",
    "NEIGHBOR_MIDDL_WEST",
    "NEIGHBOR_MIDDL_EAST",
    "NEIGHBOR_SOUTH_WEST",
    "NEIGHBOR_SOUTH_MIDL",
    "NEIGHBOR_SOUTH_EAST"
};

/*
 * Fill the automatic variables once the important ones have been fixed.
 */
void init_mpi_parameters(mpi_parameters* par_variables) {
    int i;
    int ix, iy;
    int pos;
    int neighbor;
    int ncx = par_variables->spatial_mesh->num_cell_x;
    int ncy = par_variables->spatial_mesh->num_cell_y;
    int ncx_local = par_variables->ncx_local;
    int ncy_local = par_variables->ncy_local;
    const int ncxminusone = ncx - 1;
    const int ncyminusone = ncy - 1;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    // MPI buffers for allgatherv of rho.
    par_variables->recv_counts = malloc(par_variables->mpi_world_size * sizeof(int));
    for (i = 0; i < par_variables->mpi_world_size; i++)
        par_variables->recv_counts[i] =
            (par_variables->layout_2d[i].i_max - par_variables->layout_2d[i].i_min + 1) *
            (par_variables->layout_2d[i].j_max - par_variables->layout_2d[i].j_min + 1);
    par_variables->displs = malloc(par_variables->mpi_world_size * sizeof(int));
    par_variables->displs[0] = 0;
    for (i = 1; i < par_variables->mpi_world_size; i++)
        par_variables->displs[i] = par_variables->displs[i-1] + par_variables->recv_counts[i-1];
    
    par_variables->rho_send_buf = allocateMatrix(par_variables->ncx_local, par_variables->ncy_local);
    par_variables->rho_recv_buf = allocateMatrix(ncx, ncy);
    
    // Computation of the indices we have for rho that should be sent to neighbors.
    box_2d local_box = par_variables->layout_2d[par_variables->mpi_rank];
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_SOUTH_WEST) { // WEST
            par_variables->rho_send_box[neighbor].i_min = (local_box.i_min - OVERLAP    ) & ncxminusone;
            par_variables->rho_send_box[neighbor].i_max = (local_box.i_min              );
            if (par_variables->rho_send_box[neighbor].i_max == 0)
                par_variables->rho_send_box[neighbor].i_max = ncx; // WARNING : assumption that rho stores the redundant ncx cell !
        } else if (neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_SOUTH_MIDL) { // MIDL
            par_variables->rho_send_box[neighbor].i_min = (local_box.i_min              );
            par_variables->rho_send_box[neighbor].i_max = (local_box.i_max              );
        } else { // EAST
            par_variables->rho_send_box[neighbor].i_min = (local_box.i_max + 1          ) & ncxminusone;
            par_variables->rho_send_box[neighbor].i_max = (local_box.i_max + OVERLAP + 1) & ncxminusone;
        }
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_NORTH_EAST) { // NORTH
            par_variables->rho_send_box[neighbor].j_min = (local_box.j_max + 1          ) & ncyminusone;
            par_variables->rho_send_box[neighbor].j_max = (local_box.j_max + OVERLAP + 1) & ncyminusone;
        } else if (neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_MIDDL_EAST) { // MIDDL
            par_variables->rho_send_box[neighbor].j_min = (local_box.j_min              );
            par_variables->rho_send_box[neighbor].j_max = (local_box.j_max              );
        } else { // SOUTH
            par_variables->rho_send_box[neighbor].j_min = (local_box.j_min - OVERLAP    ) & ncyminusone;
            par_variables->rho_send_box[neighbor].j_max = (local_box.j_min              );
            if (par_variables->rho_send_box[neighbor].j_max == 0)
                par_variables->rho_send_box[neighbor].j_max = ncy; // WARNING : assumption that rho stores the redundant ncy cell !
        }
    }
    
    // No need for modulos for receive boxes (because the boxes are already inside the local_box).
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_SOUTH_WEST) { // WEST
            par_variables->rho_recv_box[neighbor].i_min = (local_box.i_min              );
            par_variables->rho_recv_box[neighbor].i_max = (local_box.i_min + OVERLAP    );
        } else if (neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_SOUTH_MIDL) { // MIDL
            par_variables->rho_recv_box[neighbor].i_min = (local_box.i_min              );
            par_variables->rho_recv_box[neighbor].i_max = (local_box.i_max              );
        } else { // EAST
            par_variables->rho_recv_box[neighbor].i_min = (local_box.i_max - OVERLAP + 1);
            par_variables->rho_recv_box[neighbor].i_max = (local_box.i_max + 1          ); // WARNING : assumption that rho stores the redundant ncx cell !
        }
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_NORTH_EAST) { // NORTH
            par_variables->rho_recv_box[neighbor].j_min = (local_box.j_max - OVERLAP + 1);
            par_variables->rho_recv_box[neighbor].j_max = (local_box.j_max + 1          ); // WARNING : assumption that rho stores the redundant ncy cell !
        } else if (neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_MIDDL_EAST) { // MIDDL
            par_variables->rho_recv_box[neighbor].j_min = (local_box.j_min              );
            par_variables->rho_recv_box[neighbor].j_max = (local_box.j_max              );
        } else { // SOUTH
            par_variables->rho_recv_box[neighbor].j_min = (local_box.j_min              );
            par_variables->rho_recv_box[neighbor].j_max = (local_box.j_min + OVERLAP    );
        }
    }
    
    // overlapped_box[neighbor] is the indices that belong both to layout_2d[my_rank] +/- OVERLAP and to the neighbor.
    // It is not exactly par_variables->rho_send_box[neighbor] even if it is really close.
    box_2d overlapped_box[NB_NEIGHBORS_2D];
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_SOUTH_WEST) { // WEST
            overlapped_box[neighbor].i_min = (local_box.i_min - OVERLAP) & ncxminusone;
            overlapped_box[neighbor].i_max = (local_box.i_min - 1      ) & ncxminusone;
        } else if (neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_SOUTH_MIDL) { // MIDL
            overlapped_box[neighbor].i_min = (local_box.i_min          );
            overlapped_box[neighbor].i_max = (local_box.i_max          );
        } else { // EAST
            overlapped_box[neighbor].i_min = (local_box.i_max + 1      ) & ncxminusone;
            overlapped_box[neighbor].i_max = (local_box.i_max + OVERLAP) & ncxminusone;
        }
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_NORTH_EAST) { // NORTH
            overlapped_box[neighbor].j_min = (local_box.j_max + 1      ) & ncyminusone;
            overlapped_box[neighbor].j_max = (local_box.j_max + OVERLAP) & ncyminusone;
        } else if (neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_MIDDL_EAST) { // MIDDL
            overlapped_box[neighbor].j_min = (local_box.j_min          );
            overlapped_box[neighbor].j_max = (local_box.j_max          );
        } else { // SOUTH
            overlapped_box[neighbor].j_min = (local_box.j_min - OVERLAP) & ncyminusone;
            overlapped_box[neighbor].j_max = (local_box.j_min - 1      ) & ncyminusone;
        }
    }
    
    // Computation of the send_area_indices. If, during the update position step, a particle has its
    // new cell inside send_area_indices[neighbor], it will be sent to this neighbor.
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        par_variables->send_area_sizes[neighbor] = 0;
        box_2d neighbor_box = par_variables->layout_2d[par_variables->neighbors_ids[neighbor]];
        int start_i, stop_i, start_j, stop_j;
        // In general, particles_send_box[neighbor] sould be equal to layout_2d[neighbors_ids[neighbor]] \setminus overlapped_box[neighbor]...
        start_i = neighbor_box.i_min;
        stop_i  = neighbor_box.i_max;
        start_j = neighbor_box.j_min;
        stop_j  = neighbor_box.j_max;
        // WARNING : ...but here we make it equal to just MARGIN.
        // We make the explicit assumption that a particle cannot cross more than MARGIN
        // cells in one time step, because we do not allocate bags over that limit.
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_SOUTH_WEST) { // WEST
            start_i = neighbor_box.i_max - OVERLAP - MARGIN + 1;
            stop_i  = neighbor_box.i_max;
        } else if (neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_SOUTH_MIDL) { // MIDL
            start_i = neighbor_box.i_min;
            stop_i  = neighbor_box.i_max;
        } else { // EAST
            start_i = neighbor_box.i_min;
            stop_i  = neighbor_box.i_min + OVERLAP + MARGIN - 1;
        }
        if (neighbor == NEIGHBOR_NORTH_WEST || neighbor == NEIGHBOR_NORTH_MIDL || neighbor == NEIGHBOR_NORTH_EAST) { // NORTH
            start_j = neighbor_box.j_min;
            stop_j  = neighbor_box.j_min + OVERLAP + MARGIN - 1;
        } else if (neighbor == NEIGHBOR_MIDDL_WEST || neighbor == NEIGHBOR_MIDDL_EAST) { // MIDDL
            start_j = neighbor_box.j_min;
            stop_j  = neighbor_box.j_max;
        } else { // SOUTH
            start_j = neighbor_box.j_max - OVERLAP - MARGIN + 1;
            stop_j  = neighbor_box.j_max;
        }
        
        for (ix = start_i; ix <= stop_i; ix++) {
        for (iy = start_j; iy <= stop_j; iy++) {
            if (!(ix >= overlapped_box[neighbor].i_min && ix <= overlapped_box[neighbor].i_max &&
                    iy >= overlapped_box[neighbor].j_min && iy <= overlapped_box[neighbor].j_max))
                par_variables->send_area_sizes[neighbor]++;
        }}
        par_variables->send_area_indices[neighbor] = malloc(par_variables->send_area_sizes[neighbor] * sizeof(int));
        pos = 0;
        for (ix = start_i; ix <= stop_i; ix++) {
        for (iy = start_j; iy <= stop_j; iy++) {
            if (!(ix >= overlapped_box[neighbor].i_min && ix <= overlapped_box[neighbor].i_max &&
                    iy >= overlapped_box[neighbor].j_min && iy <= overlapped_box[neighbor].j_max))
                par_variables->send_area_indices[neighbor][pos++] = COMPUTE_I_CELL_2D(icell_param, ix & ncxminusone, iy & ncyminusone);
        }}
    }

/*
// Testing purposes only.
if (par_variables->mpi_rank == 0) {
  printf("My box : ");
  print_box_2d(par_variables->layout_2d[par_variables->mpi_rank]);
  for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
    printf("%s (%d values) : ", neighbor_names_2d[neighbor], par_variables->send_area_sizes[neighbor]);
    for (pos = 0; pos < par_variables->send_area_sizes[neighbor]; pos++)
      printf("%d ", par_variables->send_area_indices[neighbor][pos]);
    printf("\n");
  }
}
exit(0);
*/
    
    // Allocation of the MPI buffers for rho exchange.
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        par_variables->rho_boxes_sizes[neighbor] = number_points_box_2d(par_variables->rho_send_box[neighbor]);
        par_variables->rho_send_bufs[neighbor] = malloc(par_variables->rho_boxes_sizes[neighbor] * sizeof(double));
        par_variables->rho_recv_bufs[neighbor] = malloc(par_variables->rho_boxes_sizes[neighbor] * sizeof(double));
    }
    
    // Allocation of rho_2d.
    par_variables->rho_2d = allocate_matrix(ncx + 1, ncy + 1);
    
    // Computation of the indices that have some particles on this process but that
    // have mainly particles on neighbor processes (due to overlapping).
    par_variables->num_overlapped_points = 2 * (ncx_local + ncy_local + 2 * OVERLAP) * OVERLAP;
    par_variables->overlapped_indices = malloc(par_variables->num_overlapped_points * sizeof(int));
    pos = 0;
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++)
        for (ix = overlapped_box[neighbor].i_min; ix <= overlapped_box[neighbor].i_max; ix++)
        for (iy = overlapped_box[neighbor].j_min; iy <= overlapped_box[neighbor].j_max; iy++)
            par_variables->overlapped_indices[pos++] = COMPUTE_I_CELL_2D(icell_param, ix, iy);
    
    // Initial allocation of the MPI buffers for particle exchange.
    int initial_size = 500;
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        par_variables->current_send_size[neighbor] = initial_size;
        par_variables->current_recv_size[neighbor] = initial_size;
        par_variables->ic_send[neighbor] = malloc(initial_size * sizeof(int));
        par_variables->dx_send[neighbor] = malloc(initial_size * sizeof(float));
        par_variables->dy_send[neighbor] = malloc(initial_size * sizeof(float));
        par_variables->vx_send[neighbor] = malloc(initial_size * sizeof(double));
        par_variables->vy_send[neighbor] = malloc(initial_size * sizeof(double));
        par_variables->ic_recv[neighbor] = malloc(initial_size * sizeof(int));
        par_variables->dx_recv[neighbor] = malloc(initial_size * sizeof(float));
        par_variables->dy_recv[neighbor] = malloc(initial_size * sizeof(float));
        par_variables->vx_recv[neighbor] = malloc(initial_size * sizeof(double));
        par_variables->vy_recv[neighbor] = malloc(initial_size * sizeof(double));
    }
}

/*
 * Cuts the 2d mesh ncx x ncy into world_size boxes ; each MPI process will
 * handle one of those boxes.
 * Affect more MPI processes to the dimensions that have bigger number of points.
 *
 * Required fields : mpi_world_size, spatial_mesh.
 * Updated fields : num_procs_x, num_procs_y.
 * @param[in, out] par_variables
 */
void affect_processors_to_2d_mesh(mpi_parameters* par_variables) {
    int mpi_world_size = par_variables->mpi_world_size;
    int ncx = par_variables->spatial_mesh->num_cell_x;
    int ncy = par_variables->spatial_mesh->num_cell_y;

    // power2 is the maximum power of 2 <= world_size
    int power2 = int_pow(2, (int)(log((double)mpi_world_size) / log(2.)));

    // Affect the processors depending on the mesh size.
    par_variables->num_procs_x = 1;
    par_variables->num_procs_y = 1;
    while(par_variables->num_procs_x * par_variables->num_procs_y < power2) {
        if ((double)par_variables->num_procs_x  / ((double)ncx) < (double)par_variables->num_procs_y  / ((double)ncy))
            par_variables->num_procs_x *= 2;
        else
            par_variables->num_procs_y *= 2;
    }
}

/*
 * Auxiliary function that splits a range of indices described by 2 integers
 * into a given number of intervals.
 * The function tries to partition the original interval equitably.
 */
void split_array_indices_aux(int** intervals_array,
        int start_index, int interval_segment_length,
        int min, int max) {
    if (interval_segment_length == 1) {
        // terminate recursion by filling values for this interval
        intervals_array[start_index][0] = min;
        intervals_array[start_index][1] = max;
    } else {
        // split this interval and launch new recursions
        split_array_indices_aux(intervals_array,
            start_index,                             interval_segment_length/2, min, min + (max - min) / 2);
        split_array_indices_aux(intervals_array,
            start_index + interval_segment_length/2, interval_segment_length/2, min + (max - min) / 2 + 1, max);
    }
}

int** split_array_indices(int num_elements, int num_intervals) {
    if (num_elements < num_intervals) {
        fprintf(stderr, "Requested to split %d elements into %d intervals : impossible.\n",
            num_elements, num_intervals);
        exit(EXIT_FAILURE);
    }
    int** intervals = allocate_int_matrix(num_intervals, 2);
    split_array_indices_aux(intervals, 0, num_intervals, 0, num_elements - 1);
    return intervals;
}

/*
 * Return a column-major mapping (i, j) -> mapping.
 *
 * @param[in] ncx number of rows (direction x)
 * @param[in] ncy number of cols (direction y)
 * @param[in] i index on direction x
 * @param[in] j index on direction y
 * @return    the column-major index corresponding to the grid sizes and indexes.
 */
int linear_index_2d(int ncx, int ncy, int i, int j) {
    return i + j * ncx;
}

/*
 * Creates the initial layout, depending on the number of processors available
 * and the number of cells in each direction.
 * TODO: load-balancing : split the full domain in subdomains on which the
 * integral of f is more or less constant. For small perturbations, we don't
 * care.
 *
 * Required fields : mpi_rank, spatial_mesh, num_procs_x, num_procs_y.
 * Updated fields : ncx_local, ncy_local.
 *
 * @param[in, out] par_variables
 */
void initialize_layout_with_distributed_2d_array(mpi_parameters* par_variables) {
    int ncx = par_variables->spatial_mesh->num_cell_x;
    int ncy = par_variables->spatial_mesh->num_cell_y;
    int num_procs_x = par_variables->num_procs_x;
    int num_procs_y = par_variables->num_procs_y;
    // num_procs_x and num_procs_y must be powers of 2.
    int num_procs_x_minus_one = num_procs_x - 1;
    int num_procs_y_minus_one = num_procs_y - 1;
    int i, j, node;
    int my_i = -1;
    int my_j = -1;
    box_2d box;
    int** intervals_x = split_array_indices(ncx, num_procs_x);
    int** intervals_y = split_array_indices(ncy, num_procs_y);
    
    par_variables->layout_2d = malloc(num_procs_x * num_procs_y * sizeof(box_2d));
    
    for (i = 0; i < num_procs_x; i++) {
        for (j = 0; j < num_procs_y; j++) {
            box.i_min = intervals_x[i][0];
            box.i_max = intervals_x[i][1];
            box.j_min = intervals_y[j][0];
            box.j_max = intervals_y[j][1];
            node = linear_index_2d(num_procs_x, num_procs_y, i, j);
            par_variables->layout_2d[node] = box;
            if (node == par_variables->mpi_rank) {
                my_i = i;
                my_j = j;
                par_variables->ncx_local = box.i_max - box.i_min + 1;
                par_variables->ncy_local = box.j_max - box.j_min + 1;
            }
        }
    }
    deallocate_int_matrix(intervals_x, num_procs_x, 2);
    deallocate_int_matrix(intervals_y, num_procs_y, 2);
    
    if (my_i == -1 || my_j == -1) {
        fprintf(stderr, "Irrecuperable error in initialize_layout_with_distributed_2d_array.\n");
        exit(EXIT_FAILURE);
    }
    
/*
 // Test purposes only.
 if (par_variables->mpi_rank == 0) {
   for (i = 0; i < num_procs_x; i++) {
     for (j = 0; j < num_procs_y; j++) {
       printf("Proc_indices(%d, %d) : ", i, j);
       node = linear_index_2d(num_procs_x, num_procs_y, i, j);
       print_box_2d(par_variables->layout_2d[node]);
     }
   }
 }
*/
    
    par_variables->neighbors_ids[NEIGHBOR_NORTH_WEST] = linear_index_2d(num_procs_x, num_procs_y, (my_i - 1) & num_procs_x_minus_one, (my_j + 1) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_NORTH_MIDL] = linear_index_2d(num_procs_x, num_procs_y, (my_i    ) & num_procs_x_minus_one, (my_j + 1) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_NORTH_EAST] = linear_index_2d(num_procs_x, num_procs_y, (my_i + 1) & num_procs_x_minus_one, (my_j + 1) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_MIDDL_WEST] = linear_index_2d(num_procs_x, num_procs_y, (my_i - 1) & num_procs_x_minus_one, (my_j    ) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_MIDDL_EAST] = linear_index_2d(num_procs_x, num_procs_y, (my_i + 1) & num_procs_x_minus_one, (my_j    ) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_SOUTH_WEST] = linear_index_2d(num_procs_x, num_procs_y, (my_i - 1) & num_procs_x_minus_one, (my_j - 1) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_SOUTH_MIDL] = linear_index_2d(num_procs_x, num_procs_y, (my_i    ) & num_procs_x_minus_one, (my_j - 1) & num_procs_y_minus_one);
    par_variables->neighbors_ids[NEIGHBOR_SOUTH_EAST] = linear_index_2d(num_procs_x, num_procs_y, (my_i + 1) & num_procs_x_minus_one, (my_j - 1) & num_procs_y_minus_one);
    
/*
 // Test purposes only.
 if (par_variables->mpi_rank == 0) {
   printf("My box : ");
   print_box_2d(par_variables->layout_2d[par_variables->mpi_rank]);
   for (i = 0; i < NB_NEIGHBORS_2D; i++) {
     printf("%s : ", neighbor_names_2d[i]);
     print_box_2d(par_variables->layout_2d[par_variables->neighbors_ids[i]]);
   }
 }
 exit(0);
*/
}



/*****************************************************************************
 *                                Update rho                                 *
 *****************************************************************************/

/*
 * Computes the integral of f on v.
 *
 * @param[in, out] par_variables
 */
void update_rho_2d(mpi_parameters* par_variables) {
    size_t i, j;
    int neighbor, process;
    int pos;
    MPI_Request request_recv[NB_NEIGHBORS_2D];
    MPI_Request request_send[NB_NEIGHBORS_2D];
    MPI_Status status;
    int ncx = par_variables->spatial_mesh->num_cell_x;
    int ncy = par_variables->spatial_mesh->num_cell_y;
    
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        process = par_variables->neighbors_ids[neighbor];
        // Receive the overlap
        MPI_Irecv(&par_variables->rho_recv_bufs[neighbor][0],
                  par_variables->rho_boxes_sizes[neighbor],
                  MPI_DOUBLE,
                  process,
                  neighbor, // With less than 16 processes, a same process may be neighbor multiple times, hence need for different tags.
                  MPI_COMM_WORLD,
                  &request_recv[neighbor]);
        // Write the overlap from rho_2d into rho_send_bufs[neighbor]
        pos = 0;
        for (i = par_variables->rho_send_box[neighbor].i_min; i <= par_variables->rho_send_box[neighbor].i_max; i++)
            for (j = par_variables->rho_send_box[neighbor].j_min; j <= par_variables->rho_send_box[neighbor].j_max; j++)
                par_variables->rho_send_bufs[neighbor][pos++] = par_variables->rho_2d[i][j];
        // Send the overlap
        MPI_Isend(&par_variables->rho_send_bufs[neighbor][0],
                  par_variables->rho_boxes_sizes[neighbor],
                  MPI_DOUBLE,
                  process,
                  symmetric(neighbor), // With less than 16 processes, a same process may be neighbor multiple times, hence need for different tags. Tags must be the same, so use symmetry.
                  MPI_COMM_WORLD,
                  &request_send[neighbor]);
    }
    // Synchronisation
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        MPI_Wait(&request_recv[neighbor], &status);
        MPI_Wait(&request_send[neighbor], &status);
    }
    
    // Reduce from rho_recv_bufs[neighbor] into rho_2d
    for (neighbor = 0; neighbor < NB_NEIGHBORS_2D; neighbor++) {
        pos = 0;
        for (i = par_variables->rho_recv_box[neighbor].i_min; i <= par_variables->rho_recv_box[neighbor].i_max; i++)
            for (j = par_variables->rho_recv_box[neighbor].j_min; j <= par_variables->rho_recv_box[neighbor].j_max; j++)
                par_variables->rho_2d[i][j] += par_variables->rho_recv_bufs[neighbor][pos++];
    }
    
    // Puts the local rho_2d in a linearized fashion to be sent by mpi.
    box_2d local_box = par_variables->layout_2d[par_variables->mpi_rank];
    pos = 0;
    for (i = local_box.i_min; i <= local_box.i_max; i++)
        for (j = local_box.j_min; j <= local_box.j_max; j++)
            par_variables->rho_send_buf[pos++] = par_variables->rho_2d[i][j];
    
    // Gather the concatenation of all the linearised rho_2d_locals
    MPI_Allgatherv(par_variables->rho_send_buf, par_variables->ncx_local * par_variables->ncy_local, MPI_DOUBLE_PRECISION,
        par_variables->rho_recv_buf, par_variables->recv_counts, par_variables->displs, MPI_DOUBLE_PRECISION, MPI_COMM_WORLD);
    
    // Rebuild rho_2d from the concatenation of the linearised rho_2d_locals
    pos = 0;
    for (neighbor = 0; neighbor < par_variables->mpi_world_size; neighbor++)
        for (i = par_variables->layout_2d[neighbor].i_min; i <= par_variables->layout_2d[neighbor].i_max; i++)
            for (j = par_variables->layout_2d[neighbor].j_min; j <= par_variables->layout_2d[neighbor].j_max; j++)
                par_variables->rho_2d[i][j] = par_variables->rho_recv_buf[pos++];
    
/*
    // Test the initial rho.
  if (par_variables->mpi_rank == 0) {
    FILE* file_diag_rho = fopen("test_rho.dat", "w");
    for (i = 0; i < ncx; i++) {
        for (j = 0; j < ncy; j++)
            fprintf(file_diag_rho, "%f ", par_variables->rho_2d[i][j]);
        fprintf(file_diag_rho, "\n");
    }
    fclose(file_diag_rho);
  }
    MPI_Finalize();
    exit(0);
*/
    
    // Periodicity
    for (i = 0; i < ncx + 1; i++)
        par_variables->rho_2d[i][ncy] = par_variables->rho_2d[i][0];
    for (j = 0; j < ncy + 1; j++)
        par_variables->rho_2d[ncx][j] = par_variables->rho_2d[0][j];
}

