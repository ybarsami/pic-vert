#include <omp.h>                   // functions omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                 // functions fprintf, fopen, fclose
                                   // constant  stderr (standard error output stream)
#include <stdlib.h>                // functions exit (error handling), malloc, free ((de)allocate memory)
                                   // constant  EXIT_FAILURE (error handling)
                                   // type      size_t
#include "initial_distributions.h" // types     speeds_generator_1d, distribution_function_1d, max_distribution_function
                                   // variables speed_generators_1d, distribution_funs_1d, distribution_maxs_1d
#include "matrix_functions.h"      // functions allocate_aligned_int_matrix, deallocate_int_matrix
#include "meshes.h"                // type      cartesian_mesh_1d
#include "particle_type_soa_1d.h"  // type      particle_sorter_oop_1d
#include "random.h"                // function  pic_vert_next_random_double

/*
 * Initializes arrays of num_particle random particles following a given distribution
 * for positions, and following the gaussian distribution for speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] i_cell, dx, vx[num_particle] newly allocated arrays of randomized particles.
 */
void create_particle_arrays_1d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_1d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        int** i_cell, float** dx, double** vx) {
    size_t i, j;
    double x, v_x;
    double control_point, evaluated_function;
    speeds_generator_1d speeds_generator = speed_generators_1d[sim_distrib];
    distribution_function_1d distrib_function = distribution_funs_1d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_1d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;

    *weight = (float)x_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        (*speeds_generator)(speed_params, &v_x);
        (*i_cell)[j] = (int)x;
        (*dx)[j]     = (float)(x - (int)x);
        (*vx)[j]     = v_x;
    }

    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_1d.dat", "w");
    unsigned int num_cells_1d = mesh.num_cell_x;
    unsigned int* nb_particle_per_cell = malloc(num_cells_1d * sizeof(unsigned int));
    for (i = 0; i < num_cells_1d; i++)
        nb_particle_per_cell[i] = 0;
    for (i = 0; i < num_particle; i++)
        nb_particle_per_cell[(*i_cell)[i]]++;
    switch(sim_distrib) {
        default:
            for (i = 0; i < mesh.num_cell_x; i++)
                fprintf(file_diag_particles, "%ld %d\n", i, nb_particle_per_cell[i]);
    }
    free(nb_particle_per_cell);
    fclose(file_diag_particles);
}



/*****************************************************************************
 *                           Sorting functions                               *
 *                     (by increasing values of i_cell)                      *
 *****************************************************************************/

/*
 * @param[in] num_particle, number of particles in the array to sort.
 * @param[in] num_cell, number of cells in the mesh (number of different values for i_cell).
 * @return    a particle sorter.
 */
particle_sorter_oop_1d new_particle_sorter_oop_1d(unsigned int num_particle, unsigned int num_cell) {
    int num_threads;

    particle_sorter_oop_1d sorter;
    sorter.num_particle = num_particle;
    sorter.num_cell = num_cell;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    sorter.num_particle_per_cell = allocate_aligned_int_matrix(num_threads, num_cell);
    sorter.index_next_particle   = allocate_aligned_int_matrix(num_threads, num_cell);
    if (posix_memalign((void**)&sorter.dx_tmp, VEC_ALIGN, num_particle * sizeof(float))) {
        fprintf(stderr, "posix_memalign failed to initialize dx_tmp.\n");
        exit(EXIT_FAILURE);
    }
    if (posix_memalign((void**)&sorter.vx_tmp, VEC_ALIGN, num_particle * sizeof(double))) {
        fprintf(stderr, "posix_memalign failed to initialize vx_tmp.\n");
        exit(EXIT_FAILURE);
    }
    return sorter;
}

/*
 * Sort the array of particles (with temporary arrays).
 *
 * @param[in, out] sorter a pointer on the particle_sorter_oop for this array (has to be initialized
 *                 before the call).
 * @param[in, out] i_cell, dx, vx pointers on the [num_particle] arrays of particles to sort.
 */
void sort_particles_oop_1d(particle_sorter_oop_1d* sorter, int** i_cell,
        float** dx, double** vx) {
    size_t i, k;
    int start_index, stop_index;
    int thread_id, num_threads;
    float* float_array_tmp;
    double* double_array_tmp;
    
    #pragma omp parallel private(thread_id, i, k, start_index, stop_index)
    {
        num_threads = omp_get_num_threads();
        thread_id = omp_get_thread_num();
        
        // Count how many particles are in each cell.
        for (i = 0; i < sorter->num_cell; i++)
            sorter->num_particle_per_cell[thread_id][i] = 0;
        
        #pragma omp for schedule(static)
        for (i = 0; i < sorter->num_particle; i++)
            sorter->num_particle_per_cell[thread_id][(*i_cell)[i]]++;
        
        // The implicit barrier makes sure that all the threads updated num_particle_per_cell before the prefix scan.
        
        // At the beginning, no particle is supposed to be in its right place. So for each cell,
        // the index in which to put the next particle is sum_{k = 0}^{cell-1} num_particle_per_cell[k]
        sorter->index_next_particle[thread_id][0] = 0;
        for (i = 0; i < thread_id; i++)
            sorter->index_next_particle[thread_id][0] += sorter->num_particle_per_cell[i][0];
        for (k = 1; k < sorter->num_cell; k++) {
            sorter->index_next_particle[thread_id][k] = sorter->index_next_particle[thread_id][k - 1];
            for (i = thread_id; i < num_threads; i++)
                sorter->index_next_particle[thread_id][k] += sorter->num_particle_per_cell[i][k - 1];
            for (i = 0; i < thread_id; i++)
                sorter->index_next_particle[thread_id][k] += sorter->num_particle_per_cell[i][k];
        }
        
        // For each particle, put it into its right place in the particles_tmp array,
        // (at the index given by index_next_particle), and increment index_next_particle.
        // Schedule static ensures that this loop is divided among threads as the previous omp loop.
        #pragma omp for schedule(static)
        for (i = 0; i < sorter->num_particle; i++) {
            sorter->dx_tmp[sorter->index_next_particle[thread_id][(*i_cell)[i]]] = (*dx)[i];
            sorter->vx_tmp[sorter->index_next_particle[thread_id][(*i_cell)[i]]] = (*vx)[i];
            sorter->index_next_particle[thread_id][(*i_cell)[i]]++;
        }
        
        // Because i_cell is now sorted and we counted, for each value i, the number of particles
        // nb(i) that have i_cell value i, we can just put "i" inside the i_cell array nb(i) times.
        // This allows to scan the i_cell array with better locality and to avoid using a temporary
        // array, compared to having the update inside the previous loop.
        for (i = 0; i < sorter->num_cell; i++) {
            stop_index = sorter->index_next_particle[thread_id][i];
            start_index = stop_index - sorter->num_particle_per_cell[thread_id][i];
            for (k = start_index; k < stop_index; k++)
                (*i_cell)[k] = i;
        }
    }
    
    float_array_tmp = *dx;
    *dx             = sorter->dx_tmp;
    sorter->dx_tmp  = float_array_tmp;
    double_array_tmp = *vx;
    *vx              = sorter->vx_tmp;
    sorter->vx_tmp   = double_array_tmp;
}

void free_particle_sorter_oop_1d(particle_sorter_oop_1d sorter, unsigned int num_cell) {
    int num_threads;

    #pragma omp parallel
    num_threads = omp_get_num_threads();
    deallocate_int_matrix(sorter.num_particle_per_cell, num_threads, num_cell);
    deallocate_int_matrix(sorter.index_next_particle,   num_threads, num_cell);
    free(sorter.dx_tmp);
    free(sorter.vx_tmp);
}

