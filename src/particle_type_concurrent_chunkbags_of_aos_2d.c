#include <mpi.h>                                          // constants MPI_UNSIGNED, MPI_SUM, MPI_COMM_WORLD
                                                          // function  MPI_Allreduce
#include <omp.h>                                          // function  omp_get_num_threads
#include <stdbool.h>                                      // constant  true
#include <stdio.h>                                        // functions fprintf, fopen, fclose, fscanf, sprintf
                                                          // constant  stderr (standard error output stream)
#include <stdlib.h>                                       // functions exit (error handling), malloc, free ((de)allocate memory)
                                                          // constant  EXIT_FAILURE (error handling)
                                                          // type      size_t
#include "domain_decomposition.h"                         // type      mpi_parameters
#include "initial_distributions.h"                        // types     speeds_generator_2d, distribution_function_2d, max_distribution_function
                                                          // variables speed_generators_2d, distribution_funs_2d, distribution_maxs_2d
#include "matrix_functions.h"                             // functions allocate_int_array, allocate_aligned_int_matrix
#include "meshes.h"                                       // type      cartesian_mesh_2d
#include "parameters.h"                                   // constants DBL_DECIMAL_DIG, FLT_DECIMAL_DIG
#include "particle_type_concurrent_chunkbags_of_aos_2d.h" // types     particle, chunk, bag
                                                          // variables free_index, FREELIST_SIZE
#include "random.h"                                       // function  pic_vert_next_random_double
#include "space_filling_curves.h"                         // macros    I_CELL_PARAM_2D, ICX_PARAM_2D, ICY_PARAM_2D,
                                                          //           COMPUTE_I_CELL_2D, ICX_FROM_I_CELL_2D, ICY_FROM_I_CELL_2D

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 *            Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |       +—————————————————————————————————————+
 *           |                                             |
 *           |     +——————+     +— ...     ——+     +————+  |
 *           |     |      |     |            |     |    |  |
 *           v     |      v     |            v     |    v  v
 *       |———————| |  |———————| |        |———————| |  |———————|
 *       |       | |  |       | |        |       | |  |       |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *       |       |    |       |          |       |    |       |
 *  size |  16   |    |  11   |          |  13   |    |  17   |
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *     0 |       |    |       |          |       |    |       |
 *     1 |       |    |       |          |       |    |       |
 *     2 |       |    |       |          |       |    |       |
 *     3 |       |    |       |          |       |    |       |
 *     4 |       |    |       |          |       |    |       |
 *     5 |       |    |       |          |       |    |       |
 *     6 |       |    |       |          |       |    |       |
 *     7 |       |    |       |          |       |    |       |
 *     8 |       |    |       |          |       |    |       |
 *     9 |       |    |       |          |       |    |       |
 *    10 |       |    |       |          |       |    |       |
 *    11 |       |    |   ?   |          |       |    |       |
 *    12 |       |    |   ?   |          |       |    |       |
 *    13 |       |    |   ?   |          |   ?   |    |       |
 *    14 |       |    |   ?   |          |   ?   |    |       |
 *    15 |       |    |   ?   |          |   ?   |    |       |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *
 */

chunk*** free_chunks;

/*
 * Fills free_chunks with as many chunks as possible and sets free_index accordingly.
 *
 * @param mpi_world_size the number of mpi processes when using domain decomposition. Optional, defaults to 1.
 */
void init_freelists(long int num_particle, int mpi_world_size) {
  int num_threads;
  #pragma omp parallel
  num_threads = omp_get_num_threads();
  FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads / mpi_world_size;
  free_chunks = malloc(num_threads * sizeof(chunk**));
  free_index = allocate_aligned_int_matrix(num_threads, 1);
  for (size_t i = 0; i < num_threads; i++) {
    // The freelists could be aligned to the cache size, but this doesn't matter:
    // their size ensure that there won't be any false sharing.
    free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
    for (size_t j = 0; j < FREELIST_SIZE; j++)
      free_chunks[i][j] = malloc(sizeof(chunk));
    FREE_INDEX(i) = FREELIST_SIZE;
  }
#ifdef PIC_VERT_TESTING
  nb_malloc = allocate_int_array(num_threads);
  nb_free   = allocate_int_array(num_threads);
  for (size_t i = 0; i < num_threads; i++) {
    nb_malloc[i] = 0;
    nb_free[i]   = 0;
  }
#endif
}

chunk* chunk_alloc(int thread_id) {
  if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
#ifdef PIC_VERT_TESTING
    nb_malloc[thread_id]++;
#endif
    return (chunk*) malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
  if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
#ifdef PIC_VERT_TESTING
    nb_free[thread_id]++;
#endif
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the front of a chunk bag.
 * We have to set the field "size" to 0, because:
 *   1. When a chunk goes back to a freelist, this field is not reset.
 *   2. When a chunk is newly allocated, this field is not set neither.
 * It is simpler to set to 0 in this lone function, rather than
 * both in the allocation and in the free functions.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 */
void add_front_chunk(bag* b, int thread_id) {
  chunk* c = chunk_alloc(thread_id);
  c->size = 0;
  c->next = b->front;
  #pragma omp atomic write
  b->front = c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |  +————+
 *           |  |
 *           v  v
 *        |———————|
 *        |       |
 *   next |   X———+——>NIL
 *        |       |
 *        |———————|
 *        |       |
 *   size |   0   |
 *        |       |
 *        |———————|
 *      0 |   ?   |
 *      1 |   ?   |
 *      2 |   ?   |
 *      3 |   ?   |
 *      4 |   ?   |
 *      5 |   ?   |
 *      6 |   ?   |
 *      7 |   ?   |
 *      8 |   ?   |
 *      9 |   ?   |
 *     10 |   ?   |
 *     11 |   ?   |
 *     12 |   ?   |
 *     13 |   ?   |
 *     14 |   ?   |
 *     15 |   ?   |
 *     16 |   ?   |
 *     17 |   ?   |
 *     18 |   ?   |
 *     19 |   ?   |
 *        |———————|
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int thread_id) {
  // Initialize b->front to null before because add_front_chunk
  // uses b->front to set the front chunk's next field.
  b->front = (void*)0;
  add_front_chunk(b, thread_id);
  b->back = b->front;
}

/*
 * Nullify a bag (already allocated).
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           v       v
 *              null
 *
 * @param[in, out] b the bag to nullify.
 */
void bag_nullify(bag* b) {
  b->front = (void*)0;
  b->back  = (void*)0;
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c) {
    size += c->size;
    c = c->next;
  }
  return size;
}

/* 
 * Merge other into b; other is re-initialized.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int thread_id) {
  b->back->next = other->front;
  b->back       = other->back;
  bag_init(other, thread_id);
}

/* 
 * Merge other into b; other is nullified.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append_nullify(bag* b, bag* other) {
  if (other->front) {
    b->back->next = other->front;
    b->back       = other->back;
    bag_nullify(other);
  }
}

/* 
 * Merge other into b; other is re-initialized if not null,
 * and stays null if it was already null.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append_null_remains(bag* b, bag* other, int thread_id) {
  if (other->front)
    bag_append(b, other, thread_id);
}

/*
 * Guarantee that the entire value of *p is read atomically.
 * No part of *p can change during the read operation.
 */
chunk* atomic_read(chunk** p) {
  chunk* value;
  #pragma omp atomic read
  value = *p;
  return value;
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is thread-safe (uses atomics).
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push_concurrent(bag* b, particle p, int thread_id) {
  chunk* c;
  int index;
  while (true) { // Until success.
    c = b->front;

    #pragma omp atomic capture
    index = c->size++;

    if (index < CHUNK_SIZE) {
      // The chunk is not full, we can write the particle.
      c->array[index] = p;
      if (index == CHUNK_SIZE - 1) {
        // The chunk is now full, we have to extend the bag.
        // Inside add_front_chunk, the update of the b-> front
        // pointer is made atomic so that other threads see the update.
        add_front_chunk(b, thread_id);
      }
      return;
    } else {
      // The chunk is full, another thread has just pushed a particle
      // at the end, and is now extending the bag.
      // First we have to cancel our additional "c->size++".
      c->size = CHUNK_SIZE;
      while (atomic_read(&b->front) == c) {
        // Then we wait until the other thread extends the bag.
        // The atomic_read forces the thread to read the value in the
        // main memory, and not in its temporary view.
      }
    }
  }
}

/* 
 * Add a particle into a chunk bag. Add it into the first chunk,
 * then tests if this chunk is full. In that case, allocate a new
 * chunk after adding the particle.
 * This function is not thread-safe. Multiple threads should not
 * access the same bag.
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push_serial(bag* b, particle p, int thread_id) {
  chunk* c = b->front;
  int index = c->size++;
  c->array[index] = p;
  if (index == CHUNK_SIZE - 1) {
    // chunk is now full, we have to extend the bag
    add_front_chunk(b, thread_id);
  }
}

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the number of particles.
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy;
    
    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        printf("%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3) {
        printf("%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        printf("I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell, &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy,
                                                             .vx = vx, .vy = vy }, 0);
    }
    fclose(file_read_particles);
}

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the number of particles.
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t i, j;
    int i_cell;
    double x, y, vx, vy;
    double control_point, evaluated_function;
    speeds_generator_2d speeds_generator = speed_generators_2d[sim_distrib];
    distribution_function_2d distrib_function = distribution_funs_2d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_2d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    *weight = (float)x_range * (float)y_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        (*speeds_generator)(speed_params, &vx, &vy);
        i_cell = COMPUTE_I_CELL_2D(icell_param, (int)x, (int)y);
        bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .dy = (float)(y - (int)y),
                                                             .vx = vx, .vy = vy }, 0);
    }
    
    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_2d.dat", "w");
    long int nb_particle_in_cell;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
        case TWO_STREAM_1D_PROJ2D:
            for (i = 0; i < ncx; i++) {
                nb_particle_in_cell = 0;
                for (j = 0; j < ncy; j++)
                    nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)]));
                fprintf(file_diag_particles, "%ld %ld\n", i, nb_particle_in_cell);
            }
            break;
        default:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++)
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)])));
    }
    fclose(file_diag_particles);
}

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  par_variables the MPI parameters.
 * @param[in]  num_particle the number of particles.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_dd_particle_array_2d(mpi_parameters par_variables, long int num_particle,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int num_read_particle, total_num_read_particle;
    cartesian_mesh_2d mesh = *par_variables.spatial_mesh;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy;
    
    sprintf(filename, "initial_particles_%ldkk_rank%d.dat", num_particle / 1000000, par_variables.mpi_rank);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        printf("%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &num_read_particle) < 3) {
        printf("%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        printf("I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    MPI_Allreduce(&num_read_particle, &total_num_read_particle, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
    if (total_num_read_particle != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file(s).\n", num_particle, total_num_read_particle);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)num_particle);
    for (i = 0; i < num_read_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell, &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_read_particle);
            exit(EXIT_FAILURE);
        }
        bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy,
                                                             .vx = vx, .vy = vy }, 0);
    }
    fclose(file_read_particles);
}

/*
 * Split a file with all the particles of the form initial_particles_%dkk.dat in 4 files
 * of the form initial_particles_%dkk_rank%d.dat
 *
 * @param[in]  par_variables the MPI parameters.
 * @param[in]  num_particle the number of particles.
 */
void read_and_split_particles_2d(mpi_parameters par_variables, long int num_particle) {
  if (par_variables.mpi_rank == 0) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    cartesian_mesh_2d mesh = *par_variables.spatial_mesh;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icx_param = ICX_PARAM_2D(ncx, ncy);
    const int icy_param = ICY_PARAM_2D(ncx, ncy);
    int i_cell, ic_x, ic_y;
    float dx, dy;
    double vx, vy;
    
    char filename_write[4][99];
    FILE* file_write_particles[4];
    long int nb_particles[4];
    int i_proc;
    for (i_proc = 0; i_proc < 4; i_proc++) {
        sprintf(filename_write[i_proc], "initial_particles_%ldkk_rank%d.dat", num_particle / 1000000, i_proc);
        file_write_particles[i_proc] = fopen(filename_write[i_proc], "w");
        fprintf(file_write_particles[i_proc], "%d %d\n", ncx, ncy);
        nb_particles[i_proc] = 0;
    }
    
    // Count the number of particles for each process.
    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell, &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        } else {
            ic_x = ICX_FROM_I_CELL_2D(icx_param, i_cell);
            ic_y = ICY_FROM_I_CELL_2D(icy_param, i_cell);
            for (i_proc = 0; i_proc < 4; i_proc++)
                if (ic_x >= par_variables.layout_2d[i_proc].i_min && ic_x <= par_variables.layout_2d[i_proc].i_max &&
                        ic_y >= par_variables.layout_2d[i_proc].j_min && ic_y <= par_variables.layout_2d[i_proc].j_max)
                    nb_particles[i_proc]++;
        }
    }
    fclose(file_read_particles);
    
    // Write the number of particles for each process.
    for (i_proc = 0; i_proc < 4; i_proc++)
        fprintf(file_write_particles[i_proc], "%ld\n", nb_particles[i_proc]);
    
    // Read the particles and write them in the corresponding file.
    file_read_particles = fopen(filename, "r");
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3)
        exit(EXIT_FAILURE);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell, &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        } else {
            ic_x = ICX_FROM_I_CELL_2D(icx_param, i_cell);
            ic_y = ICY_FROM_I_CELL_2D(icy_param, i_cell);
            for (i_proc = 0; i_proc < 4; i_proc++)
                if (ic_x >= par_variables.layout_2d[i_proc].i_min && ic_x <= par_variables.layout_2d[i_proc].i_max &&
                        ic_y >= par_variables.layout_2d[i_proc].j_min && ic_y <= par_variables.layout_2d[i_proc].j_max)
                    fprintf(file_write_particles[i_proc], "%d %.*g %.*g %.*g %.*g\n", i_cell,
                        FLT_DECIMAL_DIG, dx, FLT_DECIMAL_DIG, dy,
                        DBL_DECIMAL_DIG, vx, DBL_DECIMAL_DIG, vy);
        }
    }
    fclose(file_read_particles);
    
    for (i_proc = 0; i_proc < 4; i_proc++)
        fclose(file_write_particles[i_proc]);
  }
  MPI_Finalize();
  exit(0);
}

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]      par_variables the MPI parameters.
 * @param[in]      sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]      spatial_params.
 * @param[in]      thermal_speed.
 * @param[in, out] num_particle the number of particles.
 * @param[out]     weight.
 * @param[out]     particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of randomized particles.
 */
void create_dd_particle_array_2d(mpi_parameters par_variables,
        unsigned char sim_distrib, double* spatial_params, double* speed_params,
        long int* num_particle, float* weight, bag** particles) {
    size_t i, j;
    int i_cell, ic_x, ic_y;
    double x, y, vx, vy;
    double control_point, evaluated_function;
    speeds_generator_2d speeds_generator = speed_generators_2d[sim_distrib];
    distribution_function_2d distrib_function = distribution_funs_2d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_2d[sim_distrib];
    
    cartesian_mesh_2d mesh = *par_variables.spatial_mesh;
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    long int my_nb_particles = 0;
    for (j = 0; j < *num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        ic_x = (int)x;
        ic_y = (int)y;
        if (ic_x >= par_variables.layout_2d[par_variables.mpi_rank].i_min && ic_x <= par_variables.layout_2d[par_variables.mpi_rank].i_max &&
                ic_y >= par_variables.layout_2d[par_variables.mpi_rank].j_min && ic_y <= par_variables.layout_2d[par_variables.mpi_rank].j_max) {
            (*speeds_generator)(speed_params, &vx, &vy);
            i_cell = COMPUTE_I_CELL_2D(icell_param, ic_x, ic_y);
            bag_push_serial(&((*particles)[i_cell]), (particle){ .dx = (float)(x - ic_x), .dy = (float)(y - ic_y),
                                                                 .vx = vx, .vy = vy }, 0);
            my_nb_particles++;
        }
    }
    MPI_Allreduce(&my_nb_particles, num_particle, 1, MPI_LONG, MPI_SUM, MPI_COMM_WORLD);
    
    *weight = (float)x_range * (float)y_range / ((float)*num_particle);
    
    // Test the initial distribution.
    char filename[99];
    sprintf(filename, "test_particles_2d_rank%d.dat", par_variables.mpi_rank);
    FILE* file_diag_particles = fopen(filename, "w");
    unsigned int nb_particle_in_cell;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
        case TWO_STREAM_1D_PROJ2D:
            for (i = par_variables.layout_2d[par_variables.mpi_rank].i_min; i <= par_variables.layout_2d[par_variables.mpi_rank].i_max; i++) {
                nb_particle_in_cell = 0;
                for (j = par_variables.layout_2d[par_variables.mpi_rank].j_min; j <= par_variables.layout_2d[par_variables.mpi_rank].j_max; j++)
                    nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)]));
                fprintf(file_diag_particles, "%ld %d\n", i, nb_particle_in_cell);
            }
            break;
        default:
            for (i = par_variables.layout_2d[par_variables.mpi_rank].i_min; i <= par_variables.layout_2d[par_variables.mpi_rank].i_max; i++)
                for (j = par_variables.layout_2d[par_variables.mpi_rank].j_min; j <= par_variables.layout_2d[par_variables.mpi_rank].j_max; j++)
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)])));
    }
    fclose(file_diag_particles);
}

/*
 * Write the num_particle particles in a file, in ASCII.
 *
 * @param[in] par_variables the MPI parameters.
 * @param[in] num_particle the number of particles.
 * @param[in] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of randomized particles.
 */
void write_ascii_dd_particle_array_2d(mpi_parameters par_variables,
        long int num_particle, bag* particles) {
    size_t i, j;
    cartesian_mesh_2d mesh = *par_variables.spatial_mesh;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int num_cells_2d = ncx * ncy;
    char filename[99];
    bag* my_bag;
    chunk* my_chunk;
    
    sprintf(filename, "initial_particles_%ldkk_rank%d.dat", num_particle / 1000000, par_variables.mpi_rank);
    FILE* file_write_particles = fopen(filename, "w");
    fprintf(file_write_particles, "%d %d\n", ncx, ncy);
    fprintf(file_write_particles, "%ld\n", num_particle);
    for (j = 0; j < num_cells_2d; j++) {
        my_bag = &(particles[j]);
        for (my_chunk = my_bag->front; my_chunk; my_chunk = my_chunk->next) {
            for (i = 0; i < my_chunk->size; i++) {
                fprintf(file_write_particles, "%ld %.*g %.*g %.*g %.*g\n", j,
                 FLT_DECIMAL_DIG, my_chunk->array[i].dx, FLT_DECIMAL_DIG, my_chunk->array[i].dy,
                 DBL_DECIMAL_DIG, my_chunk->array[i].vx, DBL_DECIMAL_DIG, my_chunk->array[i].vy);
            }
        }
    }
    fclose(file_write_particles);
}

