#include <omp.h>                   // functions omp_get_num_threads, omp_get_thread_num
#include <stdio.h>                 // functions fprintf, fopen, fclose, fscanf, sprintf
                                   // constant  stderr (standard error output stream)
#include <stdlib.h>                // functions exit (error handling), malloc, free ((de)allocate memory)
                                   // constant  EXIT_FAILURE (error handling)
                                   // type      size_t
#include "initial_distributions.h" // types     speeds_generator_3d, distribution_function_3d, max_distribution_function
                                   // variables speed_generators_3d, distribution_funs_3d, distribution_maxs_3d
#include "matrix_functions.h"      // functions allocate_aligned_int_matrix, deallocate_int_matrix
#include "meshes.h"                // type      cartesian_mesh_3d
#include "particle_type_aos_3d.h"  // type      particle_sorter_oop_3d
#include "random.h"                // function  pic_vert_next_random_double
#include "space_filling_curves.h"  // macros    COMPUTE_I_CELL_3D, I_CELL_PARAM1_3D, I_CELL_PARAM2_3D

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[num_particle] newly allocated array of particles read from file.
 */
void read_particle_arrays_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        float* weight, particle_3d** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y, throw_that_number_z;
    unsigned int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int ncz = mesh.num_cell_z;
    
    sprintf(filename, "initial_particles_%dkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    if (fscanf(file_read_particles, "%d %d %d %u", &throw_that_number_x, &throw_that_number_y, &throw_that_number_z,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy ncz num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_z != ncz) {
        fprintf(stderr, "I expected ncz = %d but found %d in the input file.\n", ncz, throw_that_number_z);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %d but found %d in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) * (float)(mesh.z_max - mesh.z_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %f %lf %lf %lf", &(*particles)[i].i_cell,
                &(*particles)[i].dx, &(*particles)[i].dy, &(*particles)[i].dz,
                &(*particles)[i].vx, &(*particles)[i].vy, &(*particles)[i].vz) < 7) {
            fprintf(stderr, "I expected %d particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
    }
    fclose(file_read_particles);
}

/*
 * Initializes arrays of num_particle random particles following a given distribution
 * for positions, and following the gaussian distribution for speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[num_particle] newly allocated array of randomized particles.
 */
void create_particle_arrays_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        particle_3d** particles) {
    size_t j;
    double x, y, z, v_x, v_y, v_z;
    double control_point, evaluated_function;
    speeds_generator_3d speeds_generator = speed_generators_3d[sim_distrib];
    distribution_function_3d distrib_function = distribution_funs_3d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_3d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const double z_range = mesh.z_max - mesh.z_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int ncz = mesh.num_cell_z;
    const int icell_param1 = I_CELL_PARAM1_3D(ncx, ncy, ncz);
    const int icell_param2 = I_CELL_PARAM2_3D(ncx, ncy, ncz);
    
    *weight = (float)x_range * (float)y_range * (float)z_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            z = z_range * pic_vert_next_random_double() + mesh.z_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y, z);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        z = (z - mesh.z_min) / mesh.delta_z;
        (*speeds_generator)(speed_params, &v_x, &v_y, &v_z);
        (*particles)[j] = (particle_3d) {
            .i_cell = COMPUTE_I_CELL_3D(icell_param1, icell_param2, (int)x, (int)y, (int)z),
            .dx     = (float)(x - (int)x),
            .vx     = v_x,
            .dy     = (float)(y - (int)y),
            .vy     = v_y,
            .dz     = (float)(z - (int)z),
            .vz     = v_z};
    }
    
#ifdef PIC_VERT_TEST_INITIAL_DISTRIBUTION
    // Test the initial distribution.
    size_t i, k;
    FILE* file_diag_particles = fopen("test_particles_3d.dat", "w");
    unsigned int num_cells_3d = ncx * ncy * ncz;
    unsigned int nb_particle_in_cell;
    unsigned int* nb_particle_per_cell = malloc(num_cells_3d * sizeof(unsigned int));
    for (i = 0; i < num_cells_3d; i++)
        nb_particle_per_cell[i] = 0;
    for (i = 0; i < num_particle; i++)
        nb_particle_per_cell[(*i_cell)[i]]++;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ3D:
            for (i = 0; i < ncx; i++) {
                nb_particle_in_cell = 0;
                for (j = 0; j < ncy; j++)
                    for (k = 0; k < ncz; k++)
                        nb_particle_in_cell += nb_particle_per_cell[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)];
                fprintf(file_diag_particles, "%ld %d\n", i, nb_particle_in_cell);
            }
            break;
        case LANDAU_2D_PROJ3D:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++) {
                    nb_particle_in_cell = 0;
                    for (k = 0; k < ncz; k++)
                        nb_particle_in_cell += nb_particle_per_cell[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)];
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, nb_particle_in_cell);
                }
            break;
        default:
            for (k = 0; k < ncz; k++) {
                for (i = 0; i < ncx; i++)
                    for (j = 0; j < ncy; j++)
                        fprintf(file_diag_particles, "%ld %ld %ld %d\n", i, j, k, nb_particle_per_cell[COMPUTE_I_CELL_3D(icell_param1, icell_param2, i, j, k)]);
                fprintf(file_diag_particles, "\n\n");
            }
    }
    free(nb_particle_per_cell);
    fclose(file_diag_particles);
#endif
}



/*****************************************************************************
 *                           Sorting functions                               *
 *                     (by increasing values of i_cell)                      *
 *****************************************************************************/

/*
 * @param[in] num_particle, number of particles in the array to sort.
 * @param[in] num_cell, number of cells in the mesh (number of different values for i_cell).
 * @return    a particle sorter.
 */
particle_sorter_oop_3d new_particle_sorter_oop_3d(unsigned int num_particle, unsigned int num_cell) {
    int num_threads;

    particle_sorter_oop_3d sorter;
    sorter.num_particle = num_particle;
    sorter.num_cell = num_cell;
    #pragma omp parallel
    num_threads = omp_get_num_threads();
    sorter.num_particle_per_cell = allocate_aligned_int_matrix(num_threads, num_cell);
    sorter.index_next_particle   = allocate_aligned_int_matrix(num_threads, num_cell);
    sorter.particles_tmp = malloc(num_particle * sizeof(particle_3d));
    if (!sorter.particles_tmp) {
        fprintf(stderr, "malloc failed to initialize particles_tmp.\n");
        exit(EXIT_FAILURE);
    }
    return sorter;
}

/*
 * Sort the array of particles (with temporary arrays).
 *
 * @param[in, out] sorter a pointer on the particle_sorter_oop for this array (has to be initialized
 *                 before the call).
 * @param[in, out] particles a pointer on the [num_particle] array of particles to sort.
 */
void sort_particles_oop_3d(particle_sorter_oop_3d* sorter, particle_3d** particles) {
    size_t i, k;
    int thread_id, num_threads;
    particle_3d* particle_array_tmp;
    
    #pragma omp parallel private(thread_id, i, k)
    {
        num_threads = omp_get_num_threads();
        thread_id = omp_get_thread_num();
        
        // Count how many particles are in each cell.
        for (i = 0; i < sorter->num_cell; i++)
            sorter->num_particle_per_cell[thread_id][i] = 0;
        
        #pragma omp for schedule(static)
        for (i = 0; i < sorter->num_particle; i++)
            sorter->num_particle_per_cell[thread_id][(*particles)[i].i_cell]++;
        
        // The implicit barrier makes sure that all the threads updated num_particle_per_cell before the prefix scan.
        
        // At the beginning, no particle is supposed to be in its right place. So for each cell,
        // the index in which to put the next particle is sum_{k = 0}^{cell-1} num_particle_per_cell[k]
        sorter->index_next_particle[thread_id][0] = 0;
        for (i = 0; i < thread_id; i++)
            sorter->index_next_particle[thread_id][0] += sorter->num_particle_per_cell[i][0];
        for (k = 1; k < sorter->num_cell; k++) {
            sorter->index_next_particle[thread_id][k] = sorter->index_next_particle[thread_id][k - 1];
            for (i = thread_id; i < num_threads; i++)
                sorter->index_next_particle[thread_id][k] += sorter->num_particle_per_cell[i][k - 1];
            for (i = 0; i < thread_id; i++)
                sorter->index_next_particle[thread_id][k] += sorter->num_particle_per_cell[i][k];
        }
        
        // For each particle, put it into its right place in the particles_tmp array,
        // (at the index given by index_next_particle), and increment index_next_particle.
        // Schedule static ensures that this loop is divided among threads as the previous omp loop.
        #pragma omp for schedule(static)
        for (i = 0; i < sorter->num_particle; i++)
            sorter->particles_tmp[sorter->index_next_particle[thread_id][(*particles)[i].i_cell]++] = (*particles)[i];
    }
    
    particle_array_tmp    = *particles;
    *particles            = sorter->particles_tmp;
    sorter->particles_tmp = particle_array_tmp;
}

void free_particle_sorter_oop_3d(particle_sorter_oop_3d sorter, unsigned int num_cell) {
    int num_threads;

    #pragma omp parallel
    num_threads = omp_get_num_threads();
    deallocate_int_matrix(sorter.num_particle_per_cell, num_threads, num_cell);
    deallocate_int_matrix(sorter.index_next_particle,   num_threads, num_cell);
    free(sorter.particles_tmp);
}

