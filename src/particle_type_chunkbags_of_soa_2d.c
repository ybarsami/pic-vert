#include <omp.h>                               // function  omp_get_num_threads
#include <stdio.h>                             // functions fprintf, fopen, fclose, fscanf, sprintf
                                               // constant  stderr (standard error output stream)
#include <stdlib.h>                            // functions exit (error handling), malloc, free ((de)allocate memory)
                                               // constant  EXIT_FAILURE (error handling)
                                               // type      size_t
#include "initial_distributions.h"             // types     speeds_generator_2d, distribution_function_2d, max_distribution_function
                                               // variables speed_generators_2d, distribution_funs_2d, distribution_maxs_2d
#include "matrix_functions.h"                  // function  allocate_aligned_int_matrix
#include "meshes.h"                            // type      cartesian_mesh_2d
#include "particle_type_chunkbags_of_soa_2d.h" // types     chunk, bag
                                               // variables free_chunks, free_index, FREELIST_SIZE
#include "random.h"                            // function  pic_vert_next_random_double
#include "space_filling_curves.h"              // macros    COMPUTE_I_CELL_2D, I_CELL_PARAM_2D

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |       +—————————————————————————————————————+
 *           |                                             |
 *           |     +——————+     +— ...     ——+     +————+  |
 *           |     |      |     |            |     |    |  |
 *           v     |      v     |            v     |    v  v
 *       |———————| |  |———————| |        |———————| |  |———————|
 *       |       | |  |       | |        |       | |  |       |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *       |       |    |       |          |       |    |       |
 *  size |  16   |    |  11   |          |  13   |    |  17   |
 *       |       |    |       |          |       |    |       |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *     0 |       |    |       |          |       |    |       |
 *     1 |       |    |       |          |       |    |       |
 *     2 |       |    |       |          |       |    |       |
 *     3 |       |    |       |          |       |    |       |
 *     4 |       |    |       |          |       |    |       |
 *     5 |       |    |       |          |       |    |       |
 *     6 |       |    |       |          |       |    |       |
 *     7 |       |    |       |          |       |    |       |
 *     8 |       |    |       |          |       |    |       |
 *     9 |       |    |       |          |       |    |       |
 *    10 |       |    |       |          |       |    |       |
 *    11 |       |    |   ?   |          |       |    |       |
 *    12 |       |    |   ?   |          |       |    |       |
 *    13 |       |    |   ?   |          |   ?   |    |       |
 *    14 |       |    |   ?   |          |   ?   |    |       |
 *    15 |       |    |   ?   |          |   ?   |    |       |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |
 *       |———————|    |———————|    ...   |———————|    |———————|
 *
 */

/*
 *         y-axis
 *            ^
 *            |
 *      y_max |———————|———————|   ...               ...   |———————|
 *            |       |  2 *  |                           |  ncy  |
 *            |ncx - 1|  ncx  |                           |* ncx  |
 *            |       |  - 1  |                           |  - 1  |
 *            |———————|———————|   ...               ...   —————————
 *            .               .                                   .
 *            .               .                                   .
 *            .               .                                   .
 *                                    dx*delta_x
 *            |                         <——>
 *            |           ...           |———————|                 .
 *            |                         |       |                 .
 *          y +-------------------------|--*    | ^               .
 *            |                         |  .    | | dy*delta_y
 *            |           ...           |———————| v
 *            |                            .     
 *            .               .            .              .       .
 *            .               .            .              .       .
 *            .               .            .              .       .
 *            |———————|———————|            .              |———————| ^
 *            |       |       |            .              |(ncy-1)| |
 *            |   1   |ncx + 1|            .              |* ncx  | | delta_y
 *            |       |       |            .              |  + 1  | |
 *            |———————|———————|   ...      .        ...   ————————— v
 *            |       |       |            .              |(ncy-1)|
 *            |   0   |  ncx  |            .              |* ncx  |
 *            |       |       |            .              |       |
 *      y_min +———————|———————|   ...   ———+—————   ...   —————————————> x-axis
 *          x_min     <———————>            x                    x_max
 *                     delta_x
 *
 * In the physical world, the particle has x in [x_min ; x_max [
 *                                         y in [y_min ; y_max [
 * 
 * This is mapped to a grid of size ncx * ncy. Thus, we have :
 *     delta_x = (x_max - x_min) / ncx;
 *     delta_y = (y_max - y_min) / ncy;
 * 
 * If we call :
 *     x_mapped = (x - x_min) / delta_x which is in [0 ; ncx [
 *     y_mapped = (y - y_min) / delta_y which is in [0 ; ncy [
 *     index_x = floor(x_mapped), the number of times we have to move
 *         by delta_x from the start of the mesh to the particle on the x-axis
 *     index_y = floor(y_mapped), the number of times we have to move
 *         by delta_y from the start of the mesh to the particle on the y-axis
 * Then i_cell, the cell index (given inside the cells above), is computed by :
 *     i_cell = index_x * ncy + index_y;
 */

chunk*** free_chunks;

/*
 * Fills free_chunks with as many chunks as possible and sets free_index accordingly.
 */
void init_freelists(long int num_particle) {
  int num_threads;
  #pragma omp parallel
  num_threads = omp_get_num_threads();
  FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads;
  free_chunks = malloc(num_threads * sizeof(chunk**));
  free_index = allocate_aligned_int_matrix(num_threads, 1);
  for (size_t i = 0; i < num_threads; i++) {
    // The freelists could be aligned to the cache size, but this doesn't matter:
    // their size ensure that there won't be any false sharing.
    free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
    for (size_t j = 0; j < FREELIST_SIZE; j++)
      free_chunks[i][j] = malloc(sizeof(chunk));
    FREE_INDEX(i) = FREELIST_SIZE;
  }
}

chunk* chunk_alloc(int thread_id) {
  if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
    return (chunk*) malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
  if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the front of a chunk bag.
 * We have to set the field "size" to 0, because:
 *   1. When a chunk goes back to a freelist, this field is not reset.
 *   2. When a chunk is newly allocated, this field is not set neither.
 * It is simpler to set to 0 in this lone function, rather than
 * both in the allocation and in the free functions.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 * @return         the newly allocated chunk.
 */
chunk* new_back_chunk(bag* b, int thread_id) {
  chunk* c = chunk_alloc(thread_id);
  c->size = 0;
  c->next = (void*)0;
  b->back = c;
  return c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 *
 *           Chunk bag
 *       { front   back  };
 *       |———————|———————|
 *       |       |       |
 *       |   X   |   X   |
 *       |   |   |   |   |
 *       |———+———|———+———|
 *           |       |
 *           |  +————+
 *           |  |
 *           v  v
 *        |———————|
 *        |       |
 *   next |   X———+——>NIL
 *        |       |
 *        |———————|
 *        |       |
 *   size |   0   |
 *        |       |
 *        |———————|
 *      0 |   ?   |
 *      1 |   ?   |
 *      2 |   ?   |
 *      3 |   ?   |
 *      4 |   ?   |
 *      5 |   ?   |
 *      6 |   ?   |
 *      7 |   ?   |
 *      8 |   ?   |
 *      9 |   ?   |
 *     10 |   ?   |
 *     11 |   ?   |
 *     12 |   ?   |
 *     13 |   ?   |
 *     14 |   ?   |
 *     15 |   ?   |
 *     16 |   ?   |
 *     17 |   ?   |
 *     18 |   ?   |
 *     19 |   ?   |
 *        |———————|
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int thread_id) {
  chunk* c = new_back_chunk(b, thread_id);
  b->front = c;
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c) {
    size += c->size;
    c = c->next;
  }
  return size;
}

/* 
 * Merge other into b; other becomes empty.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int thread_id) {
  b->back->next = other->front;
  b->back       = other->back;
  bag_init(other, thread_id);
}

/* 
 * Add a particle into a chunk bag. Add it into the last chunk,
 * unless this chunk is full. In that case, allocate a new chunk
 * before adding it into that new chunk.
 *
 * @param[in, out] b
 * @param[in]      dx, dy, vx, vy
 */
void bag_push(bag* b, float dx, float dy, double vx, double vy, int thread_id) {
  if (b->back->size == CHUNK_SIZE) {
    chunk* old_back = b->back;
    chunk* c = new_back_chunk(b, thread_id);
    old_back->next = c;
  }
  b->back->dx[b->back->size] = dx;
  b->back->dy[b->back->size] = dy;
  b->back->vx[b->back->size] = vx;
  b->back->vy[b->back->size] = vy;
  b->back->size++;
}

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy;

    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell,
                &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        bag_push(&((*particles)[i_cell]), dx, dy, vx, vy, 0);
    }
    fclose(file_read_particles);
}

/*
 * Return an array of num_particle random particles following a given distribution
 * for positions, and following the gaussian distribution for speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of randomized particles.
 */
void create_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t i, j;
    int i_cell;
    double x, y, vx, vy;
    double control_point, evaluated_function;
    speeds_generator_2d speeds_generator = speed_generators_2d[sim_distrib];
    distribution_function_2d distrib_function = distribution_funs_2d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_2d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    *weight = (float)x_range * (float)y_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        speeds_generator(speed_params, &vx, &vy);
        i_cell = COMPUTE_I_CELL_2D(icell_param, (int)x, (int)y);
        bag_push(&((*particles)[i_cell]), (float)(x - (int)x), (float)(y - (int)y), vx, vy, 0);
    }
    
    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_2d.dat", "w");
    long int nb_particle_in_cell;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
        case TWO_STREAM_1D_PROJ2D:
            for (i = 0; i < ncx; i++) {
                nb_particle_in_cell = 0;
                for (j = 0; j < ncy; j++)
                    nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)]));
                fprintf(file_diag_particles, "%ld %ld\n", i, nb_particle_in_cell);
            }
            break;
        default:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++)
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)])));
    }
    fclose(file_diag_particles);
}

