#include <omp.h>                               // function  omp_get_num_threads
#include <stdio.h>                             // functions fprintf, fopen, fclose, fscanf, sprintf
                                               // constant  stderr (standard error output stream)
#include <stdlib.h>                            // functions exit (error handling), malloc, free ((de)allocate memory)
                                               // constant  EXIT_FAILURE (error handling)
                                               // type      size_t
#include "initial_distributions.h"             // types     speeds_generator_2d, distribution_function_2d, max_distribution_function
                                               // variables speed_generators_2d, distribution_funs_2d, distribution_maxs_2d
#include "matrix_functions.h"                  // function  allocate_aligned_int_matrix
#include "meshes.h"                            // type      cartesian_mesh_2d
#include "particle_type_chunkbags_of_aos_2d.h" // types     particle, chunk, bag
                                               // variables free_chunks, free_index, FREELIST_SIZE
#include "random.h"                            // function  pic_vert_next_random_double
#include "space_filling_curves.h"              // macros    COMPUTE_I_CELL_2D, I_CELL_PARAM_2D

/*****************************************************************************
 *                              Chunk bags                                   *
 *****************************************************************************/

/*
 * In this example, CHUNK_SIZE = 20.
 *
 * ? in the array means allocated space for the array, not filled with elements.
 *
 * ? in the size means that the size is not yet computed for the last chunk.
 *
 *                    Chunk bag
 *       { front   back    b_head  b_end };
 *       |———————|———————|———————|———————|
 *       |       |       |       |       |
 *       |   X   |   X   |   X   |   X   |
 *       |   |   |   |   |   |   |   |   |
 *       |———+———|———+———|———+———|———+———|
 *           |       |       |       |
 *           |       |       |       +——————————————————————————————————————+
 *           |       |       |                                              |
 *           |       |       +—————————————————————————————————————————+    |
 *           |       |                                                 |    |
 *           |       +—————————————————————————————————————+           |    |
 *           |                                             |           |    |
 *           |     +——————+     +— ...     ——+     +————+  |           |    |
 *           |     |      |     |            |     |    |  |           |    |
 *           v     |      v     |            v     |    v  v           |    |
 *       |———————| |  |———————| |        |———————| |  |———————|        |    |
 *       |       | |  |       | |        |       | |  |       |        |    |
 *  next |   X———+—+  |   X———+—+        |   X———+—+  |   X———+——>NIL  |    |
 *       |       |    |       |          |       |    |       |        |    |
 *       |———————|    |———————|    ...   |———————|    |———————|        |    |
 *       |       |    |       |          |       |    |       |        |    |
 *  size |  16   |    |  11   |          |  13   |    |   ?   |        |    |
 *       |       |    |       |          |       |    |       |        |    |
 *       |———————|    |———————|    ...   |———————|    |———————|        |    |
 *     0 |       |    |       |          |       |    |       |        |    |
 *     1 |       |    |       |          |       |    |       |        |    |
 *     2 |       |    |       |          |       |    |       |        |    |
 *     3 |       |    |       |          |       |    |       |        |    |
 *     4 |       |    |       |          |       |    |       |        |    |
 *     5 |       |    |       |          |       |    |       |        |    |
 *     6 |       |    |       |          |       |    |       |        |    |
 *     7 |       |    |       |          |       |    |       |        |    |
 *     8 |       |    |       |          |       |    |       |        |    |
 *     9 |       |    |       |          |       |    |       |        |    |
 *    10 |       |    |       |          |       |    |       |        |    |
 *    11 |       |    |   ?   |          |       |    |       |        |    |
 *    12 |       |    |   ?   |          |       |    |       |        |    |
 *    13 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    14 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    15 |       |    |   ?   |          |   ?   |    |       |        |    |
 *    16 |   ?   |    |   ?   |          |   ?   |    |       |        |    |
 *    17 |   ?   |    |   ?   |          |   ?   |    |   ?   |<———————+    |
 *    18 |   ?   |    |   ?   |          |   ?   |    |   ?   |             |
 *    19 |   ?   |    |   ?   |          |   ?   |    |   ?   |             |
 *       |———————|    |———————|    ...   |———————|    |———————|<————————————+
 *
 */

chunk*** free_chunks;

/*
 * Fills free_chunks with as many chunks as possible and sets free_index accordingly.
 */
void init_freelists(long int num_particle) {
  int num_threads;
  #pragma omp parallel
  num_threads = omp_get_num_threads();
  FREELIST_SIZE = num_particle / CHUNK_SIZE / num_threads;
  free_chunks = malloc(num_threads * sizeof(chunk**));
  free_index = allocate_aligned_int_matrix(num_threads, 1);
  for (size_t i = 0; i < num_threads; i++) {
    // The freelists could be aligned to the cache size, but this doesn't matter:
    // their size ensure that there won't be any false sharing.
    free_chunks[i] = malloc(FREELIST_SIZE * sizeof(chunk*));
    for (size_t j = 0; j < FREELIST_SIZE; j++)
      free_chunks[i][j] = malloc(sizeof(chunk));
    FREE_INDEX(i) = FREELIST_SIZE;
  }
}

chunk* chunk_alloc(int thread_id) {
  if (FREE_INDEX(thread_id) > 0) {
    return free_chunks[thread_id][--FREE_INDEX(thread_id)];
  } else {
    return (chunk*) malloc(sizeof(chunk));
  }
}

void chunk_free(chunk* c, int thread_id) {
  if (FREE_INDEX(thread_id) < FREELIST_SIZE) {
    free_chunks[thread_id][FREE_INDEX(thread_id)++] = c;
  } else {
    free(c);
  }
}

/*
 * Allocate a new chunk and put it at the back
 * of a chunk bag.
 *
 * @param[in, out] b the bag in which to put the new chunk.
 * @return         the newly allocated chunk.
 */
chunk* new_back_chunk(bag* b, int thread_id) {
  chunk* c = chunk_alloc(thread_id);
  // c->size needs not be initialized
  c->next = (void*)0;
  b->back = c;
  b->back_head = &(c->array[0]);
  b->back_end  = &(c->array[CHUNK_SIZE]);
  return c;
}

/*
 * Initialize a bag (already allocated) with only one empty chunk.
 *
 * In this example, CHUNK_SIZE = 20.
 * The array is filled with ? because it's not filled yet.
 * The size of the array is of course 0, but it will not be maintained,
 * so we don't compute it.
 *
 *                    Chunk bag
 *       { front   back    b_head  b_end };
 *       |———————|———————|———————|———————|
 *       |       |       |       |       |
 *       |   X   |   X   |   X   |   X   |
 *       |   |   |   |   |   |   |   |   |
 *       |———+———|———+———|———+———|———+———|
 *           |       |       |       |
 *           |  +————+       |       |
 *           |  |            |       |
 *           v  v            |       |
 *        |———————|          |       |
 *        |       |          |       |
 *   next |   X———+——>NIL    |       |
 *        |       |          |       |
 *        |———————|          |       |
 *        |       |          |       |
 *   size |   ?   |          |       |
 *        |       |          |       |
 *        |———————|          |       |
 *      0 |   ?   |<—————————+       |
 *      1 |   ?   |                  |
 *      2 |   ?   |                  |
 *      3 |   ?   |                  |
 *      4 |   ?   |                  |
 *      5 |   ?   |                  |
 *      6 |   ?   |                  |
 *      7 |   ?   |                  |
 *      8 |   ?   |                  |
 *      9 |   ?   |                  |
 *     10 |   ?   |                  |
 *     11 |   ?   |                  |
 *     12 |   ?   |                  |
 *     13 |   ?   |                  |
 *     14 |   ?   |                  |
 *     15 |   ?   |                  |
 *     16 |   ?   |                  |
 *     17 |   ?   |                  |
 *     18 |   ?   |                  |
 *     19 |   ?   |                  |
 *        |———————|<—————————————————+
 *
 * @param[in, out] b the bag to initialize.
 */
void bag_init(bag* b, int thread_id) {
  chunk* c = new_back_chunk(b, thread_id);
  b->front = c;
}

/*
 * Compute the number of elements stored into the last
 * chunk of a chunk bag.
 * Uses pointer arithmetic.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b->back.
 */
int bag_back_size(bag* b) {
  return CHUNK_SIZE - (b->back_end - b->back_head);
}

/*
 * Compute the number of elements stored into all
 * chunks of a chunk bag.
 *
 * @param[in] b the bag.
 * @return    the number of elements stored into b.
 */
int bag_size(bag* b) {
  chunk* c = b->front;
  int size = 0;
  while (c != b-> back) {
    size += c->size;
    c = c->next;
  }
  return size + bag_back_size(b);
}

/*
 * Compute the number of elements stored into the last
 * chunk of a chunk bag, and store this number inside
 * b->back->size.
 *
 * @param[in] b the bag.
 */
void bag_store_size_of_back(bag* b) {
  b->back->size = bag_back_size(b);
}

/* 
 * Merge other into b; other becomes empty.
 *
 * @param[in, out] b
 * @param[in, out] other
 */
void bag_append(bag* b, bag* other, int thread_id) {
  bag_store_size_of_back(b);
  b->back->next = other->front;
  b->back       = other->back;
  b->back_end   = other->back_end;
  b->back_head  = other->back_head;
  bag_init(other, thread_id);
}

/* 
 * Add a particle into a chunk bag. Add it into the last chunk,
 * unless this chunk is full. In that case, allocate a new chunk
 * before adding it into that new chunk.
 *
 * @param[in, out] b
 * @param[in]      p
 */
void bag_push(bag* b, particle p, int thread_id) {
  if (b->back_head == b->back_end) {
    chunk* old_back = b->back;
    chunk* c = new_back_chunk(b, thread_id);
    old_back->size = CHUNK_SIZE;
    old_back->next = c;
  }
  *(b->back_head) = p;
  b->back_head++;
}

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of particles read from file.
 */
void read_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        float* weight, bag** particles) {
    size_t i;
    char filename[99];
    int throw_that_number_x, throw_that_number_y;
    long int throw_that_number_also;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    int i_cell;
    float dx, dy;
    double vx, vy;
    
    sprintf(filename, "initial_particles_%ldkk.dat", num_particle / 1000000);
    FILE* file_read_particles = fopen(filename, "r");
    if (!file_read_particles) { // Error in file opening
        fprintf(stderr, "%s doesn't exist.\n", filename);
        exit(EXIT_FAILURE);
    }
    // WARNING : if you read num_particle from a file, the particle loop has to begin with #pragma omp simd,
    // else the compiler will not vectorize it because it doesn't know the loop count at compile time !
    if (fscanf(file_read_particles, "%d %d %ld", &throw_that_number_x, &throw_that_number_y,
            &throw_that_number_also) < 3) {
        fprintf(stderr, "%s should begin with ncx ncy num_particle.\n", filename);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_x != ncx) {
        fprintf(stderr, "I expected ncx = %d but found %d in the input file.\n", ncx, throw_that_number_x);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_y != ncy) {
        fprintf(stderr, "I expected ncy = %d but found %d in the input file.\n", ncy, throw_that_number_y);
        exit(EXIT_FAILURE);
    }
    if (throw_that_number_also != num_particle) {
        fprintf(stderr, "I expected num_particle = %ld but found %ld in the input file.\n", num_particle, throw_that_number_also);
        exit(EXIT_FAILURE);
    }
    *weight = (float)(mesh.x_max - mesh.x_min) * (float)(mesh.y_max - mesh.y_min) / ((float)mpi_world_size * (float)num_particle);
    for (i = 0; i < num_particle; i++) {
        if (fscanf(file_read_particles, "%d %f %f %lf %lf", &i_cell,
                &dx, &dy, &vx, &vy) < 5) {
            fprintf(stderr, "I expected %ld particles but there are less in the input file.\n", num_particle);
            exit(EXIT_FAILURE);
        }
        bag_push(&((*particles)[i_cell]), (particle){ .dx = dx, .dy = dy,
                                                      .vx = vx, .vy = vy }, 0);
    }
    fclose(file_read_particles);
}

/*
 * Return an array of num_particle random particles following a given distributions
 * for positions and speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the array (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[mesh.ncx * mesh.ncy] a newly allocated array of chunkbags of randomized particles.
 */
void create_particle_array_2d(int mpi_world_size, long int num_particle, cartesian_mesh_2d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        bag** particles) {
    size_t i, j;
    int i_cell;
    double x, y, vx, vy;
    double control_point, evaluated_function;
    speeds_generator_2d speeds_generator = speed_generators_2d[sim_distrib];
    distribution_function_2d distrib_function = distribution_funs_2d[sim_distrib];
    max_distribution_function max_distrib_function = distribution_maxs_2d[sim_distrib];
    
    const double x_range = mesh.x_max - mesh.x_min;
    const double y_range = mesh.y_max - mesh.y_min;
    const int ncx = mesh.num_cell_x;
    const int ncy = mesh.num_cell_y;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    
    *weight = (float)x_range * (float)y_range / ((float)mpi_world_size * (float)num_particle);
    
    for (j = 0; j < num_particle; j++) {
        do {
            x = x_range * pic_vert_next_random_double() + mesh.x_min;
            y = y_range * pic_vert_next_random_double() + mesh.y_min;
            control_point = (*max_distrib_function)(spatial_params) * pic_vert_next_random_double();
            evaluated_function = (*distrib_function)(spatial_params, x, y);
        } while (control_point > evaluated_function);
        x = (x - mesh.x_min) / mesh.delta_x;
        y = (y - mesh.y_min) / mesh.delta_y;
        (*speeds_generator)(speed_params, &vx, &vy);
        i_cell = COMPUTE_I_CELL_2D(icell_param, (int)x, (int)y);
        bag_push(&((*particles)[i_cell]), (particle){ .dx = (float)(x - (int)x), .dy = (float)(y - (int)y), .vx = vx, .vy = vy }, 0);
    }
    
    // Test the initial distribution.
    FILE* file_diag_particles = fopen("test_particles_2d.dat", "w");
    long int nb_particle_in_cell;
    switch(sim_distrib) {
        case LANDAU_1D_PROJ2D:
        case TWO_BEAMS_FIJALKOW:
        case TWO_STREAM_1D_PROJ2D:
            for (i = 0; i < ncx; i++) {
                nb_particle_in_cell = 0;
                for (j = 0; j < ncy; j++)
                    nb_particle_in_cell += bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)]));
                fprintf(file_diag_particles, "%ld %ld\n", i, nb_particle_in_cell);
            }
            break;
        default:
            for (i = 0; i < ncx; i++)
                for (j = 0; j < ncy; j++)
                    fprintf(file_diag_particles, "%ld %ld %d\n", i, j, bag_size(&((*particles)[COMPUTE_I_CELL_2D(icell_param, i, j)])));
    }
    fclose(file_diag_particles);
}

