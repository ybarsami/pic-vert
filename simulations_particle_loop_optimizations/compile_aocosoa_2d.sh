#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside ../particle_loop_optimizations_simulations/sim2d_aocosoa_2loops.c

compile_gcc() {
  export OMPI_CC=gcc-6
  mpicc -I../include ../src/matrix_functions.c ../src/meshes.c ../src/output.c ../src/parameter_reader.c ../src/random.c ../src/space_filling_curves.c ../src/diagnostics.c ../src/fields.c ../src/initial_distributions.c ../src/particle_type_chunkbags_of_soa_2d.c ../src/poisson_solvers.c ../src/rho.c ../particle_loop_optimizations_simulations/sim2d_aocosoa_2loops.c -DNB_PROC=$1 -DNB_PARTICLE=50000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wfloat-conversion -o aocosoa_2d-$1thread.out
}

compile_gcc_report_vect() {
  export OMPI_CC=gcc-6
  mpicc -I../include ../src/matrix_functions.c ../src/meshes.c ../src/output.c ../src/parameter_reader.c ../src/random.c ../src/space_filling_curves.c ../src/diagnostics.c ../src/fields.c ../src/initial_distributions.c ../src/particle_type_chunkbags_of_soa_2d.c ../src/poisson_solvers.c ../src/rho.c ../particle_loop_optimizations_simulations/sim2d_aocosoa_2loops.c -DNB_PROC=$1 -DNB_PARTICLE=50000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wfloat-conversion -o aocosoa_2d-$1thread.out -fopt-info-vec-all 2> vect_info.txt
}

compile_icc() {
  export OMPI_CC=icc
  mpicc -I../include ../src/matrix_functions.c ../src/meshes.c ../src/output.c ../src/parameter_reader.c ../src/random.c ../src/space_filling_curves.c ../src/diagnostics.c ../src/fields.c ../src/initial_distributions.c ../src/particle_type_chunkbags_of_soa_2d.c ../src/poisson_solvers.c ../src/rho.c ../particle_loop_optimizations_simulations/sim2d_aocosoa_2loops.c -DNB_PROC=$1 -DNB_PARTICLE=50000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o aocosoa_2d-$1thread.out
}

compile_icc_report_vect() {
  export OMPI_CC=icc
  mpicc -I../include ../src/matrix_functions.c ../src/meshes.c ../src/output.c ../src/parameter_reader.c ../src/random.c ../src/space_filling_curves.c ../src/diagnostics.c ../src/fields.c ../src/initial_distributions.c ../src/particle_type_chunkbags_of_soa_2d.c ../src/poisson_solvers.c ../src/rho.c ../particle_loop_optimizations_simulations/sim2d_aocosoa_2loops.c -DNB_PROC=$1 -DNB_PARTICLE=50000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o aocosoa_2d-$1thread.out -qopt-report=5 -qopt-report-phase=vec
}

#compile_gcc 1
compile_gcc 2

