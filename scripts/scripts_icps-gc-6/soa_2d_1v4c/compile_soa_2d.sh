#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles. Here given for 50M particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations/sim2d_soa_XXX_ipdpsw2017.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/soa_2d_1v4c

compile_icc() {
  mkdir -p "$FOLDER/$1core-$2-$3"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_$2_ipdpsw2017.c -DNB_PARTICLE=50000000 -DI_CELL_2D_TYPE=$3 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-$2-$3/soa_2d-$1thread.out
}

compile_icc_report_vect() {
  mkdir -p "$FOLDER/$1core-$2-$3"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_$2_ipdpsw2017.c -DNB_PARTICLE=50000000 -DI_CELL_2D_TYPE=$3 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-$2-$3/soa_2d-$1thread.out -qopt-report=5 -qopt-report-phase=vec
}

compile_icc 1 4corners ROW_MAJOR
compile_icc 1 4corners TILE
compile_icc 1 1value ROW_MAJOR
compile_icc 1 1value TILE
#compile_icc_report_vect 1 4corners ROW_MAJOR

