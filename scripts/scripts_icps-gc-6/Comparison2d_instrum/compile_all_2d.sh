#!/bin/bash

#Libraries to link          : -lm
#Modify the compile scripts : modify the -DI_CELL_2D_TYPE=       for the type of space-filling curve (TILE, ROW_MAJOR, MORTON, see space_filling_curves.h)
#                             modify the -DNB_ITER_BETWEEN_SORT= for the number of time steps between two successive sorts
#                             modify the -DSTRIP_SIZE=           for a different strip size (From our experiments on Marconi: "In practice, in a 2d code, choosing a strip-size k between 64 and 256 gives similar optimal results.")
#                             modify the -DCHUNK_SIZE= if you want a different chunk size (From our experiments on Marconi: "In practice, in a 2d code, choosing a strip-size k between 128 and 512 gives similar optimal results.")
#Modify other parameters    : modify the other parameters in parameters_2d.txt

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/Comparison2d_instrum

compile() {
  mkdir -p "$FOLDER/vth$1"
  cp parameters_2d_vth$1.txt $FOLDER/vth$1/
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_chunkbags_of_aos_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/instrum_sim2d_aocoaos_4corners.c -DCHUNK_SIZE=256 -DPIC_VERT_CROSSING -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/vth$1/coaos_2d.out
}

#for vth in 1.0 0.1
for vth in 0.1
do
    compile $vth
done

