#!/bin/bash

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/Comparison3d_20threads

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"

run() {
  export OMP_NUM_THREADS=20
  cd $FOLDER/soa_3loops_sm
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./soa_3d.out parameters_3d.txt | tee sortPeriodic-1mpi-10threads.txt
  cd $FOLDER/soa_3loops
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./soa_3d.out parameters_3d.txt | tee sortPeriodic-1mpi-10threads.txt
  cd $FOLDER/soa_1loop
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./soa_3d.out parameters_3d.txt | tee sortPeriodic-1mpi-10threads.txt
#  cd $FOLDER/coaos
#  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./coaos_3d.out parameters_3d.txt | tee sortPeriodic-1mpi-10threads.txt
  cd $FOLDER/cosoa_concurrent
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./cosoa_3d.out parameters_3d.txt | tee sortPeriodic-1mpi-10threads.txt
}

run

