#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/instrum_Switzerland
nb_threads=10
output_file="sortPeriodic-1mpi-${nb_threads}thread.txt"

run() {
  cd $FOLDER/run_$1M
  export OMP_NUM_THREADS=$nb_threads
  mpirun --report-bindings --cpus-per-proc $nb_threads -np 1 ./instrum_aocosoa_3d-concurrent.out parameters_3d_$1M.txt | tee $output_file
}

run 1
run 16
cd $FOLDER
echo -n "" > $output_file
cat $FOLDER/run_1M/$output_file >> $output_file
cat $FOLDER/run_16M/$output_file >> $output_file

