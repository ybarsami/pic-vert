#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations_any_tile_size/instrum_sim3d_aocosoa_concurrent_colored_1array-Xloops.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/instrum_bimaxwellians

compile_icc() {
  export OMPI_CC=icc
  mkdir -p "$FOLDER/proportion$1-drift$2-vth$3"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/instrum_sim3d_aocosoa_concurrent_colored_1array-Xloops.c -DOMP_TILE_SIZE=2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.02 -DPROPORTION_FAST_PARTICLES=$1 -DDRIFT_VELOCITY=$2 -DTHERMAL_SPEED=$3 -DNB_PARTICLE=100000000 -DCHUNK_SIZE=256 -DPIC_VERT_SHARED_BAGS -DPIC_VERT_CROSSING -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/proportion$1-drift$2-vth$3/instrum_aocosoa_3d-concurrent.out
}

#for proportion in 0.02 0.05 0.10
#for proportion in 0.02
#do
#    for drift in 10.0 11.0 12.0 13.0 14.0 15.0
#    do
#        compile_icc $proportion $drift 0.1
#    done
#done

compile_icc 0.00 0.00 0.339
compile_icc 0.02 10.0 0.10
compile_icc 0.02 11.0 0.126
compile_icc 0.02 13.0 0.178
compile_icc 0.02 15.0 0.234
compile_icc 0.02 17.0 0.287
compile_icc 0.02 20.0 0.355
compile_icc 0.02 22.0 0.3585

