#!/bin/bash

#Libraries to link          : -lm
#Modify the compile scripts : modify the -DI_CELL_2D_TYPE=       for the type of space-filling curve (TILE, ROW_MAJOR, MORTON, see space_filling_curves.h)
#                             modify the -DNB_ITER_BETWEEN_SORT= for the number of time steps between two successive sorts
#                             modify the -DSTRIP_SIZE=           for a different strip size (From our experiments on Marconi: "In practice, in a 2d code, choosing a strip-size k between 64 and 256 gives similar optimal results.")
#                             modify the -DCHUNK_SIZE= if you want a different chunk size (From our experiments on Marconi: "In practice, in a 2d code, choosing a strip-size k between 128 and 512 gives similar optimal results.")
#Modify other parameters    : modify the other parameters in parameters_2d.txt

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/Cold_Comparison2d_20threads

compile() {
  mkdir -p "$FOLDER/soa_3loops_sm"
  cp parameters_2d.txt $FOLDER/soa_3loops_sm/
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_4corners_stripmined_jocs2018.c -DI_CELL_2D_TYPE=TILE -DNB_ITER_BETWEEN_SORT=20 -DSTRIP_SIZE=128 -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/soa_3loops_sm/soa_2d.out
  mkdir -p "$FOLDER/soa_3loops"
  cp parameters_2d.txt $FOLDER/soa_3loops/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_4corners_ipdpsw2017.c -DI_CELL_2D_TYPE=TILE -DNB_ITER_BETWEEN_SORT=20 -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/soa_3loops/soa_2d.out
  mkdir -p "$FOLDER/soa_1loop"
  cp parameters_2d.txt $FOLDER/soa_1loop/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_4corners_1loop_ipdpsw2017.c -DI_CELL_2D_TYPE=TILE -DNB_ITER_BETWEEN_SORT=20 -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/soa_1loop/soa_2d.out
  mkdir -p "$FOLDER/coaos"
  cp parameters_2d.txt $FOLDER/coaos/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_chunkbags_of_aos_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_aocoaos_4corners_ppam2017.c -DCHUNK_SIZE=128 -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/coaos/coaos_2d.out
  mkdir -p "$FOLDER/cosoa_concurrent"
  cp parameters_2d.txt $FOLDER/cosoa_concurrent/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_aocosoa_europar2018.c -DCHUNK_SIZE=256 -lfftw3 -lm -O3 -qopenmp -xHost -std=gnu11 -o $FOLDER/cosoa_concurrent/cosoa_2d.out
}

compile

