#!/bin/bash
echo "--- Thermal speed 1 ---" > "results.txt"
for tile in $(seq 2 8)
do
    grep -r "Execution time (total)    : " "colors-10core-size${tile}-vth1.0/sortAlways-1mpi-10thread.txt" > "tmp.txt"
    sed -i 's|Execution time (total)    : ||g' "tmp.txt"
    sed -i 's|s||g' "tmp.txt"
    echo -n "Tile : ${tile} ; Borders : $(expr $tile / 2) ; Timing : " >> "results.txt"
    cat "tmp.txt" >> "results.txt"
done
echo "" >> "results.txt"
echo "--- Thermal speed 2 ---" >> "results.txt"
for tile in $(seq 2 8)
do
    grep -r "Execution time (total)    : " "colors-10core-size${tile}-vth2.0/sortAlways-1mpi-10thread.txt" > "tmp.txt"
    sed -i 's|Execution time (total)    : ||g' "tmp.txt"
    sed -i 's|s||g' "tmp.txt"
    echo -n "Tile : ${tile} ; Borders : $(expr $tile / 2) ; Timing : " >> "results.txt"
    cat "tmp.txt" >> "results.txt"
done
rm "tmp.txt"

