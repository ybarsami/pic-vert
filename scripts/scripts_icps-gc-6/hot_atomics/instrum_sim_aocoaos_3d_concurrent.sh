#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/hot_atomics

run() {
  cd $FOLDER/vth$2
  export OMP_NUM_THREADS=$1
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./instrum_aocoaos_3d-concurrent.out | tee sortPeriodic-1mpi-$1thread.txt
}

for v_th in 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0
do
    run 10 $v_th
done

