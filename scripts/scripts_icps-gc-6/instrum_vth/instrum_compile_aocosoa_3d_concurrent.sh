#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations_any_tile_size/instrum_sim3d_aocosoa_concurrent_colored_1array-Xloops.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/instrum_vth

compile_icc() {
  export OMPI_CC=icc
  mkdir -p "$FOLDER/$1core-size$3-vth$2"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/instrum_sim3d_aocosoa_concurrent_colored_1array-Xloops.c -DNB_PROC=$1 -DOMP_TILE_SIZE=$3 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.05 -DTHERMAL_SPEED=$2 -DNB_PARTICLE=200000000 -DCHUNK_SIZE=256 -DPIC_VERT_SHARED_BAGS -DPIC_VERT_CROSSING -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-size$3-vth$2/instrum_aocosoa_3d-$1thread-concurrent.out
}

for tile_size in 2 4
do
    for v_th in 1.0 2.0 3.0 4.0 5.0 6.0 7.0 8.0
    do
        compile_icc 10 $v_th $tile_size
    done
done

