#!/bin/bash

#Libraries to link for HDF5 : -lsz -lz -lm -lhdf5
#Include path for hdf5.h    : -I/usr/include/hdf5/openmpi
#                             (find your right path by typing "locate hdf5.h" in a terminal, maybe preceded by "updatedb")
#Library path for libhdf5.a : -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi
#                             (find your right path by typing "locate libhdf5.a" in a terminal, maybe preceded by "updatedb")
#Modify the compile scripts : modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations/instrum_sim2d3v_aocosoa_concurrent.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/instrum_electronHole2d3v

compile_icc() {
  export OMPI_CC=icc
  mkdir -p "$FOLDER"
  cp parameters_2d.txt $FOLDER/
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include -I/usr/include/hdf5/openmpi -L/usr/lib/x86_64-linux-gnu/hdf5/openmpi $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/hdf5_io.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_2d3v.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/instrum_sim2d3v_aocosoa_concurrent.c -DOMP_TILE_SIZE=2 -DCHUNK_SIZE=256 -DPIC_VERT_SHARED_BAGS -DPIC_VERT_CROSSING -lfftw3 -lsz -lz -lm -lhdf5 -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/instrum_aocosoa_2d3v-concurrent.out
}

compile_icc

