#!/bin/bash

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/soa_3d_sfc-newsort

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18"

run() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core-$2
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./soa_3d-$1thread.out | tee sortPeriodic-1mpi-$1thread-$2.txt
}

run_arrays() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core-$2-arrays
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./soa_3d-$1thread.out | tee sortPeriodic-1mpi-$1thread-$2.txt
}

run 1 ROW_MAJOR
run 1 MORTON
run 1 TILE8
run 1 TILE4

