#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles. Here given for 100M particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations/sim3d_soa_8corners.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/soa_3d_sfc-newsort

compile_icc() {
  mkdir -p "$FOLDER/$1core-$2"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim3d_soa_8corners.c -DNB_PARTICLE=100000000 -DI_CELL_3D_TYPE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-$2/soa_3d-$1thread.out
}

compile_icc_arrays() {
  mkdir -p "$FOLDER/$1core-$2-arrays"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim3d_soa_8corners.c -DNB_PARTICLE=100000000 -DI_CELL_3D_TYPE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-$2-arrays/soa_3d-$1thread.out
}

compile_icc_tile() {
  mkdir -p "$FOLDER/$1core-TILE$2"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim3d_soa_8corners.c -DNB_PARTICLE=100000000 -DI_CELL_3D_TYPE=TILE -DTILE_SIZE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-TILE$2/soa_3d-$1thread.out
}

compile_icc_tile_arrays() {
  mkdir -p "$FOLDER/$1core-TILE$2-arrays"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim3d_soa_8corners.c -DNB_PARTICLE=100000000 -DI_CELL_3D_TYPE=TILE -DTILE_SIZE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-TILE$2-arrays/soa_3d-$1thread.out
}

compile_icc 1 ROW_MAJOR
compile_icc 1 MORTON
compile_icc_tile 1 8
compile_icc_tile 1 4

