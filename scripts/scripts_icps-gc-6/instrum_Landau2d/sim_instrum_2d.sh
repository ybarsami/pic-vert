#!/bin/bash

PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/instrum_Landau2d

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18"

run() {
  export OMP_NUM_THREADS=10
  cd $FOLDER
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./coaos_2d.out parameters_2d_jocs.txt | tee sortAlways-1mpi-10threads.txt
}

run

