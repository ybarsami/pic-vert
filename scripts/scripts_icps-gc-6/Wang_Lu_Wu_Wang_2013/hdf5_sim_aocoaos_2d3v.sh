#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/Wang_Lu_Wu_Wang_2013

run() {
  cd $FOLDER
  export OMP_NUM_THREADS=$1
#  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocoaos_2d3v-concurrent.out parameters_2d3v.txt | tee sortPeriodic-1mpi-$1thread.txt
  nohup mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocoaos_2d3v-concurrent.out parameters_2d3v.txt > sortPeriodic-1mpi-$1thread.txt &
}

run 10

