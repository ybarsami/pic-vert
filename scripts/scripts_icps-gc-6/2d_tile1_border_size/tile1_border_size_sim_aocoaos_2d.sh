#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/tile1_border_size

run_concurrent() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core-$2border
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocoaos_2d-$1thread-$2border.out | tee sortAlways-1mpi-$1thread-$2border.txt
}

run_concurrent 10 0
run_concurrent 10 1
run_concurrent 10 2
