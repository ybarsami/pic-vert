#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18 20 22 24 26 28 30 32 34 36 38"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/performance_bimaxwellians
nb_threads=10
output_file="sortPeriodic-1mpi-${nb_threads}thread.txt"

run() {
  cd $FOLDER/proportion$1-drift$2-vth$3
  export OMP_NUM_THREADS=$nb_threads
  mpirun --report-bindings --cpus-per-proc $nb_threads -np 1 ./instrum_aocoaos_3d-concurrent.out | tee $output_file
}

run 0.00 0.00 0.339
run 0.02 10.0 0.10
run 0.02 11.0 0.126
run 0.02 13.0 0.178
run 0.02 15.0 0.234
run 0.02 17.0 0.287
run 0.02 20.0 0.355
run 0.02 22.0 0.3585
cd $FOLDER
echo -n "" > $output_file
cat proportion0.00-drift0.00-vth0.339/$output_file >> $output_file
cat proportion0.02-drift10.0-vth0.10/$output_file >> $output_file
cat proportion0.02-drift11.0-vth0.126/$output_file >> $output_file
cat proportion0.02-drift13.0-vth0.178/$output_file >> $output_file
cat proportion0.02-drift15.0-vth0.234/$output_file >> $output_file
cat proportion0.02-drift17.0-vth0.287/$output_file >> $output_file
cat proportion0.02-drift20.0-vth0.355/$output_file >> $output_file
cat proportion0.02-drift22.0-vth0.3585/$output_file >> $output_file

