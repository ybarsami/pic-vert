#!/bin/bash
echo "--- Black and white ---" > "results.txt"
for tile in $(seq 2 8)
do
    for borders in $(seq 0 $(expr $tile / 2))
    do
        grep -r "Execution time (total)    : " "10core-size${tile}-border${borders}/sortAlways-1mpi-10thread.txt" > "tmp.txt"
        sed -i 's|Execution time (total)    : ||g' "tmp.txt"
        sed -i 's|s||g' "tmp.txt"
        echo -n "Tile : ${tile} ; Borders : ${borders} ; Timing : " >> "results.txt"
        cat "tmp.txt" >> "results.txt"
    done
done
echo "" >> "results.txt"
echo "--- 8 colors ---" >> "results.txt"
for tile in $(seq 2 8)
do
    grep -r "Execution time (total)    : " "colors-10core-size${tile}/sortAlways-1mpi-10thread.txt" > "tmp.txt"
    sed -i 's|Execution time (total)    : ||g' "tmp.txt"
    sed -i 's|s||g' "tmp.txt"
    echo -n "Tile : ${tile} ; Borders : $(expr $tile / 2) ; Timing : " >> "results.txt"
    cat "tmp.txt" >> "results.txt"
done
rm "tmp.txt"

