#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations_any_tile_size/sim3d_aocosoa_concurrent_XXX_1array-Xloops.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/3d_any_tile_size

compile_gcc_blacknwhite() {
  export OMPI_CC=gcc-6
  mkdir -p "$FOLDER/$1core-size$2-border$3"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/sim3d_aocosoa_concurrent_1array-Xloops.c -DNB_PROC=$1 -DOMP_TILE_SIZE=$2 -DOMP_TILE_BORDERS=$3 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DNB_PARTICLE=250000000 -DCHUNK_SIZE=128 -lfftw3 -lm -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wfloat-conversion -o $FOLDER/$1core-size$2-border$3/aocosoa_3d-$1thread-concurrent.out
}

compile_icc_blacknwhite() {
  export OMPI_CC=icc
  mkdir -p "$FOLDER/$1core-size$2-border$3"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/sim3d_aocosoa_concurrent_1array-Xloops.c -DNB_PROC=$1 -DOMP_TILE_SIZE=$2 -DOMP_TILE_BORDERS=$3 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DNB_PARTICLE=250000000 -DCHUNK_SIZE=128 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core-size$2-border$3/aocosoa_3d-$1thread-concurrent.out
}

compile_gcc_8colors() {
  export OMPI_CC=gcc-6
  mkdir -p "$FOLDER/colors-$1core-size$2"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/sim3d_aocosoa_concurrent_colored_1array-Xloops.c -DNB_PROC=$1 -DOMP_TILE_SIZE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DNB_PARTICLE=250000000 -DCHUNK_SIZE=128 -lfftw3 -lm -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wfloat-conversion -o $FOLDER/colors-$1core-size$2/aocosoa_3d-$1thread-concurrent.out
}

compile_icc_8colors() {
  export OMPI_CC=icc
  mkdir -p "$FOLDER/colors-$1core-size$2"
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_3d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations_any_tile_size/sim3d_aocosoa_concurrent_colored_1array-Xloops.c -DNB_PROC=$1 -DOMP_TILE_SIZE=$2 -DNCX=64 -DNCY=64 -DNCZ=64 -DDELTA_T=0.1 -DNB_PARTICLE=250000000 -DCHUNK_SIZE=128 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/colors-$1core-size$2/aocosoa_3d-$1thread-concurrent.out
}

compile_icc_blacknwhite 10 8 0
compile_icc_blacknwhite 10 8 1
compile_icc_blacknwhite 10 8 2
compile_icc_blacknwhite 10 8 3
compile_icc_blacknwhite 10 8 4

compile_icc_blacknwhite 10 7 0
compile_icc_blacknwhite 10 7 1
compile_icc_blacknwhite 10 7 2
compile_icc_blacknwhite 10 7 3

compile_icc_blacknwhite 10 6 0
compile_icc_blacknwhite 10 6 1
compile_icc_blacknwhite 10 6 2
compile_icc_blacknwhite 10 6 3

compile_icc_blacknwhite 10 5 0
compile_icc_blacknwhite 10 5 1
compile_icc_blacknwhite 10 5 2

compile_icc_blacknwhite 10 4 0
compile_icc_blacknwhite 10 4 1
compile_icc_blacknwhite 10 4 2

compile_icc_blacknwhite 10 3 0
compile_icc_blacknwhite 10 3 1

compile_icc_blacknwhite 10 2 0
compile_icc_blacknwhite 10 2 1


compile_icc_8colors 10 8
compile_icc_8colors 10 7
compile_icc_8colors 10 6
compile_icc_8colors 10 5
compile_icc_8colors 10 4
compile_icc_8colors 10 3
compile_icc_8colors 10 2

#If you put CHUNK_SIZE=256, not all runs can be performed (32 GB of RAM):
#compile_icc_blacknwhite 10 8 3 (30M part. max)
#compile_icc_blacknwhite 10 8 4 (not enough memory)
#compile_icc_blacknwhite 10 7 3 (not enough memory)
#compile_icc_blacknwhite 10 6 2 (100M part. max)
#compile_icc_blacknwhite 10 6 3 (not enough memory)
#compile_icc_blacknwhite 10 5 2 (not enough memory)
#compile_icc_blacknwhite 10 4 2 (not enough memory)
#compile_icc_blacknwhite 10 3 1 (100M part. max)
#compile_icc_blacknwhite 10 2 1 (not enough memory)

