#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/3d_any_tile_size

run_blacknwhite() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core-size$2-border$3
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocosoa_3d-$1thread-concurrent.out | tee sortAlways-1mpi-$1thread.txt
}

run_8colors() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/colors-$1core-size$2
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocosoa_3d-$1thread-concurrent.out | tee sortAlways-1mpi-$1thread.txt
}

run_blacknwhite 10 8 0
run_blacknwhite 10 8 1
run_blacknwhite 10 8 2
run_blacknwhite 10 8 3
run_blacknwhite 10 8 4

run_blacknwhite 10 7 0
run_blacknwhite 10 7 1
run_blacknwhite 10 7 2
run_blacknwhite 10 7 3

run_blacknwhite 10 6 0
run_blacknwhite 10 6 1
run_blacknwhite 10 6 2
run_blacknwhite 10 6 3

run_blacknwhite 10 5 0
run_blacknwhite 10 5 1
run_blacknwhite 10 5 2

run_blacknwhite 10 4 0
run_blacknwhite 10 4 1
run_blacknwhite 10 4 2

run_blacknwhite 10 3 0
run_blacknwhite 10 3 1

run_blacknwhite 10 2 0
run_blacknwhite 10 2 1


run_8colors 10 8
run_8colors 10 7
run_8colors 10 6
run_8colors 10 5
run_8colors 10 4
run_8colors 10 3
run_8colors 10 2

