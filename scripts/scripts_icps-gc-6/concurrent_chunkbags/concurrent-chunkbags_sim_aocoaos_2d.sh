#!/bin/bash

export LD_LIBRARY_PATH=/opt/intel/compilers_and_libraries_2017.0.098/linux/compiler/lib/intel64_lin/:$LD_LIBRARY_PATH
export GOMP_CPU_AFFINITY="0 2 4 6 8 10 12 14 16 18"

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/concurrent_chunkbags

run_classical() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocoaos_2d-$1thread.out | tee sortAlways-1mpi-$1thread.txt
}

run_concurrent() {
  export OMP_NUM_THREADS=$1
  cd $FOLDER/$1core$2
  mpirun --report-bindings --cpus-per-proc 10 -np 1 ./aocoaos_2d-$1thread$2.out | tee sortAlways-1mpi-$1thread$2.txt
}

run_classical 10
run_concurrent 10 _concurrent
run_concurrent 10 _concurrent_2-static-arrays
run_concurrent 10 _concurrent_only
