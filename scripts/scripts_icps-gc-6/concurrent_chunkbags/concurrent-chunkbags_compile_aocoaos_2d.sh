#!/bin/bash

#Modify the compile scripts : modify the -DNB_PARTICLE= if you want a different number of particles. Here given for 200M particles.
#                             modify the -DCHUNK_SIZE= if you want a different chunk size.
#Modify the source code     : modify other simulation parameters directly inside $PICVERT_HOME/simulations/sim2d_aocoaos_4corners_XXX.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_icps-gc-6/concurrent_chunkbags

compile_icc_classical() {
  mkdir -p "$FOLDER/$1core"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_chunkbags_of_aos_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_aocoaos_4corners.c -DNB_PROC=$1 -DNB_PARTICLE=200000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core/aocoaos_2d-$1thread.out
}

compile_icc_concurrent() {
  mkdir -p "$FOLDER/$1core$2"
  cp * $FOLDER/
  mpiicc -I$PICVERT_HOME/include $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_aos_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_aocoaos_4corners$2.c -DNB_PROC=$1 -DNB_PARTICLE=200000000 -DCHUNK_SIZE=512 -lfftw3 -lm -O3 -qopenmp -march=native -std=gnu11 -o $FOLDER/$1core$2/aocoaos_2d-$1thread$2.out
}

compile_icc_classical 10
compile_icc_concurrent 10 _concurrent
compile_icc_concurrent 10 _concurrent_2-static-arrays
compile_icc_concurrent 10 _concurrent_only
