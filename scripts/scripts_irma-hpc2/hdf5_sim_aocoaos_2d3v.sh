#!/bin/bash

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/gitlab/Pic-Vert

run() {
  cd $PICVERT_HOME
  mkdir runhdf5
  cd runhdf5
  export OMP_NUM_THREADS=$1
#Décommenter la ligne avec le nohup si c'est un très long run qu'on veut lancer en tâche de fond (mais alors attention à récupérer le pid si on veut le killer).
#Sinon laisser comme ça.
#(et bien sûr ne laisser qu'une seule ligne décommentée !)
  mpirun --report-bindings --cpus-per-proc 24 -np 1 $PICVERT_HOME/scripts_irma-hpc2/aocoaos_2d3v-concurrent.out $PICVERT_HOME/scripts_irma-hpc2/parameters_2d3v.txt | tee sortPeriodic-1mpi-$1thread.txt
#  nohup mpirun --report-bindings --cpus-per-proc 24 -np 1 $PICVERT_HOME/scripts_irma-hpc2/aocoaos_2d3v-concurrent.out $PICVERT_HOME/scripts_irma-hpc2/parameters_2d3v.txt > sortPeriodic-1mpi-$1thread.txt &
}

#Si vraiment tu es sûr d'être le seul, tu peux faire tourner sur 24 coeurs, sinon laisse pour 16 coeurs, ça laisse un peu de marge.
#run 24
run 16

