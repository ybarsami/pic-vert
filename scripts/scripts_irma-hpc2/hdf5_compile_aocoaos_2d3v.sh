#!/bin/bash

#Libraries for HDF5         : -lz -lm -lhdf5
#Optional library for HDF5  : -lsz (not present on IRMA-hpc2, but not useful)
#Include path for hdf5.h    : -I/usr/include
#                             (find your right path by typing "locate hdf5.h" in a terminal, maybe preceded by "updatedb")
#Library path for libhdf5.a : -L/usr/lib/x86_64-linux-gnu
#                             (find your right path by typing "locate libhdf5.a" in a terminal, maybe preceded by "updatedb")
#Modify the compile scripts : modify the -DCHUNK_SIZE=                   for a different chunk size (you should not need to modify that)
#                             modify the -DPICVERT_MAX_NB_FOURIER_MODES= for the output of more Fourier modes
#                             modify the -DPICVERT_MAX_LINE_LENGTH=      for the use of more characters on one line in the parameter file
#Modify other parameters    : modify the other parameters in parameters_2d3v.txt
#Modify the source code     : modify anything else directly inside $PICVERT_HOME/simulations/sim2d3v_aocoaos_concurrent.c

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/gitlab/Pic-Vert

compile_gcc() {
  export OMPI_CC=gcc
  mpicc -I$PICVERT_HOME/include -I/usr/include -L/usr/lib/x86_64-linux-gnu $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/hdf5_io.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_aos_2d3v.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d3v_aocoaos_concurrent.c -DCHUNK_SIZE=256 -DPIC_VERT_USE_SPECIAL_CHUNKS -lfftw3 -lz -lm -lhdf5 -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wno-unused-function -o aocoaos_2d3v-concurrent.out
}

compile_gcc

