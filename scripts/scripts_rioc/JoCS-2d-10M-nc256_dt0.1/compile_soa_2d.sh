#!/bin/bash

#Libraries to link          : -lm
#Library path for -lfftw3   : -L/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/lib
#Modify the compile scripts : modify the -DNCX=                  for the number of cells on the X axis
#                             modify the -DNCY=                  for the number of cells on the Y axis
#                             modify the -DDELTA_T=              for the duration of a time step
#                             modify the -DNB_PARTICLE=          for the number of particles (per MPI process: 64 GB per node on Occigen => maximum around 1G per node, hence 500M per socket)
#                             modify the -DI_CELL_2D_TYPE=       for the type of space-filling curve (TILE, ROW_MAJOR, MORTON, see space_filling_curves.h)
#                             modify the -DNB_ITER=              for the number of time steps
#                             modify the -DNB_ITER_BETWEEN_SORT= for the number of time steps between two successive sorts
#                             modify the -DSTRIP_SIZE=           for a different strip size (From our experiments on Marconi: "In practice, in a 2d code, choosing a strip-size k between 64 and 256 gives similar optimal results.")
#Modify the source code     : modify anything else directly inside $PICVERT_HOME/simulations/sim2d_soa_4corners_stripmined.c

module purge
module load gcc/7.2.0
module load openmpi/3.0.1
module load fftw3/openmpi/gcc/64/3.3.3

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_rioc/JoCS-2d-10M-nc256_dt0.1

compile() {
    mkdir -p "$FOLDER"
    mpicc -I$PICVERT_HOME/include -I/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/include -L/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/lib $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_soa_2d.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d_soa_4corners_stripmined.c -DNCX=256 -DNCY=256 -DDELTA_T=0.1 -DNB_PARTICLE=5000000 -DI_CELL_2D_TYPE=TILE -DNB_ITER=100 -DNB_ITER_BETWEEN_SORT=20 -DSTRIP_SIZE=64 -lfftw3 -lm -O3 -fopenmp -march=native -std=gnu99 -o $FOLDER/soa_2d.out
}

compile

