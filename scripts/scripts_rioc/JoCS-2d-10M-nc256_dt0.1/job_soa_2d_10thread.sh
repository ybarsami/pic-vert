#!/bin/bash

module purge
module load gcc/7.2.0
module load openmpi/3.0.1
module load fftw3/openmpi/gcc/64/3.3.3

export LD_LIBRARY_PATH=/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/lib/:$LD_LIBRARY_PATH

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_rioc/JoCS-2d-10M-nc256_dt0.1

run() {
    #Without hyperthreading
    export OMP_NUM_THREADS=$1
    cd $FOLDER
    # 2 mpi processes (each of 5M particles, cf. the compile script)
    mpirun -machinefile $OAR_FILE_NODES \
           -mca plm_rsh_agent "oarsh" \
           -np 2 \
           ./soa_2d.out
}

run 10
