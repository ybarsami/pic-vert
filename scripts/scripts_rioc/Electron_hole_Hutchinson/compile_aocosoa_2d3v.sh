#!/bin/bash

#Libraries to link for HDF5 : -lm -lhdf5
#Include path for fftw3.h   : -I/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/include
#Library path for -lfftw3   : -L/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/lib
#Include path for hdf5.h    : -I/cm/shared/apps/hdf5_18/1.8.11/include
#                             (find your right path by typing "module show hdf5_18/1.8.11" in a terminal)
#Library path for libhdf5.a : /cm/shared/apps/hdf5_18/1.8.11/lib
#                             (find your right path by typing "module show hdf5_18/1.8.11" in a terminal)
#Modify the compile scripts : modify the -DCHUNK_SIZE=                   for a different chunk size.
#                             modify the -DPICVERT_MAX_NB_FOURIER_MODES= for the output of more Fourier modes
#                             modify the -DPICVERT_MAX_LINE_LENGTH=      for the use of more characters on one line in the parameter file
#Modify other parameters    : modify the other parameters in parameters_2d3v.txt
#Modify the source code     : modify anything else directly inside $PICVERT_HOME/simulations/sim2d3v_aocosoa_europar2018.c

module purge
module load gcc/7.2.0
module load openmpi/3.0.1
module load fftw3/openmpi/gcc/64/3.3.3
module load hdf5_18/1.8.11

#A modifier selon là où se situe Pic-Vert.
PICVERT_HOME=$HOME/Pic-Vert
FOLDER=$PICVERT_HOME/runs/runs_rioc/Electron_hole_Hutchinson

compile() {
  mkdir -p "$FOLDER"
  cp parameters_2d3v.txt $FOLDER/
  cp * $FOLDER/
  mpicc -I$PICVERT_HOME/include -I/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/include -I/cm/shared/apps/hdf5_18/1.8.11/include -L/cm/shared/apps/fftw/openmpi/gcc/64/3.3.3/lib -L/cm/shared/apps/hdf5_18/1.8.11/lib $PICVERT_HOME/src/matrix_functions.c $PICVERT_HOME/src/meshes.c $PICVERT_HOME/src/output.c $PICVERT_HOME/src/parameter_reader.c $PICVERT_HOME/src/random.c $PICVERT_HOME/src/space_filling_curves.c $PICVERT_HOME/src/diagnostics.c $PICVERT_HOME/src/fields.c $PICVERT_HOME/src/hdf5_io.c $PICVERT_HOME/src/initial_distributions.c $PICVERT_HOME/src/particle_type_concurrent_chunkbags_of_soa_2d3v.c $PICVERT_HOME/src/poisson_solvers.c $PICVERT_HOME/src/rho.c $PICVERT_HOME/simulations/sim2d3v_aocosoa_europar2018.c -DCHUNK_SIZE=256 -DOMP_TILE_SIZE=2 -lfftw3 -lm -lhdf5 -O3 -fopenmp -march=native -std=gnu11 --param max-completely-peeled-insns=0 -Wall -Wfloat-conversion -o $FOLDER/aocosoa_2d3v.out
}

compile

