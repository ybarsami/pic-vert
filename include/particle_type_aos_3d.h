#ifndef PIC_VERT_PARTICLE_TYPE_AOS_3D
#define PIC_VERT_PARTICLE_TYPE_AOS_3D

#include "meshes.h" // type cartesian_mesh_3d

/*****************************************************************************
 *                       Particle data structures                            *
 *****************************************************************************/

/*
 *         y-axis
 *            ^
 *            |
 *      y_max |———————|———————|   ...               ...   |———————|
 *            |       |  2 *  |                           |  ncy  |
 *            |ncx - 1|  ncx  |                           |* ncx  |
 *            |       |  - 1  |                           |  - 1  |
 *            |———————|———————|   ...               ...   —————————
 *            .               .                                   .
 *            .               .                                   .
 *            .               .                                   .
 *                                    dx*delta_x
 *            |                         <——>
 *            |           ...           |———————|                 .
 *            |                         |       |                 .
 *          y +-------------------------|--*    | ^               .
 *            |                         |  .    | | dy*delta_y
 *            |           ...           |———————| v
 *            |                            .     
 *            .               .            .              .       .
 *            .               .            .              .       .
 *            .               .            .              .       .
 *            |———————|———————|            .              |———————| ^
 *            |       |       |            .              |(ncy-1)| |
 *            |   1   |ncx + 1|            .              |* ncx  | | delta_y
 *            |       |       |            .              |  + 1  | |
 *            |———————|———————|   ...      .        ...   ————————— v
 *            |       |       |            .              |(ncy-1)|
 *            |   0   |  ncx  |            .              |* ncx  |
 *            |       |       |            .              |       |
 *      y_min +———————|———————|   ...   ———+—————   ...   —————————————> x-axis
 *          x_min     <———————>            x                    x_max
 *                     delta_x
 *
 * In the physical world, the particle has x in [x_min ; x_max [
 *                                         y in [y_min ; y_max [
 *                                         z in [z_min ; z_max [
 * 
 * This is mapped to a grid of size ncx * ncy * ncz. Thus, we have :
 *     delta_x = (x_max - x_min) / ncx;
 *     delta_y = (y_max - y_min) / ncy;
 *     delta_z = (z_max - z_min) / ncz;
 * 
 * If we call :
 *     x_mapped = (x - x_min) / delta_x which is in [0 ; ncx [
 *     y_mapped = (y - y_min) / delta_y which is in [0 ; ncy [
 *     z_mapped = (z - z_min) / delta_z which is in [0 ; ncz [
 *     index_x = floor(x_mapped), the number of times we have to move
 *         by delta_x from the start of the mesh to the particle on the x-axis
 *     index_y = floor(y_mapped), the number of times we have to move
 *         by delta_y from the start of the mesh to the particle on the y-axis
 *     index_z = floor(z_mapped), the number of times we have to move
 *         by delta_z from the start of the mesh to the particle on the z-axis
 * Then i_cell, the cell index (given inside the cells above), is computed by :
 *     i_cell = (index_x * ncy + index_y) * ncz + index_z;
 */

typedef struct particle_3d particle_3d;
struct particle_3d {
     int i_cell; // Cell index of the particle in [|0 ; ncx*ncy*ncz|[ (see the drawing above)
     float dx;   // x_mapped - index_x, a number in [0 ; 1[       (see the drawing above)
     float dy;   // y_mapped - index_y, a number in [0 ; 1[       (see the drawing above)
     float dz;   // z_mapped - index_z, a number in [0 ; 1[       (see the drawing above)
     double vx;  // speed of the particle on the x-axis
     double vy;  // speed of the particle on the y-axis
     double vz;  // speed of the particle on the z-axis
};

/*
 * Initializes arrays of num_particle particles from a file.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[out] weight.
 * @param[out] particles[num_particle] newly allocated array of particles read from file.
 */
void read_particle_arrays_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        float* weight, particle_3d** particles);

/*
 * Initializes arrays of num_particle random particles following a given distribution
 * for positions, and following the gaussian distribution for speeds.
 *
 * @param[in]  mpi_world_size the number of MPI processes.
 * @param[in]  num_particle the size of the arrays (number of particles in it).
 * @param[in]  mesh, the mesh on which we're working.
 * @param[in]  sim_distrib the physical test case (tells the distribution that the particules should follow).
 * @param[in]  spatial_params.
 * @param[in]  thermal_speed.
 * @param[out] weight.
 * @param[out] particles[num_particle] newly allocated array of randomized particles.
 */
void create_particle_arrays_3d(int mpi_world_size, unsigned int num_particle, cartesian_mesh_3d mesh,
        unsigned char sim_distrib, double* spatial_params, double* speed_params, float* weight,
        particle_3d** particles);



/*****************************************************************************
 *                           Sorting functions                               *
 *                     (by increasing values of i_cell)                      *
 *****************************************************************************/

typedef struct particle_sorter_oop_3d particle_sorter_oop_3d;
struct particle_sorter_oop_3d {
    // number of particles in the array to sort
    unsigned int num_particle;
    // number of cells in the mesh (number of different values for i_cell)
    unsigned int num_cell;
    // [num_threads][num_cell] array of integers, telling
    // how many particles each thread sees for each value of i_cell.
    int** num_particle_per_cell;
    // [num_threads][num_cell] array. index_next_particle[t][i] tells where the
    // thread t has to store the next particle that has i_cell = i. Whenever
    // a thread puts particle p in the correct position,
    // index_next_particle[t][p.i_cell] is incremented.
    int** index_next_particle;
    // [num_particle] additional arrays used to sort.
    particle_3d* particles_tmp;
};

/*
 * @param[in] num_particle, number of particles in the array to sort.
 * @param[in] num_cell, number of cells in the mesh (number of different values for i_cell).
 * @return    a particle sorter.
 */
particle_sorter_oop_3d new_particle_sorter_oop_3d(unsigned int num_particle, unsigned int num_cell);

/*
 * Sort the array of particles (with temporary arrays).
 *
 * @param[in, out] sorter a pointer on the particle_sorter_oop for this array (has to be initialized
 *                 before the call).
 * @param[in, out] particles a pointer on the [num_particle] array of particles to sort.
 */
void sort_particles_oop_3d(particle_sorter_oop_3d* sorter, particle_3d** particles);

void free_particle_sorter_oop_3d(particle_sorter_oop_3d sorter, unsigned int num_cell);

#endif // ifndef PIC_VERT_PARTICLE_TYPE_AOS_3D

