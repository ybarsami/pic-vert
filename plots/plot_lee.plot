set terminal postscript eps color enhanced
set output "diag_log_ee.eps"
set autoscale

set xlabel "Time (adimensionned)"
set xtic auto

set ylabel "0.5 log(Electric energy)"
set ytic auto

plot \
  "../run/diag_lee_4corners-opt-1loop.txt" using 1:2 title '0.5 log(Electric energy) - simulated' with lines, \
  "../run/diag_lee_4corners-opt-1loop.txt" using 1:3 title '0.5 log(Electric energy) - expected' with lines

