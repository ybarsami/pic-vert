if (!exists("ncx")) ncx=64
if (!exists("ncy")) ncy=64
if (!exists("num_part")) num_part=50000000
if (!exists("alpha")) alpha=0.01
if (!exists("kx")) kx=0.5
if (!exists("ky")) ky=0.5

#set terminal postscript eps color enhanced
#set output "test_particles.eps"
set autoscale

set xlabel "X"
set xtic auto

set ylabel "Y"
set ytic auto

set zlabel "Nb particles"
set ztic auto

splot [0:2*pi/kx][0:2*pi/ky] \
  "../run/test_particles_2d.dat" using ($1*2*pi/ky/ncx):($2*2*pi/ky/ncy):3 title 'Number of particles - 2d', \
  "../run/test_particles_3d.dat" using ($1*2*pi/ky/ncx):($2*2*pi/ky/ncy):3 title 'Number of particles - 3d', \
  num_part/ncx/ncy*(1+alpha*cos(kx*x)*cos(ky*y)) title '1 + alpha * cos(kx * x) * cos(ky * y)'

# The function is to be plotted in [0 ; 2*pi/kx)
# Thus, we have to multiply the input by 2*pi/kx/ncx

pause -1
