gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -DI_CELL_2D_TYPE=HILBERT
./a.out > hilbert2d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -DI_CELL_2D_TYPE=ROW_MAJOR
./a.out > row_major2d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -DI_CELL_2D_TYPE=TILE -DTILE_SIZE=4
./a.out > tile2d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DPIC_VERT_SFC_WITH_ADDITIONAL_ARRAYS -DI_CELL_2D_TYPE=MORTON
./a.out > morton2d.tex

for i_cell_type in hilbert tile morton row_major
do
    latex "${i_cell_type}2d.tex"
    dvips "${i_cell_type}2d.dvi"
    ps2pdf "${i_cell_type}2d.ps"
done

