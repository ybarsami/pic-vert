/**
 * draw_sfc3d.c: Print a line for use with pstricks, with gradient, for different sfcs.
 *
 * Compile with:
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=HILBERT
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=ROW_MAJOR
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=TILE -DTILE_SIZE=4
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=MORTON
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

#include <stdio.h>                // function printf
#include "colormaps.h"            // constant plasma_data
#include "space_filling_curves.h" // macros   I_CELL_PARAM1_3D, I_CELL_PARAM2_3D, ICX_PARAM1_3D, ICX_PARAM2_3D
                                  //          ICY_PARAM1_3D, ICY_PARAM2_3D, ICZ_PARAM_3D

int main(int argc, char** argv) {
    const int ncx = 16;
    const int ncy = 16;
    const int ncz = 16;
    const int icell_param1 = I_CELL_PARAM1_3D(ncx, ncy, ncz);
    const int icell_param2 = I_CELL_PARAM2_3D(ncx, ncy, ncz);
    const int icx_param1 = ICX_PARAM1_3D(ncx, ncy, ncz);
    const int icx_param2 = ICX_PARAM2_3D(ncx, ncy, ncz);
    const int icy_param1 = ICY_PARAM1_3D(ncx, ncy, ncz);
    const int icy_param2 = ICY_PARAM2_3D(ncx, ncy, ncz);
    const int icz_param  = ICZ_PARAM_3D(ncx, ncy, ncz);
    
    unsigned short i, j, k;
    unsigned short i_prev, j_prev, k_prev;
    unsigned int n;
    
    int height_min = ncx * 0.7;
    printf("\\documentclass{article}\n");
    printf("\\usepackage[utf8]{inputenc}\n");
    printf("\\usepackage[english]{babel}\n");
    printf("\\usepackage{pgf}\n");
    printf("\\usepackage{pstricks,pst-plot,pst-text,pst-tree,pst-3dplot}\n");
    printf("\\usepackage[active,tightpage]{preview}\n");
    printf("\\PreviewEnvironment{pspicture}\n");
    printf("\\begin{document}\n");
    printf("\\psset{unit=0.5}\n");
    printf("\\psset{fillstyle=solid}\n");
    printf("\\begin{pspicture*}(-%d.1,-%d.1)(%d.1,%d.1)\n", ncx, height_min, ncx-1, ncx-1);
    printf("\\pstThreeDCoor[xMin=0,xMax=%d,yMin=0,yMax=%d,zMin=0,zMax=%d]\n", ncx, ncy, ncz);
    
#define COLOR_MAP plasma_data
    double height_color_map = (double)ncx;
    
    int nbColor = 100;
    for (i = 0; i < nbColor; i++) {
        printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[i*256/nbColor][0], COLOR_MAP[i*256/nbColor][1], COLOR_MAP[i*256/nbColor][2]);
        printf("\\pspolygon[fillcolor=MyColor,linecolor=MyColor](-%d,%.4f)(-%d.5,%.4f)(-%d.5,%.4f)(-%d,%.4f)\n", ncx, (double)i * height_color_map / ((double)nbColor) - height_min, ncx-2, (double)i * height_color_map / ((double)nbColor) - height_min, ncx-2, (double)(i+1) * height_color_map / ((double)nbColor) - height_min, ncx, (double)(i+1) * height_color_map / ((double)nbColor) - height_min);
    }
    
    unsigned int nb_points = ncx * ncy * ncz;
    unsigned int last_color = 0;
    REVERSE_I_CELL_3D(icell_param1, icell_param2, 0, &i_prev, &j_prev, &k_prev);
    printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[0][0], COLOR_MAP[0][1], COLOR_MAP[0][2]);
    printf("\\pstThreeDLine[linewidth=3pt,linecolor=MyColor]{-}(%d.5, %d.5, %d.5)", i_prev, j_prev, k_prev);
    for (n = 0; n < nb_points - 1; n++) {
        REVERSE_I_CELL_3D(icell_param1, icell_param2, n + 1, &i, &j, &k);
        if (n*256/nb_points != last_color) {
            last_color = n*256/nb_points;
            printf("(%d.5, %d.5, %d.5)\n", i, j, k);
            printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[n*256/nb_points][0], COLOR_MAP[n*256/nb_points][1], COLOR_MAP[n*256/nb_points][2]);
            printf("\\pstThreeDLine[linewidth=3pt,linecolor=MyColor]{-}");
        } else if (k_prev == ncz-1 && k == 0) {
            printf("\n");
            printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[n*256/nb_points][0], COLOR_MAP[n*256/nb_points][1], COLOR_MAP[n*256/nb_points][2]);
            printf("\\pstThreeDLine[linewidth=3pt,linecolor=MyColor]{-}");
        }
        printf("(%d.5, %d.5, %d.5)", i, j, k);
        i_prev = i;
        j_prev = j;
        k_prev = k;
    }
    printf("\n");
    printf("\\end{pspicture*}\n");
    printf("\\end{document}\n");
    printf("\n");
    
    return 0;
}

