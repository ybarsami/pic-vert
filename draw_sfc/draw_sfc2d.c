/**
 * draw_sfc2d.c: Print a line for use with pstricks, with gradient, for different sfcs.
 *
 * Compile with:
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DI_CELL_2D_TYPE=HILBERT
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DI_CELL_2D_TYPE=ROW_MAJOR
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DI_CELL_2D_TYPE=TILE -DTILE_SIZE=4
 *   gcc -I../include ../src/space_filling_curves.c draw_sfc2d.c -DI_CELL_2D_TYPE=MORTON
 *
 * Contact:
 *   Yann Barsamian <ybarsamian@unistra.fr>
 */

#include <stdio.h>                // function printf
#include "colormaps.h"            // constant plasma_data
#include "space_filling_curves.h" // macros   I_CELL_PARAM_2D, ICX_PARAM_2D, ICY_PARAM_2D, REVERSE_I_CELL_2D

int main(int argc, char** argv) {
    const int ncx = 16;
    const int ncy = 16;
    const int icell_param = I_CELL_PARAM_2D(ncx, ncy);
    const int icx_param = ICX_PARAM_2D(ncx, ncy);
    const int icy_param = ICY_PARAM_2D(ncx, ncy);
    
    unsigned short i, j;
    unsigned short i_prev, j_prev;
    unsigned int n;
    
    printf("\\documentclass{article}\n");
    printf("\\usepackage[utf8]{inputenc}\n");
    printf("\\usepackage[english]{babel}\n");
    printf("\\usepackage{pgf}\n");
    printf("\\usepackage{pstricks,pst-plot,pst-text,pst-tree}\n");
    printf("\\usepackage[active,tightpage]{preview}\n");
    printf("\\PreviewEnvironment{pspicture}\n");
    printf("\\begin{document}\n");
    printf("\\psset{unit=0.5}\n");
    printf("\\psset{fillstyle=solid}\n");
    printf("\\begin{pspicture*}(-2.1,-0.1)(%d.1,%d.9)\n", ncy, ncx);
    
#define COLOR_MAP plasma_data
    double height_color_map = (ncx >= 10) ? 10. : (double)ncx;
    
    int nbColor = 100;
    for (i = 0; i < nbColor; i++) {
        printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[i*256/nbColor][0], COLOR_MAP[i*256/nbColor][1], COLOR_MAP[i*256/nbColor][2]);
        printf("\\pspolygon[fillcolor=MyColor,linecolor=MyColor](-2,%.4f)(-1,%.4f)(-1,%.4f)(-2,%.4f)\n", (double)i * height_color_map / ((double)nbColor), (double)i * height_color_map / ((double)nbColor), (double)(i+1) * height_color_map / ((double)nbColor), (double)(i+1) * height_color_map / ((double)nbColor));
    }
    printf("\\foreach \\x in {0,...,%d} {\n", ncy);
    printf("    \\psline(\\x,0)(\\x,%d)\n", ncx);
    printf("    \\psline(0,\\x)(%d,\\x)\n", ncy);
    printf("}\n");
    printf("\\psset{arrows=->,nodesep=0pt} %%Arcs parameters\n");
    printf("\\psline(-0.5,%d.5)(-0.5,%d.5) \\uput[d](-0.5,%d.5){$i_x$}\n", ncx, ncx - 1, ncx - 1);
    printf("\\psline(-0.5,%d.5)( 0.5,%d.5) \\uput[r]( 0.5,%d.5){$i_y$}\n", ncx, ncx, ncx);
    
    unsigned int nb_points = ncx * ncy;
    REVERSE_I_CELL_2D(icell_param, 0, &i_prev, &j_prev);
    for (n = 0; n < nb_points - 1; n++) {
        printf("\\definecolor{MyColor}{rgb}{%.6f,%.6f,%.6f}\n", COLOR_MAP[n*256/nb_points][0], COLOR_MAP[n*256/nb_points][1], COLOR_MAP[n*256/nb_points][2]);
        printf("\\psline[linewidth=3pt,linecolor=MyColor]{-}");
        REVERSE_I_CELL_2D(icell_param, n + 1, &i, &j);
        printf("(%d.5, %d.5)(%d.5, %d.5)\n", j_prev, ncx - 1 - i_prev, j, ncx - 1 - i);
        i_prev = i;
        j_prev = j;
    }
    printf("\\end{pspicture*}\n");
    printf("\\end{document}\n");
    printf("\n");
    
    return 0;
}

