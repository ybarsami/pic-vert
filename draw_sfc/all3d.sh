gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=HILBERT
./a.out > hilbert3d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=ROW_MAJOR
./a.out > row_major3d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=TILE -DTILE_SIZE=2
./a.out > tile3d.tex
gcc -I../include ../src/space_filling_curves.c draw_sfc3d.c -DI_CELL_3D_TYPE=MORTON
./a.out > morton3d.tex

for i_cell_type in hilbert tile morton row_major
do
    latex "${i_cell_type}3d.tex"
    dvips "${i_cell_type}3d.dvi"
    ps2pdf "${i_cell_type}3d.ps"
done

