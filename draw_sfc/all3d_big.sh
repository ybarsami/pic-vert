gcc -I../include ../src/space_filling_curves.c draw_sfc3d_big.c -DI_CELL_3D_TYPE=TILE -DTILE_SIZE=4
./a.out > tile3d.tex

for i_cell_type in tile
do
    latex "${i_cell_type}3d.tex"
    dvips "${i_cell_type}3d.dvi"
    ps2pdf "${i_cell_type}3d.ps"
done

